#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <complex>
	
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <cmath>

#ifndef POLYNOMIALFIELD_H
#define	POLYNOMIALFIELD_H

enum sign {positive, negative};
/**
* \class PolynomialField
* \brief Class to represent the P(x) field
*  The polynomial are represented by vector of float. The entries of the vector represent the coefficient of power of x starting with power 0 for index 0
*  Some examples  of polynomials and the respective vectors are :
 * [0 1]     Polynomial : x 
 * [1 1]     Polynomial : x+1
 * [-1 0 1]  Polynomial : x^2-1
 * 
 * The constructor takes in the degree of the polynomial,
 * Before setting the coefficients of a created polynomial, it is initialised to P(x)=0;
 * The field comprises the following operations : addition (+), substraction (-), multiplication(*), power (^)
 * There is also an implementation of the binomial expansion 
 *  \author Yves-Francois Rivard, Serge Mbamba, Noel Moyo
* \version 1.0
* \date 24/01/2014 
 * @TODO  implementation of long division
*
*/
template <class T>
class PolynomialField {
public:
    PolynomialField(int );
    PolynomialField(const PolynomialField& orig);
    virtual ~PolynomialField();
    static std::vector<float> BinomialExpansion (int degree, sign _sign);
    PolynomialField operator*(const  PolynomialField& rhs);
    PolynomialField operator*(const T);
    PolynomialField operator+(PolynomialField& rhs);
    PolynomialField operator-(PolynomialField& rhs);
    PolynomialField operator^(int);
    PolynomialField operator=(const PolynomialField& rhs);
    unsigned int getOrder();
   
    T getCoefficientAtPower(unsigned int);
    std::vector<T> getCoefficients();
      
    void setCoefficients(const std::vector <T>& );
    void setDegree(unsigned int );
      
    
/**
* \brief overloading the << operator to allow cout to be called on a Polynomial
* \author Yves-Francois Rivard, Serge Mbamba, Noel Moyo
* \version 1.0
* \date 24/01/2014 
 * @TODO  implementation of long division
*
*/
       friend std::ostream& operator << (std::ostream& out, PolynomialField& rhs)
           {            
           std::string str="";
           std::string sign;
           for (int i=rhs.degree; i>0; i--)
           {
               std::complex <double> val(rhs.coefficients[i]);
               
               std::ostringstream ss;
               if (val.real()<0) val.real()*=-1;
               if (val.real()<0.0000000001) // it is not required to display values less than 10^-10, a Zero is displayed instead
                   ss << 0;
               else
               {
                   
                   ss << val.real();      
               }
               
               std::string s(ss.str());
               
            
                   ss << val.imag();      
               
               
               std::string s2(ss.str());
               
               
               if (val.imag()!=0) s=s+"+i*("+s2+')';
               
               std::ostringstream ss_2;
               ss_2<<i;    
               std::string s_2(ss_2.str());
               
               std::complex <double> val2(rhs.coefficients[i-1]);
               // Appending the sign a minus or a plus
               if  (val2.real()>=0)  
                 sign="+";
               else
                 sign="-";
               
               str=str + s + "*x^" +s_2 + sign;
               s.clear();
               s_2.clear();
           }
           std::ostringstream ss_3;
           
           std::complex <double> val(rhs.coefficients[0]);
          
            if (val.real()<0) val.real()*=-1;
            if (val.real()<0.0000000001)// it is not required to display values less than 10^-10, a Zero is displayed instead
                ss_3 << 0;
            else
            {   
               
                ss_3 << val.real();      
            }
           
           std::string s(ss_3.str());
       
           ss_3 << val.imag();      
         
           std::string s2(ss_3.str());
               
               
           if (val.imag()!=0) s=s+"+i*("+s2+')';
           
           str=str+s;
           out<<str<<std::endl;
          
           return out;
           
           }
private:
    std::vector <T> coefficients;
    unsigned int degree;
    
};


/**
 * \brief Constructor 
 * Set the degree of the polynomial to the value of the input 
 * Pre-populate the polynomial with coefficients of value zero 
 * @param _degree
 */
template <class T>
PolynomialField<T>::PolynomialField(int _degree) :
       coefficients(),
       degree(_degree)
{
  
    for (int i=0; i<degree; i++)
      coefficients.push_back(0);
}

/**
 * \brief getter function that returns the degree of the polynomial
 */
template <class T>
unsigned int PolynomialField<T>::getOrder(){
    
    return degree;
}
 
/**
 * \brief Function that resets the degree of a polynomial
 * The resetting only occurs if the current degree of the polynomial is less than the one that it supposed to be reset to
 * After resetting the coefficients of high order terms are set to zero
 * @param _degree
 */ 
template <class T>
void PolynomialField<T>::setDegree(unsigned int _degree){
    
   if (degree<_degree){ 
   degree=_degree;
   while(coefficients.size()<degree)
      coefficients.push_back(0);
   }
   
}
/**
 * \brief Function that returns the coefficients of one polynomial ter
 * The input is the power of the terms 
 * For instance if the P(x)=0.2+ 12 *x + 8*x^2 is the polynomial and when  P(x) an instance of P(x) calls the method 
 * with input 2, it returns the coefficients of the term in x^2 which is 8.
 * @param index
 * @return 
 */
template <class T>
T PolynomialField<T>::getCoefficientAtPower(unsigned int index){
          
    if (index<=degree)
    return coefficients[index];
    else{
//        cout << "Index greater than the size of the polynomial"<<endl;
    }
          
}
/**
 * \brief Function that sets the coefficients of the Polynomial terms
 * The coefficients are set from the term in x^0 to the last highest power of x respectively 
 * to the entry in index 0 of the vector to the last entry of the vector
 * @param coeff
 */
template <class T>
void PolynomialField<T>::setCoefficients(const std::vector <T>& coeff){
    setDegree(coeff.size());
    coefficients=coeff;
    
}


/**
 * Copy constructor
 * @param orig
 */
template <class T>
PolynomialField<T>::PolynomialField(const PolynomialField& orig)
{
    degree=orig.degree;
    coefficients=orig.coefficients;
}

template <class T>
PolynomialField<T>::~PolynomialField() {
}

/**
 * overloading * operator 
 * Definition here of multiplication by a constant
 * The returned polynomial is the result of the operation of  a * P(x) , where a is the parameter 
 * The right call of the operation occurs in the order : polynomial * constant.
 * Example:  G(x) = P(x) * 5 
 * @param val
 * @return  A polynomial which the result of the multiplication
 */
template <class T>
 PolynomialField<T> PolynomialField<T>::operator*(const T val)
 {
     
    PolynomialField Sum_Poly(degree);
  
 
    std::vector <T> temp=coefficients; 
    
  for (int i=0; i<temp.size(); i++)
  {
      temp[i]=val*temp[i];
  }
    Sum_Poly.setCoefficients(temp);
    return Sum_Poly;
 }
 
 
 
/**
 * overloading * operator 
 * Definition here of multiplication by another polynomial
 * The return polynomial is the result of the operation of  P(x) * G(x), where G(x) is the input
 * @param rhs
 * @return A polynomial which the result of the multiplication
 */
 template <class T>
PolynomialField<T> PolynomialField<T>::operator*(const PolynomialField& rhs){ 
    
    int resulting_degree = degree + rhs.degree;

    PolynomialField Prod_Poly(resulting_degree);
    std::vector <T> temp_coeff;
    
    for (int i=0; i<resulting_degree-1; i++)
        
    temp_coeff.push_back(0);
  
  for (int i=0; i< degree; i++)
        for (int j=0; j <rhs.degree;  j++)
        {
            temp_coeff[i + j] += (coefficients[i] * rhs.coefficients[j]);              
        }

  Prod_Poly.setCoefficients(temp_coeff);

    return Prod_Poly;
     
}
 
template <class T>
PolynomialField<T> PolynomialField<T>::operator=(const PolynomialField& rhs){ 
    
    coefficients.clear();
    degree=rhs.degree;
    coefficients=rhs.coefficients;

    return *this;   
}

/**
 * overloading ^ operator 
 * Definition here of the elevation to a power
 * The returned polynomial is the result of the operation of  P(x)^a , where a is the parameter 
 * The right call of the operation occurs in the order : polynomial ^constant.
 * Example:  G(x) = P(x)^ 5
 * @param val
 * @return  A polynomial which the result of the elevation of the calling polynomial to a power equal to input argument
 */
template <class T>
PolynomialField<T> PolynomialField<T>::operator^(int power){ 
    
    if (power ==0)
    {
        PolynomialField special (1);
        std::vector<T> special_vector;
        special_vector.push_back(1);
        special.setCoefficients(special_vector);
        return special;
    }
  PolynomialField Power_Poly(degree);
  
  Power_Poly.setCoefficients(coefficients);
  int temp=power;
  PolynomialField copyPower(degree);
  copyPower=Power_Poly;
  
  while (temp>1)
  {
      Power_Poly=Power_Poly*copyPower;    
      temp--;
  }
 
  return Power_Poly;    
}


/**
 * overloading * operator 
 * Definition here of addition 
 * The return polynomial is the result of the operation of  P(x) + G(x), where G(x) is the input
 * @param rhs
 * @return A polynomial which the result of the addition of the calling polynomial to the argument
 */
template <class T>
PolynomialField<T> PolynomialField<T>::operator+(PolynomialField& rhs){ 
    
    int deg=0;
    
    if (degree>=rhs.degree)
    {
        deg=degree;
        rhs.setDegree(deg);
    }
    else {
          
        deg=rhs.degree;
        setDegree(deg);
    }  
  
  std::vector <T> temp_coeff=rhs.coefficients;
  std::vector <T> temp=coefficients; 
  
  for (int i=0; i<degree; i++)
  {
      temp[i]=temp_coeff[i]+coefficients[i];
   
  }
  PolynomialField Sum_Poly(deg);
  Sum_Poly.setCoefficients(temp);
 
  return Sum_Poly;     
}

/**
 * overloading * operator 
 * Definition here of substraction
 * The return polynomial is the result of the operation of  P(x) - G(x), where G(x) is the input
 * @param rhs
 * @return A polynomial which the result of the substraction of the calling polynomial to the argument
 */
template <class T>
PolynomialField<T> PolynomialField<T>::operator-(PolynomialField& rhs){ 
   
      int deg=0;
    
    if (degree>=rhs.degree)
    {
        deg=degree;
        rhs.setDegree(deg);
    }
    else {
        
   
        deg=rhs.degree;
        setDegree(deg);
      }
   

  
  std::vector <T> temp_coeff=rhs.coefficients;
  std::vector <T> temp=coefficients; 
  
  for (int i=0; i<degree; i++)
  {
      temp[i]=temp_coeff[i]-coefficients[i];
   
  }
  PolynomialField Diff_Poly(deg);
  Diff_Poly.setCoefficients(temp);
 
    return Diff_Poly;
       
}

/**
* \brief Function used to generate Pascal s triangle.
*	@param row Used to select the row which must be returned from the triangle.
* 	@param _sign Used to determine wether a triangle with positive coefficients or alternating positive and negative coefficient is returned.
*       @return pascal_row Vector containing the coefficients of the selected row.
*/


template <class T>
std::vector<float>  PolynomialField<T>::BinomialExpansion (int row, sign _sign){
    
    std::vector <T> pascal_row;
    pascal_row.push_back (1.0);
   
    float multiplicative;
   
    switch (_sign){
        case positive: multiplicative = 1.0;
                break;
        case negative: multiplicative = -1.0;
                break;
        default: //throw exception
                break;       
    }
           
    for (int i = 0; i < row; i++)
    {
        pascal_row.push_back((abs(pascal_row[i]) * (row - i) / (i + 1)) *pow (multiplicative, i + 1) );
    }
    std::reverse(pascal_row.begin(),pascal_row.end());
    return pascal_row;
 }

template <class T>
std::vector<T>  PolynomialField<T>::getCoefficients (){
    return coefficients;
}
#endif	/* POLYNOMIALFIELD_H */
