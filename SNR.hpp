#include "Exceptions.hpp"
#include <queue>
#include <string>
#include <fstream>
#include <vector>

#include "DataSet.hpp"
#include <cmath>
#include <iostream>
#include <cstdio>

#include "Graph.hpp"
#include "Noise.hpp"
#include <boost/thread.hpp>

#ifndef SNR_HPP
#define SNR_HPP 

using boost::thread;
using std::abs;
using std::vector;
using std::cout;
using std::endl;
/**
 * \class SNR
 * \brief SNR class to calculate the ratio
 * SNR constructor will take in two data sets, for the signal and noise, and the signal to noise 
 * ratio is calculated in the getSNR function
 * 
 * \author Mike Geddes, Matt van der Velden
 * \date 26/11/2012 
 */
template<class T>
class SNR
{

public:
	SNR(const DataSet<T>& signal, const DataSet<T>& noise);
	T getSNR();

private:
	
	// for use in multithreading
	struct calculatePower
	{
	public:
		calculatePower(DataSet<T>& data, T& pwr):
			rSum(pwr){
			vector<complex<T> > one = data.exportData();
			rSum = 0;
			T rMax = 0;
			for(double k = 0; k<one.size()-1; k++){
				if (rMax <= abs(one[k]))
					rMax = abs(one[k]);
				else
					rMax = rMax;
				rSum = rSum + abs(one[k])*abs(one[k]);
			}
			//rSum /= rMax;
			//cout<<rSum<<endl;
			rSum /= one.size();
			rSum = sqrt(rSum);
			//cout<<rSum<<endl;
		}
		void operator()()
		{
			
		} //operator
	private:
		T& rSum;
	}; //calculatePower

	DataSet<T> tSignal;
	DataSet<T> tNoise;
}; //SNR

/** \fn Constructor
 * \param signal is a constant dataset to be used to calculate SNR
 * \param noise is a constant dataset to be used to calculate SNR
 */
template<class T>
SNR<T>::SNR(const DataSet<T>& signal, const DataSet<T>& noise):
	tSignal(signal),
	tNoise(noise){

}

/** \fn getSNR
 * \brief calculates the SNR value
 */
template<class T>
T SNR<T>::getSNR(){
	T pwrSignal = 0;
	T pwrNoise = 0;

	thread t1(calculatePower(tSignal, pwrSignal));
	thread t2(calculatePower(tNoise, pwrNoise));


	t1.join();
	t2.join();
	//cout << 10*log10(pwrNoise/pwrNoise) << endl;

	T p1 = pwrNoise/pwrNoise;
	T p2 = pwrSignal/pwrNoise;

	T tmp = 10*log10(p2/p1);
	return tmp;
}


#endif