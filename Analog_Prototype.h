#include <cstdlib>
#include <complex>
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include "PolynomialField.hpp"
#include "Exceptions.hpp"

using namespace std;

#ifndef ANALOG_PROTOTYPE_H
#define	ANALOG_PROTOTYPE_H

struct transformation_data {
    float coefficient;
    int transformation_numerator;
    int transformation_denominator;
};

enum Filter_type {HighPass, LowPass, BandStop, BandPass};

/**
* \class Analog_Prototype
* \brief Class to generate the continuous prototype polynomials
* 
* Four kinds of prototype filters can be generated from this class. Two seperate constructors can be called i.e. one for Lowpass and highpass filters, and another
* for bandpass and bandstop filters.
* 
*
* \author Yves-Francois Rivard, Serge Mbamba, Noel Moyo
* \version 1.0
* \date 24/01/2014 
*
*/

class Analog_Prototype {
public:
    Analog_Prototype(int order, Filter_type type, float freq_low, float freq_high);
    Analog_Prototype(int order, Filter_type type, float freq_cutoff);
    complex<float> * Coefficients(vector<complex<float> >Poly);
    void display_Coefficients();
    vector<complex<float> > multiplication(Analog_Prototype P1, Analog_Prototype P2);
    void polynomial_Poles(vector<complex<float> >&Poles);
    void polynomial_Order();
    void LP_HPTrans();
    void LP_LPTrans();
    void LP_BPTrans();
    void LP_BSTrans();
    void Filter_Gain(float gain);
    vector<float>getNumer();
    vector<float>getDenom();
    
private:
    void transform();
    vector<float> transform_coefficients(const vector <transformation_data> & data);
    Analog_Prototype():Coeff_num(),Coeff_Den()
    {
        
    }
    
    int _order;
    
    Filter_type _type;
    float _freq_low;
    float _freq_high;
    float _freq_cutoff;
    
    vector <transformation_data> tf_numerator ;
    vector <transformation_data> tf_denominator ;
    
    vector<float>Coeff_num;
    vector<float>Coeff_Den;
    
    vector<float> conversion_numerator;
    vector <float> conversion_denominator;
    
    complex<float> *coeff;
    int degree; 
};


#endif	/* ANALOG_PROTOTYPE_H */
