#include "Modulator.h"
#include "QAM.h"
#include "DataSet.hpp"
#include "Exceptions.hpp"
#include <queue>
#include <string>
#include <fstream>
#include <vector>

using std::vector;
using std::string;
using std::queue; 

#ifndef SYNCRO_H
#define SYNCRO_H

/**
* \class Syncro 
* \brief Syncronise class to syncronise symbols
* 
* Syncro will take in a dataset and either add or remove preamble
* for the syncronisation to take place allow for proper demodulation 
*
* \author Mike Geddes, Matt van der Velden
* \version 1.0
* \date 26/11/2012 
*/

class Syncro
{
public: 
	//Constructor
	Syncro(Modulator &scheme, DataSet<float> &preambleD);
	// Add preamble to signal so that the receiver can synchronize
	DataSet<float> AddPreamble(string s);
	// Remove the preamble from the given signal and return a pure data signal
	DataSet<float> RemovePreamble(DataSet<float> &data);
	// Destructor
	~Syncro();

private:
	DataSet<float> preamble;
	Modulator* locMod;
	float scanPercentage;
	int correlationCount;
};

#endif
