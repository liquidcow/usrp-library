#include "Syncro.h"
#include "Correlator.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <boost/thread.hpp>

// Syncroniser works on two threads, forward and backwards searching for the preamble
struct syncForwardThread
{
public:
    syncForwardThread(DataSet<float>& d, DataSet<float>& p, float &mn, float &mx, int &tmn, int &tmx, int& sP):
    	data(d),
    	preamble(p),
    	min(mn),
    	max(mx),
    	trackmin(tmn),
    	trackmax(tmx),
    	startPoint(sP){
    }
    void operator()()
    {
    	cout<<"forwards range: ("<<startPoint+1<<","<<startPoint*2<<")"<<endl;
  		for(int i= startPoint+1; i < startPoint*2 ;i++){
		 	//DataSet<float> temp = data.slice(i, i+preamble.size() ); //slow bit
			float cor = c1.correlateWindow(preamble, data, i);
			//float cor = temp.correlate(preamble);
			if(cor<min){
				min = cor;
				trackmin = i;
			}
			if(cor>max){
				max = cor;
				trackmax = i;
			}
		}
    }

private:
    DataSet<float>& preamble, data;
    float& min, max;
    int& trackmin, trackmax, startPoint;
    Correlator c1;
};

struct syncBackwardsThread
{
public:
    syncBackwardsThread(DataSet<float>& d, DataSet<float>& p, float &mn, float &mx, int &tmn, int &tmx, int& sP):
    	data(d),
    	preamble(p),
    	min(mn),
    	max(mx),
    	trackmin(tmn),
    	trackmax(tmx),
    	startPoint(sP){
    }
    void operator()()
    {
    	cout<<"backwards range: ("<<0<<","<<startPoint<<")"<<endl;
  		for(int i= startPoint; i > 0; i--){
			//DataSet<float> temp = data.slice(i,i+preamble.size()); //slow bit
			float cor = c2.correlateWindow(preamble, data,i);
			//float cor = temp.correlate(preamble);
			if(cor<min){
				min = cor;
				trackmin = i;
			}
			if(cor>max){
				max = cor;
				trackmax = i;
			}
		}
    }

private:
    DataSet<float>& preamble, data;
    float& min, max;
    int& trackmin, trackmax, startPoint;
    Correlator c2;
};


/**
* \brief constructor
*  \param &scheme The modulation scheme to be used
*  \param &preambleD The preamble that is to be used
*/
Syncro::Syncro(Modulator &scheme, DataSet<float> &preambleD):
	preamble(preambleD),
	correlationCount(0),
	scanPercentage(0.1)
{
	locMod = &scheme;
}


/** \brief Add preamble to given string
*  \param s String in with preamble is to be added
*/
DataSet<float> Syncro::AddPreamble(string s)
{
	locMod->push_back(s);
	DataSet<float> tmp = locMod->pop();
	DataSet<float> preCopy(preamble);
	preCopy.concatenate(tmp);
	return preCopy;
}


/** 
*	\brief Syncronise a DataSet and return the DataSet with the preamble removed
*  	\param &data DataSet to which must be syncronised and preamble removed
*/
DataSet<float> Syncro::RemovePreamble(DataSet<float> &data)
{
	//Graph<float> plotter(data.sampleRate());
	float cutOff = 0.19;//5*preamble.correlate(preamble);

	int forward_trackmin = -1;
	int forward_trackmax = -1;
	float forward_max = 0;
	float forward_min = 0;
	int back_trackmin = -1;
	int back_trackmax = -1;
	float back_max = 0;
	float back_min = 0;
	scanPercentage = (preamble.size()*0.8)/data.size();
        
	int syncAttempts =  round((data.size() - preamble.size())*scanPercentage);
	int startPoint = round(syncAttempts/2);
	cout<<"startPoint: "<<startPoint<<endl;
cout<<"Sync attempts: "<<syncAttempts<<endl;
	cout<<"scan percentage: "<<scanPercentage<<endl;
	boost::thread t1(syncForwardThread(data,preamble, forward_min, forward_max, forward_trackmin, forward_trackmax, startPoint));
	
	boost::thread t2(syncBackwardsThread(data,preamble, back_min, back_max, back_trackmin, back_trackmax, startPoint));
	t2.join();
	t1.join();

	if(forward_trackmin < 0 && forward_trackmax < 0 && back_trackmin < 0 && back_trackmax < 0){
		cout<<"Sync failed - no correlation. Empty DataSet returned."<<endl;
		DataSet<float> empty;
		return empty;
	}else{
		forward_min*=-1;
		back_min*=-1;
		if(forward_min < cutOff && forward_max < cutOff && back_min < cutOff && back_max < cutOff ){
			cout<<"Sync failed - insignificant correlation. Empty DataSet returned."<<endl;
			DataSet<float> empty;
			return empty;	
		}
		//cout<<"forward_max: "<<forward_max<<"\tforward_min: "<<forward_min<<"\tback_max: "<<back_max<<"\tback_min: "<<back_min<<endl;
		//cout<<"forward_trackmax: "<<forward_trackmax<<"\tforward_trackmin: "<<forward_trackmin<<"\tback_trackmax: "<<back_trackmax<<"\tback_trackmin: "<<back_trackmin<<endl;
		if(forward_min > forward_max && forward_min > back_max && forward_min > back_min)
			return data.slice(forward_trackmin + preamble.size(), data.size());
		if(forward_max > forward_min && forward_max > back_max && forward_max > back_min)
			return data.slice(forward_trackmax + preamble.size(), data.size());
		if(back_min > back_max && back_min > forward_max && back_min > forward_min)
			return data.slice(back_trackmin + preamble.size(), data.size());
		if(back_max > back_min && back_max > forward_max && back_max > forward_min)
			return data.slice(back_trackmax + preamble.size(), data.size());
	}
}
/* 
 * \brief Destructor
 */
Syncro::~Syncro()
{
	//std::cout << "die!!" << std::endl;
	//delete (locMod);
}