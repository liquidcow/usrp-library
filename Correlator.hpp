#include "Exceptions.hpp"
#include <queue>
#include <string>
#include <fstream>
#include <vector>

#include "DataSet.hpp"
#include <cmath>
#include <iostream>
#include <cstdio>

#include "Graph.hpp"
#include "Noise.hpp"

#include <boost/thread.hpp>

#ifndef CORRELATOR_HPP
#define CORRELATOR_HPP

const float CORRELATION_ERROR = 5e-3;  // define maximum allowable error for correlation

/**
* \class Correlator
* \brief Correlation of two dataset members
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \modified : Serge Mbamba, Noel Moyo, Yves-Francois Rivard
* \version 2.0
* \date 27/01/2014
*/
class Correlator{
public:
	Correlator();

	float correlate(const DataSet<float>& d1, const DataSet<float>& d2);
	float correlateWindow(const DataSet<float>& d1, const DataSet<float>& d2, double start);
private:

	/**
	* \struct computeRealThread
	* \brief structure for threaded packet energy calculation of the real component
	* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
        * \modified : Serge Mbamba, Noel Moyo, Yves-Francois Rivard
	* \version 2.0
	* \date 27/01/2014 
	*/
	struct computeRealThread
	{
	  computeRealThread(const vector<complex<float> > &w1, const vector<complex<float> > &w2, float &res){
	     if(w1.size()!=w2.size()) throw DataSetSizeMismatch();
              for (int i = 0; i<w1.size();i++){
               
	      res += w1[i].real()*w2[i].real();
	    } 
	  }
	  void operator ()()
	  {}
	};


	/**
	* \struct computeImagThread
	* \brief structure for threaded packet energy calculation of the imaginary component
	* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
        * \modified : Serge Mbamba, Noel Moyo, Yves-Francois Rivard
	* \version 2.0
	* \date 27/01/2014
	*/
	struct computeImagThread
	{
	  computeImagThread(const vector<complex<float> > &w1, const vector<complex<float> > &w2, float &res){
	      if(w1.size()!=w2.size()) throw DataSetSizeMismatch();
              for (int i = 0; i<w1.size();i++){
	      res += w1[i].imag()*w2[i].imag();
	    }
	  }
	  void operator ()()
	  {}
	};

	/**
	* \struct compReal
	* \brief structure for threaded packet energy calculation of the real component
	* \author Mike Geddes
        * \modified : Serge Mbamba, Noel Moyo, Yves-Francois Rivard
	* \version 2.0
	* \date 27/01/2014
	*/
	struct compReal
	{
	  compReal(const vector<complex<float> > &w1, const vector<complex<float> > &w2, float &res, int sp){
	      if(w1.size()>w2.size()+sp) throw DataSetSizeMismatch();
              for (int i = 0; i<w1.size(); i++){
	      res += w1[i].real()*w2[i+sp].real();
	    } 
	  }
	  void operator ()()
	  {}
	};

		/**
	* \struct compImag
	* \brief structure for threaded packet energy calculation of the real component
	* \author Mike Geddes
        * \modified : Serge Mbamba, Noel Moyo, Yves-Francois Rivard
	* \version 2.0
	* \date 27/01/2014
	*/
	struct compImag
	{
	  compImag(const vector<complex<float> > &w1, const vector<complex<float> > &w2, float &res, int sp){
	      if(w1.size()>w2.size()+sp) throw DataSetSizeMismatch();
              for (int i = 0; i<w1.size();i++){
	      res += w1[i].imag()*w2[i+sp].imag();
	    } 
	  }
	  void operator ()()
	  {}
	};

};

#endif