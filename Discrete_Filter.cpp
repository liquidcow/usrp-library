#include "Discrete_Filter.h"


/**
* \brief Discrete filter constructor
*	@param sampling_rate Device sampling rate
*/
Discrete_Filter::Discrete_Filter(double sampling_rate):
  _sampling_rate(sampling_rate),
  tf_numerator (),
  tf_denominator ()
{
}
/**
* \brief Function used to filter a DataSet sequence
* 
* Put time equation in the form y[n] = Ax[n] + B x[n-1] + Cx[n-2] + D y[n-1] + E y [n-2]
* Push back coefficients in vectors in order they appear i.e. x coefficients vector: [A, B, C], y coefficients vector: [D, E]
* 
*	@param myInput sequence to be filtered
*	@return output filtered DataSet
*/
DataSet <float> Discrete_Filter::sequence_filtering(const DataSet<float>& myInput)
{ 
     DataSet <float> output=myInput;
     vector < complex<float> >  Data_to_filter= output.exportData();   
     Const <float> signal (0.0);
     DataSet<float> signal_discrete(signal,_sampling_rate ,0 , 0.1);
     signal_discrete.populate();
     vector < complex<float> >  Filtered_data = signal_discrete.exportData();
     
     if (output.size() != signal_discrete.size())
     {
         throw sequenceFilteringTimeMismatch();
     }
 
     vector <int> indices_x;
     vector <int> indices_y;

     float _real;
     float _imag;
     
     for (long i=0; i< output.size(); i++) 
     {
         for (long j = 0; j < time_coefficients_x.size(); j++)
         {
             indices_x.push_back(i - (time_coefficients_x.size() - 1 - j ));
         }

         for (long j = 0; j < time_coefficients_y.size(); j++)
         {
             indices_y.push_back(i - (time_coefficients_y.size() - j));
         }

    _real=0;
    _imag=0;
   
     if (indices_y.size()==time_coefficients_y.size() && indices_x.size()==time_coefficients_x.size()){
     for(int j=0; j<indices_x.size(); j++)
     {
             if(indices_x[j]>=0 && indices_x[j]<Data_to_filter.size()){
                _real=_real+real(Data_to_filter[indices_x[j]])*time_coefficients_x[j];
                _imag=_imag+imag(Data_to_filter[indices_x[j]])*time_coefficients_x[j];
             }      
     }

     for(int j=0; j<indices_y.size(); j++)    {
             if(indices_y[j]>=0 && indices_y[j]<Filtered_data.size() ){             
                    _imag=_imag+imag(Filtered_data[indices_y[j]])*time_coefficients_y[j];
                    _real=_real+real(Filtered_data[indices_y[j]])*time_coefficients_y[j];
             }
     }
     }
     else {
         throw sequenceFilteringIndexandCoefficientSizeMismatch();
     }
     complex <float> temp (_real, _imag);
     Filtered_data[i]=temp;

     indices_x.clear();
     indices_y.clear();    
     }
     output.setData( Filtered_data);
   
     return output;
}

/**
* \brief Function used to convert a z transfer function equation into the time domain
* 
* Suppose we have a transfer function of the form H(z) = (z + 2) / (z^2 + 2z +3)
* the input vectors must be as follows: numerator: [2,1] 
*                                       denominator: [3,2,1]
* The output is of the form needed for the filter sequence function.
* 
*	@param z_numerator vector containing the numerator coefficients of the transfer function
* 	@param z_denominator vector containing the denominator coefficients of the transfer function
*/

void Discrete_Filter::z_to_time ()
{
    time_coefficients_x = z_numerator;
    time_coefficients_y= z_denominator;
    
    float divider = time_coefficients_y [time_coefficients_y.size() -1];
    
    time_coefficients_y.pop_back();
    
    for (int i = 0; i < time_coefficients_y.size(); i++)
    {
        time_coefficients_y[i] = (time_coefficients_y[i] / divider) * -1.0;
    }
    
    for (int i = 0; i < time_coefficients_x.size(); i++)
    {
        time_coefficients_x[i] = time_coefficients_x[i] / divider; 
    }
    
    reverse(time_coefficients_x.begin(),time_coefficients_x.end());
    //reverse(time_coefficients_y.begin(),time_coefficients_y.end());
}



/**
* \brief Function used to convert a continuous transfer function into a discrete one through the use of the bilinear transformation
* 
* Suppose we have a transfer function of the form H(s) = (2.3s) / (0.9s^2 + 1)
* the input vectors must be as follows: numerator: [0, 2.3] 
*                                       denominator: [1, 0, 0.9]
* The output is a series of data structures containing information of every term. i.e. after some simplification, the discrete transfer function is as follows:
* H(z) = (2.3*(2/T) * (z-1) * (z+1)) / ( 0.9 * (2/T)^2 * (z-1)^2 + (z+1)^2)
* 
* In the data structure, coefficient represents the magnitude value of each term, the bilinear_numerator represents how many times that term is multiplied by the factor (z-1),
* and bilinear_denominator represents how many times that term is multiplied the the factor (z+1).  
* 
* Assuming a sampling frequency of 10 Hertz for easy calculations, the numerator data structures will be: [0,0,2], [46,1,1]
*                                                                  the denominator data structures will be: [1,0,2], [0, 1, 1], [360,2,0]
* 
*	@param continuous_numerator vector containing the numerator coefficients of the transfer function
* 	@param continuous_denominator vector containing the denominator coefficients of the transfer function
*       @TODO pre-warping
*/

void Discrete_Filter::bilinear_transform (const vector <float>& continuous_numerator, const vector <float>& continuous_denominator)
{
    float bilinear_coefficient = 2.0 / (1.0 / _sampling_rate); //  2/T
    
    vector <transformation_data> tf_numerator_temp(continuous_numerator.size());
    vector <transformation_data>  tf_denominator_temp(continuous_denominator.size());
    
    int highest = 0;
    
    if (continuous_numerator.size() > continuous_denominator.size())
    {
        highest = continuous_numerator.size() - 1;
    }
    
    else if (continuous_denominator.size() >= continuous_numerator.size())
    {
        highest = continuous_denominator.size() - 1;
    }
    
    for (int i = 0; i < continuous_numerator.size(); i++)
    {
         tf_numerator_temp[i].coefficient = pow (bilinear_coefficient, i) * continuous_numerator [i];
         tf_numerator_temp[i].transformation_numerator = i;
         tf_numerator_temp[i].transformation_denominator = highest - i;
    }
    
    for (int i =0; i < continuous_denominator.size(); i++)
    {
         tf_denominator_temp[i].coefficient =  pow (bilinear_coefficient, i) * continuous_denominator [i];
         tf_denominator_temp[i].transformation_numerator = i;
         tf_denominator_temp[i].transformation_denominator = highest - i;
    }
    tf_numerator=tf_numerator_temp;
    tf_denominator=tf_denominator_temp;
    
    z_numerator = Z_coefficientsComputator (tf_numerator);
    z_denominator = Z_coefficientsComputator (tf_denominator);
    
    while(z_numerator[z_numerator.size() - 1] == 0)
    {
        z_numerator.pop_back();
    }
    
    while(z_denominator[z_denominator.size() - 1] == 0)
    {
        z_denominator.pop_back();
    }
    
    z_to_time();
}


/**
* \brief Function which returns the structs containing the transformation data of the transfer function numerator.
* @return tf_numerator vector of transformation_data structures
*/


vector <transformation_data>  Discrete_Filter::getTf_numerator() const
{
    return tf_numerator; 
}

/**
* \brief Function which returns the structs containing the transformation data of the transfer function denominator.
* @return tf_denominator vector of transformation_data structures
*/

vector <transformation_data>  Discrete_Filter::getTf_denominator() const
{
       return tf_denominator;           
}

/**
* \brief Function which computes the z transfer function coefficients after the transformation structures have been computed form the bilinear transformation function.
 * This function must be called independently for both the numerator and denominator of the transfer function.
 * @param data Vector of transformation structures used to multiply out as required.
 * @return returns the calculated coefficients.
*/

  vector <float> Discrete_Filter::Z_coefficientsComputator(const vector <transformation_data>&  data) const{
    PolynomialField <float > my (5); 
    PolynomialField <float >  my_1 (1);
    PolynomialField <float >  my_2 (1);
     
    PolynomialField <float> my_4 (1);
      vector <float> v;
  for (int i=0; i< data.size(); i++)  {
       my_1.setCoefficients(PolynomialField<float>::BinomialExpansion(data[i].transformation_numerator, negative));
       my_2.setCoefficients(PolynomialField<float>::BinomialExpansion(data[i].transformation_denominator,positive));     
       PolynomialField <float > my_3=my_1*my_2;
   
       v.push_back(data[i].coefficient);
       my_4.setCoefficients(v);
       PolynomialField <float > my_5=my_4*my_3;   
   
      my  = my +my_5;
      v.clear();
   }     
      return my.getCoefficients();
}

/**
* \brief Function which displays the coefficients of the z domain transfer function. It should be noted that they are displayed from lowest power to highest power.
*/
  
void Discrete_Filter::display_z_coefficients()
{
    cout << "Z numerator: ";     
    for (int i = 0; i <  z_numerator.size(); i++)
    {
        cout << z_numerator[i] << " ";
    }
    cout << endl;
    cout << "Z denominator: ";
    for (int i = 0; i < z_denominator.size(); i++)
    {
        cout << z_denominator[i] << " ";
    }
    
    cout << endl;
}

/**
* \brief Function which displays the coefficients of the filter difference equation.
*/

void Discrete_Filter::display_time_coefficients()
{
    cout << "time coefficient numerator: ";     
    for (int i = 0; i <  time_coefficients_x.size(); i++)
    {
        cout << time_coefficients_x[i] << " ";
    }
    cout << endl;
    cout << "time coefficient denominator: ";
    for (int i = 0; i < time_coefficients_y.size(); i++)
    {
        cout << time_coefficients_y[i] << " ";
    }
    
    cout << endl;
}