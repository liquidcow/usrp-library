#include <cmath>
#include "Function.hpp"

#ifndef SINC_HPP
#define SINC_HPP

/**
* \class Sinc
* \brief Sinc is a class that is akin to a sinc function that can be used for trasnmission
*
* Sinc allows for the evaluation of a sinc wave, which may be useful for when users want to 
* transmit a sinc pulse. Like with the Sinusoid, just the amplitude, frequency and phase are
* defined. 
*
* \author Ashton Hudson
* \verison 1.0
* \date 20/11/2012
*
*/
template <class T>
class Sinc : public Function<T>
{
public: 
	Sinc(float amp, float freq, float phase); 
	virtual T operator()(T input); 
private:
	float pi; 
	float amp;
	float freq; 
	float phase; 
	float e;
};

/**
* Sinc constructor that allows for us to set amplitude, frequency and phase
* @param amp This is the amplitude of the wave
* @param freq This is the frequency of the wave in hertz
* @param phase this is the phase of the wave in radians, not degrees! 
*/
template<class T>
Sinc<T>::Sinc(float amp, float freq, float phase):amp(amp), freq(freq), phase(phase)
{
	pi = 4*atan(1.0);
	e = 0.00001; 		// e for error encoding, so that when the sinc wave nears 0, we can approximate
}

/**
* The round brackets have been overloaded for when the user wants to 
* evaluate their sinc wave at time T. Just pass the time into the 
* wave that the user wants. 
* @param input this is a user defined type, and the input is time. 
*/
template<class T>
T Sinc<T>::operator()(T input)
{
	T in = input;
	if (in <0+e||in >0-e)	// Our approximation in the sinc pulse. 
	{
		in = in+0.00001; 
	}
	T out = static_cast<T> (amp*(sin(2*pi*in*freq+phase)/in));
	return out; 
}

#endif
