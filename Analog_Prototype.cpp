#include "Analog_Prototype.h"

/**
* \brief constructor for Bandpass and Bandstop filters.
* @param order Order of the filter
* @param type enumeration of the filter type
* @param freq_low lower cutoff frequency of the band filter
* @param frew_high higer cutoff frequency of the band filter 
*/

Analog_Prototype::Analog_Prototype(int order, Filter_type type, float freq_low, float freq_high):_order (order),_type(type),_freq_low(freq_low),_freq_high(freq_high),_freq_cutoff(0), Coeff_num(),Coeff_Den()
{
    if(_type != BandPass && _type != BandStop)
    {
        throw wrongButterWorthSpecifications();
    }
    polynomial_Order();
}

/**
* \brief constructor for LowPass and Highpass filters.
* @param order Order of the filter
* @param type enumeration of the filter type
* @param freq_low cutoff cutoff frequency of the filter
*/

Analog_Prototype::Analog_Prototype(int order, Filter_type type, float freq_cutoff):_order (order),_type(type),_freq_low(0),_freq_high(0),_freq_cutoff(freq_cutoff), Coeff_num(),Coeff_Den()
{
    if(_type != LowPass && _type != HighPass)
    {
        throw wrongButterWorthSpecifications();
    }
    polynomial_Order();
}

/**
 * \brief Function to store polynomial coefficients in coeff array
 * @param Poly vector that has filter poles
 * @return array of coefficients
 */

complex<float> * Analog_Prototype::Coefficients(vector<complex<float> >Poly)
{
    degree=Poly.size();
    if(degree>1)
        degree=degree-1;
    
    coeff= new complex<float>[degree+1];
    for(int i= degree; i>=0;i--)
    {
        
        coeff[i]=Poly[i];
        if(degree==1)
            coeff[degree].real()=1;
            coeff[degree].imag()=0; 
    }
    return coeff;  
}

/**
* \brief Function which displays the prototype Butterworth transfer function coefficients
*/

void Analog_Prototype::display_Coefficients() {
   
    cout << "numerator coefficients: ";
    for (int i = 0; i < Coeff_num.size(); i++)
    {
        cout << Coeff_num[i] << " ";
    }
    cout << endl;
    cout << "denominator coefficients: ";
        for (int i = 0; i < Coeff_Den.size(); i++)
    {
        cout << Coeff_Den[i] << " ";
    }  
    cout << endl;
}
/**
 * \brief Function for multiplying two polynomials of various orders
 * @param P1 Butterworth object with a polynomial with order n
 * @param P2 Butterworth object with a polynomial with order m
 * @return the polynomial product of P1 and P2, which has order n+m
 */
vector<complex<float> > Analog_Prototype::multiplication(Analog_Prototype P1, Analog_Prototype P2) 
{
    int degree_Max = P1.degree + P2.degree;
    
    complex<float> *multi = new complex<float>[degree_Max + 1];
    vector<complex<float> >Mult;
    Mult.clear();
    for (int i = P1.degree; i >= 0; i--)
    {for (int j = P2.degree; j >= 0; j--)
        multi[i + j] += P1.coeff[i] * P2.coeff[j];
    }
    for(int y=0;y<=degree_Max;y++)
    { Mult.push_back(multi[y]);
  
    }
    return Mult;
}
/**
 * \brief Computes the Left Hand plane Poles for any order and store the poles in the vector
 * @param Poles vector of complex poles 
 */
void Analog_Prototype::polynomial_Poles(vector<complex<float> >&Poles)
{
    vector<float>LHP;
    LHP.clear();
    Poles.clear();
   
    float pi= 4*atan(1);
    if( _order % 2==1)
    {  
    
   for(int i=1; i<=2*_order; i++)
    {
        float real= cos((pi*i)/_order)*-1;
        float imag= sin((pi*i)/_order)*-1;
        complex<float> odd(real,imag);
        if(odd.real()>0)
        Poles.push_back(odd);      
    }
      
        }
    else
        if(_order % 2==0)
    {
       for(int i=1; i<=2*_order; i++)
       {
           float real= cos((pi*i)/_order +pi/(2*_order))*-1;
           float imag= sin((pi*i)/_order + pi/(2*_order))*-1;
           complex<float>even(real,imag);
           if(even.real()>0)
           Poles.push_back(even);
           
       }
       }
}

/**
 * \brief This function computes the polynomial coefficients for the different Butterworth order filters
 *        The numerator coefficients are stored in Coeff_num, and the denominator coefficients are stored
 *        Coeff_Den. Four filter transformations are implemented: Low Pass Filter to: LP,HP,BP,BS Transformation
 */
void Analog_Prototype::polynomial_Order()
{
    vector<complex<float> >Poles;
    vector<float> poly_Coff;
    vector<complex<float> >First;
    vector<complex<float> >Sec;
    
    Analog_Prototype P1, P2, P3;
    polynomial_Poles(Poles);
    
    if(_order==1)
    {   First.push_back(Poles[0]);
        complex<float> *first_Co=P1.Coefficients(First);
        for(int i=0; i<2;i++)
    {
        poly_Coff.push_back(first_Co[i].real());
    }
    }
    else{
    vector<complex<float> >Mult;   
    First.push_back(Poles[0]);
    Sec.push_back(Poles[1]);
    P1.Coefficients(First);
    P2.Coefficients(Sec);
    Mult=P3.multiplication(P1,P2);

    if(Poles.size()>2)
    {  
        for(int h=2; h<Poles.size();h++)
         { First.clear(); 
           Sec.clear();
           First.push_back(Poles[h]);
           Sec=P3.multiplication(P1,P2);
           P1.Coefficients(First);
           P2.Coefficients(Sec);
           Mult=P3.multiplication(P1,P2);
        }
    }
    for(int i=0; i<Mult.size();i++)
    {
        poly_Coff.push_back(Mult[i].real());
    }
    
    }
    Coeff_num.push_back(1);
    Coeff_Den= poly_Coff;

    transform();
    
    switch(_type)
    {
        case LowPass: LP_LPTrans();
            break;
        case HighPass: LP_HPTrans();
            break;
        case BandPass: LP_BPTrans();
            break;
        case BandStop: LP_BSTrans();
            break;
        default: throw wrongButterWorthSpecifications();
            break;       
    }
    
    Coeff_num = transform_coefficients (tf_numerator);
    Coeff_Den = transform_coefficients (tf_denominator);
}

/**
* \brief Function which returns the denominator of the prototype butterworth transfer function
* @return Coeff_Den vector of floats containing the transfer function coefficients
*/

vector<float> Analog_Prototype::getDenom()
{
    return Coeff_Den;
}

/**
* \brief Function which returns the numerator of the prototype butterworth transfer function
* @return Coeff_num vector of floats containing the transfer function coefficients
*/

vector<float> Analog_Prototype::getNumer()
{
    return Coeff_num;
}
/** \fn transform
 * \brief Multiplies out the polynomial values
 */
void Analog_Prototype::transform ()
{
    vector <transformation_data> tf_numerator_temp(Coeff_num.size());
    vector <transformation_data>  tf_denominator_temp(Coeff_Den.size());
    
    int highest = 0;
    
    if (Coeff_num.size() > Coeff_Den.size())
    {
        highest = Coeff_num.size() - 1;
    }
    
    else if (Coeff_Den.size() >= Coeff_num.size())
    {
        highest = Coeff_Den.size() - 1;
    }
    
    for (int i = 0; i < Coeff_num.size(); i++)
    {
         tf_numerator_temp[i].coefficient =  Coeff_num [i];
         tf_numerator_temp[i].transformation_numerator = i;
         tf_numerator_temp[i].transformation_denominator = highest - i;
    }
    
    for (int i =0; i < Coeff_Den.size(); i++)
    {
         tf_denominator_temp[i].coefficient =  Coeff_Den [i];
         tf_denominator_temp[i].transformation_numerator = i;
         tf_denominator_temp[i].transformation_denominator = highest - i;
    }
    tf_numerator=tf_numerator_temp;
    tf_denominator=tf_denominator_temp;
}

/**
* \brief Function which applies the low pass to band pass transformation on the current normalized butterworth transfer function
*/

void Analog_Prototype::LP_BPTrans()
{
    conversion_numerator.push_back(_freq_low * _freq_high);
    conversion_numerator.push_back (0);
    conversion_numerator.push_back (1.0);
    
    conversion_denominator.push_back (0);
    conversion_denominator.push_back (_freq_high-_freq_low);
}

/**
* \brief Function which applies the low pass to high pass transformation on the current normalized butterworth transfer function
*/

void Analog_Prototype::LP_HPTrans()
{
    conversion_numerator.push_back(_freq_cutoff);
    
    conversion_denominator.push_back (0);
    conversion_denominator.push_back (1.0); 
}

/**
* \brief Function which applies the low pass to low pass transformation on the current normalized butterworth transfer function
*/

void Analog_Prototype::LP_LPTrans()
{
    conversion_numerator.push_back(0);
    conversion_numerator.push_back(1);
    
    conversion_denominator.push_back(_freq_cutoff);
}

/**
* \brief Function which applies the low pass to band stop transformation on the current normalized butterworth transfer function
*/

void Analog_Prototype::LP_BSTrans()
{
    conversion_numerator.push_back (0);
    conversion_numerator.push_back (_freq_high-_freq_low);
    
    conversion_denominator.push_back(_freq_low * _freq_high);
    conversion_denominator.push_back (0);
    conversion_denominator.push_back (1.0);
}

/**
 * 
 * @param data vector that contains transformed data
 * @return vector<float> vector of tranformed coefficients
 */
vector<float> Analog_Prototype::transform_coefficients(const vector <transformation_data> & data)
{ 
    PolynomialField<float>  my (1); 
    PolynomialField<float>  my_1 (1);
    PolynomialField<float>  my_2 (1);
    PolynomialField<float>  my_4 (1);
      vector <float> v;
  for (int i=0; i< data.size(); i++)  {
       my_1.setCoefficients(conversion_numerator);
       my_1 = my_1 ^ data[i].transformation_numerator;
       my_2.setCoefficients(conversion_denominator);    
       my_2 = my_2 ^ data[i].transformation_denominator;
       PolynomialField<float>  my_3=my_1*my_2;
   
       v.push_back(data[i].coefficient);
       my_4.setCoefficients(v);
       PolynomialField<float>  my_5=my_4*my_3;   
   
      my  = my +my_5;
      v.clear();
   } 
      
      return my.getCoefficients();
}

void Analog_Prototype::Filter_Gain(float gain)
{
    
}