#include "Correlator.hpp"
/**
 *  \brief Constructor for the Correlator class
 */
Correlator::Correlator()
{
    
}
/**
 * \brief Correlate Function that correlates the two data sets
 * @param d1 a constant DataSet argument
 * @param d2 a constant DataSet argument
 * @return the correlation result per point in the DataSet
 */
float Correlator::correlate(const DataSet<float>& d1, const DataSet<float>& d2){

	float f1 = 0;
	float f2 = 0;
	float correlation = 0;

	computeRealThread a(d1.IQdata, d2.IQdata, f1);
	computeImagThread b(d1.IQdata, d2.IQdata, f2);
	boost::thread t1(a);
	boost::thread t2(b);

	correlation = f1 + f2;

	t1.join();
	t2.join();

	return correlation/d1.IQdata.size();
}
/**
 * \brief Correlation Function that cuts the window of the correlated product 
 * @param d1 constant DataSet argument
 * @param d2 constant DataSet argument
 * @param start double argument is the limiting value
 * @return the correlation result per point in the DataSet
 */
float Correlator::correlateWindow(const DataSet<float>& d1, const DataSet<float>& d2, double start){
	
	float f1 = 0;
	float f2 = 0;
	float correlation = 0;


	compReal a(d1.IQdata, d2.IQdata, f1, start);
	compImag b(d1.IQdata, d2.IQdata, f2, start);

	boost::thread t1(a);
	boost::thread t2(b);

	correlation = f1 + f2;

	t1.join();
	t2.join();

	return correlation/d1.IQdata.size();
}


