#include "FSK.h"
#include <iostream>

using namespace std;

/**
* 	\brief Constructor for FSK class
*	@param M-ary this is the number of frequencies the user wants to use
* 	@param seperation this is used to increase the frequency seperation
* 	@param frequency base frequency
* 	@param periods Number of periods per symbol
*	@param sample_rate Sample rate to be used
*/
FSK::FSK(int M_ary, int seperation ,float frequency, unsigned int periods, float sample_rate, coherence _coherence): FSK_Size(M_ary), seperation(seperation), frequency(frequency), periods(periods), sample_rate(sample_rate), _coherence(_coherence)
{
	pi = 4*atan(1.0); 
        MFSK();
}		

/**
 * \brief Function used to push back information into the modulator device.
* @param char inf this is the byte the user is to send the the QAM object for sending
*/
void FSK::push_back(string inf)
{
	Const<float> z(0.0); 
	float p = periods;
	float symbol_duration = p/frequency;
	int bit_size = log2(FSK_Size);
	DataSet<float> Padding(z,sample_rate,0,symbol_duration);
	Const<float> a(0);
	DataSet<float> Temp(a,sample_rate,0,0);
	for(int i = 0; i<inf.length(); i++)
	{
		vector<unsigned> bit_value = getbits(inf[i],bit_size); 	
		for(int j = 0; j<bit_value.size(); j++)
		{
			Temp.concatenate(constellation[bit_value[j]]); 
		}
	}
        
	DataQueue.push(Temp); 
}		

/**
 * \brief function which extracts a dataSet of the information which was pushed back into the device.
* @return A fresh DataSet is returned when the user wants to remove from the buff. 
*/
DataSet<float> FSK::pop()
{
	 DataSet<float> D = DataQueue.front(); 
	 DataQueue.pop();
	 return D;
}

/**
* \brief MFSK function initializes all the MFSK features
 * 
 * creates the gray encoded MFSK constellation where M is a power of 2. The frequency separation between points depends on the sampling rate chosen
 * and wether or not a coherent or non-coherent phase is selected.
*/

void FSK::MFSK()
{
     if (!PowerOfTwo (FSK_Size))
     {
          throw invalidFSK(); 
     }
     
     int bits =  log2(FSK_Size);
     GrayVector=grayGenerator(bits);
       
       float fs=frequency/periods;
       float Df = 0;
       
       switch (_coherence)
       {
           case coherent:
               Df = 0.25*fs*(seperation + 1);
               break;
           case noncoherent:
               Df = 0.5*fs*(seperation + 1);
               break;
           default:
               throw wrongFSKCoherenceInput();
               break;
       }
      
       vector < Sinusoid<float> > Basis;
     
       for (int  i=0; i<FSK_Size; i++){
            Basis.push_back(Sinusoid<float> (0.5,(2*(i+1)-1-FSK_Size)*Df+frequency,pi/2));
            frequencies.push_back ((2*(i+1)-1-FSK_Size)*Df+frequency);
            
            if (frequencies [i] < 0.0 || frequencies [i] > (sample_rate / 2))
            {
                throw invalidFSKfreqRange(); 
            }
            
            DataSet<float> Point(Basis[i],sample_rate,0.0, periods/frequency);
         
            populateStruct<float> a(Point);
            constellation.push_back(Point);
            boost::thread A(a);
            A.join();
       }    
       
       basisSort();
}    

/**
* \brief Sorts the basis according to the gray sequence.
*/

void FSK::basisSort()
{
        vector <string> _dummy_gray =GrayVector;
        vector <float> _dummy_freq =frequencies;
        vector <DataSet <float> > _dummy_constellation=constellation;
        int  str_int=0;
       
        for (int j=0; j<GrayVector.size(); j++){
          for (int i=0; i<(GrayVector[j]).size(); i++)   {
        
            str_int= str_int*2 +  ((GrayVector[j])[i]-'0') ;
         
          }
            _dummy_gray[str_int]=GrayVector[j];
            _dummy_freq [str_int]=frequencies[j];
            _dummy_constellation[str_int]=constellation[j];
         
           str_int=0;
        }
        GrayVector=_dummy_gray;
        frequencies= _dummy_freq;
        constellation= _dummy_constellation;
}

/**
* \brief Getter function to know which frequencies are being used in the transmission.
 * @return vector of floats containing the used frequencies.
 * 
*/

vector <float> FSK::basisFrequencies() const
{
    return frequencies;
}


/**
* \brief Getter function to know which the binary sequences corresponding to different frequencies are being used in the transmission.
 * @return vector of strings containing the used frequencies.
 * 
*/
vector <string>   FSK::getGrayVector() const{
    return GrayVector;
}

/**
* \brief Getter function to obtain the frequency separation between the constellation points.
 * @return separation between consecutive frequencies
 * 
*/
 int FSK::getSeparation() const{
        
        return seperation;
}
 
/**
* \brief Getter function 
 * @return The value of the FSK size to be used for modulation a nd demodulation
 * 
*/ 
int FSK::getSize() const{
            
            return FSK_Size;
        }
/**
* \brief Getter Function for the selected period.
 * @return signal period
 * 
*/
  unsigned int FSK::getPeriods() const
  {
            
            return periods;
        }
        /**
* \brief Getter function for the sampling rate used in the algorithm.
 * @return sample rate
 * 
*/
  float FSK::getSamplingRate() const
  {
            return sample_rate;
  }
  
    /**
  * /\brief Getter function .to obtain the central frequency of the modulation scheme.
  * @return float center frequency
  * 
*/
  
  float FSK::getFrequency() const
  {
      return frequency;
  }