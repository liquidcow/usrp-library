#include "tx_usrp.hpp"
#include "rx_usrp.hpp"
#include <iostream>

using namespace std;

#ifndef USRP_HPP
#define USRP_HPP

/**
* \class usrp
* \brief proxy/wrapper interface for managing tx and rx usrp devices as well as thread management of the devices. 
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \modified Yves-Francois Rivard, Serge Mbamba, Noel Moyo 
* \version 2.0
* \date 20/11/2012
* @todo Implement the RX receiver unit 
*/
template <class T>
class usrp{

public:
	//constructor
	usrp();
	//set the sample rate
	void setSampleRate(double);
	//set center frequency
	void setCenterFrequency(double);
	//set the gain
	void setGain(double);
	//set daughterboard IF filter bandwidth
	void setIFBandwidth(double);
	//set daughterboard antenna
	void setAntenna(string);
	//set daughterboard subdevice
	void setSubdevice(string);
	//set clock reference (internal, external, mimo)
	void setClockReference(string);
	//set USRP args
	void setArgs(string);
	//set over-the-wire sample mode
	void setOTW(string);
	//initialise the USRP
	void init();
	//transmit a DataSet
	void pulse(DataSet<T> &w);
	//add a DataSet to be streamed
    void queue(DataSet<T> &d);
    //check if the device is streaming
    bool streaming();
    //start a sampling thread
	void startSampling();
    //get a sample out
    DataSet<T> sample();
    //query the size of the sample queue
    int RXQueueSize();
    // query the size of TX stream
    int TXQueueSize();
	//stop streaming
	void stop();
        //get the sample rate
        float getusrpSampleRate();
private:
	string args, ant, subdev, ref, otw;
    size_t spb;
    double rate, centerFreq, gain, bw;
    float ampl, sampleRate;
    bool initialised;
    bool stream;
    //thread* sThread;
    tx_usrp<T> TX;
    rx_usrp<T> RX;

};

/**
* /struct tx_streamPulse
* /brief structure for launching a thread of streaming a pulse
* /author Ashton Hudson, Mike Geddes, Matthew van der Velden
* /version 1.0
* /date 20/11/2012 
*/
template <class T>
struct tx_streamPulse
{
    tx_streamPulse(tx_usrp<T> &d , DataSet<T> &w): device(d),wave_table(w){}
    void operator()()
    {
        device.pulse(wave_table);      
    }
    tx_usrp<T> &device;
    DataSet<T> &wave_table;
};

/**
* \struct tx_streamLoop
* \brief structure for launching a thread to stream the queued up DataSets
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \version 1.0
* \date 20/11/2012 
*/
template <class T>
struct tx_streamQueue
{
    tx_streamQueue(tx_usrp<T> &d): device(d){}
   	void operator()()
    {
        device.stream();
    }
    tx_usrp<T> &device;
};

/**
* \struct rx_samplingThreadStruct
* \brief structure for launching a thread of sampling a rx_usrp
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \version 1.0
* \date 27/11/2012 
*/
template <class T>
struct rx_samplingThreadStruct
{
    rx_samplingThreadStruct(rx_usrp<T> &d): device(d){}
    void operator()()
    {	
        device.startSampling(); 
    }
    rx_usrp<T> &device;
};

/**
* Simple initialisation constructor. Creates a managed TX and RX USRP1 device 
*/
template <class T>
usrp<T>::usrp():
	TX(true),
	RX(true)
	{
}

/**
* Set the sampling rate of both the TX and RX USRP1 devices 
* @param r sampling rate
*/
template <class T>
void usrp<T>::setSampleRate(double r){
	TX.setSampleRate(r);
	RX.setSampleRate(r);
        sampleRate = r;
}

/**
* Set the center frequency of both the TX and RX USRP1 devices 
* @param c center frequency
*/
template <class T>
void usrp<T>::setCenterFrequency(double c){
	TX.setCenterFrequency(c);
	RX.setCenterFrequency(c);
}

/**
* Set the gain of both the TX and RX USRP1 devices 
* @param g gain
*/
template <class T>
void usrp<T>::setGain(double g){
	TX.setGain(g);
	RX.setGain(g);
}

/**
* Set the daughterboard IF bandwidth of both the TX and RX USRP1 devices 
* @param i IF bandwidth 
*/
template <class T>
void usrp<T>::setIFBandwidth(double i){
	TX.setIFBandwidth(i);
	RX.setIFBandwidth(i);
}

/**
* Manually define the antenna of both the TX and RX USRP1 devices 
* @param a antenna name
* @todo split this into TX and RX functions 
*/
template <class T>
void usrp<T>::setAntenna(string a){
	TX.setAntenna(a);
	RX.setAntenna(a);
}

/**
* Manually define the daughterboard subdevice of both the TX and RX USRP1 devices 
* @param d subdevice name
* @todo split this into TX and RX functions
*/
template <class T>
void usrp<T>::setSubdevice(string d){
	TX.setSubdevice(d);
	RX.setSubdevice(d);
}

/**
* Set the reference for the local oscillator both the TX and RX USRP1 devices 
* @param c reference (internal, external, mimo) 
*/
template <class T>
void usrp<T>::setClockReference(string c){
	TX.setClockReference(c);
	RX.setClockReference(c);
}

/**
* Manually define the arguments of both the TX and RX USRP1 devices 
* @param a args
* @todo split this into TX and RX functions
*/
template <class T>
void usrp<T>::setArgs(string a){
	TX.setArgs(a);
	RX.setArgs(a);
}

/**
* Set the over-the-wire sampling mode for both the TX and RX USRP1 devices 
* @param o otw parameter
*/
template <class T>
void usrp<T>::setOTW(string o){
	TX.setOTW(o);
	RX.setOTW(o);
}

/**
* Init wrapper to init the RX and TX units
*/
template <class T>
void usrp<T>::init(){
	TX.init();
	RX.init();
}

/**
* Poll the RX and TX USRP1 devices to see if they are streaming. I.e. safe to close down or are in operation
* @return boolean as to whether the device is in a streaming state or not
*/
template <class T>
bool usrp<T>::streaming(){
	//cout<<TX.streaming()<<"\t"<<RX.streaming()<<endl;
	if(TX.streaming() || RX.streaming()){
		return true;
	}else{
		return false;
	}
}

/**
* launches a threaded transmit pulse of a given dataset
* @param w pulse to be transmitted through the TX usrp
*/
template <class T>
void usrp<T>::pulse(DataSet<T> &w){
	//place in threaded streaming struct
	tx_streamPulse<float> pulse(TX,w);
	//start the thread
	thread t(boost::ref(pulse));
	//join the thread back to the main thread
	t.join();
}


/**
* adds a given DataSet to the queue of DataSets that are being streamed by the TX USRP. 
* @param d DataSet to be queued
*/
template <class T>
void usrp<T>::queue(DataSet<T> &d){
	//push the dataset onto the stream queue
	//cout<<"\tPacket size: "<<d.size()<<endl;
	TX.push_back(d);
	//place in threaded streaming struct
	tx_streamQueue<float> streamingThread(TX);
	//start the thread
	//thread t(boost::ref(streamingThread));   //TODO fix this reference issue
	thread t(streamingThread);
	//join the thread back to the main thread
	if(!TX.streaming()){
		t.join();
	}
}

/**
* Send stop commands to both the TX and RX USRP1 devices.
* Blocks the main thread until the devices are shutdown safely. This method prevents a segmentation fault.
*/
template <class T>
void usrp<T>::stop(){
	TX.stop();
	RX.stop();
	//wait for the devices to stop streaming
	while(TX.streaming()){
		boost::this_thread::sleep(boost::posix_time::seconds(1));
	}
	while(RX.streaming()){
		boost::this_thread::sleep(boost::posix_time::seconds(1));
	}
	//wait for the devices to give the ready to shutdown flag
	while(!RX.usrpOffline()){
		cout<<"Waiting for RX usrp shutdown.."<<endl;
		boost::this_thread::sleep(boost::posix_time::seconds(1));
	}
	while(!TX.usrpOffline()){
		cout<<"Waiting for TX usrp shutdown.."<<endl;
		boost::this_thread::sleep(boost::posix_time::seconds(1));
	}
}

/**
* triggers a thread to be launched that contineouesly samples the input of the RX USRP 
* @todo fix the dangling thread issue.
*/
template <class T>
void usrp<T>::startSampling(){
	rx_samplingThreadStruct<T> s(RX);
	thread t(s);
}
    
/**
* get a DataSet off the front of the queue of samples from the RX USRP
* @return a DataSet of one "packet" sampled from the USRP. Size is dictated by the buffer size of the USRP. 4096 by default.
*/
template <class T>
DataSet<T> usrp<T>::sample(){
	return RX.sample();
}

/**
* Query the size of the queue of samples the RX USRP has stored up. 
* @return integer of the queue size.
*/
template <class T>
int usrp<T>::RXQueueSize(){
	return RX.RXQueueSize();
}

/**
* Query the size of the queue of samples the TX USRP still has to transmit. 
* @return integer of the queue size.
*/
template <class T>
int usrp<T>::TXQueueSize(){
	return TX.sampleQueueSize();
}

/**
* Query the set sample rate of the device 
* @return used sample rate
*/
template <class T>
float usrp<T>::getusrpSampleRate()
{
    
    return sampleRate;
}
#endif
