#include <memory>
#include "usrp.hpp"
#include "DataSet.hpp"
#include <cstdlib>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>

#ifndef TRANSMISSIONHANDLER_HPP
#define	TRANSMISSIONHANDLER_HPP

enum Streaming {Queue, Pulse};

/**
* \class TransmissionHandler
* \brief Class used as an interface for the usrp transmission.
* \author Serge Mbamba, Noel Moyo, Yves-Francois Rivard
* \version 1.0
* \date 28 November 2013, 10:52 AM
* @todo Implement pulse transmission
*/
template <class T>
class TransmissionHandler {
public:
    TransmissionHandler(usrp< T >& );
    TransmissionHandler(const TransmissionHandler& orig);
    virtual ~TransmissionHandler();
    void send(Streaming stream, DataSet <T> & );
     void scale_downAmplitude();
    bool abort();
    int kbhit();
private:
    char control_bit;
    float Scale;
    usrp <T> *   myUSRP_TX;
    DataSet <T> * myDATA_to_send;
};

/**
* \brief Transmission constructor
* @param dev usrp device
* @param data Set of data to be outputted
*/
template <class T>
TransmissionHandler<T>::TransmissionHandler( usrp<T>& dev):
        control_bit('-'),
        Scale(1),
        myUSRP_TX(NULL),
        myDATA_to_send(NULL)
  {
    myUSRP_TX=&dev;      
  }
/** \fn Constructor
 * @param orig TransmisionHandler argument
 */
template <class T>
TransmissionHandler<T>::TransmissionHandler(const TransmissionHandler& orig) {
}

/* \fn Destructor
 */
template <class T>
TransmissionHandler<T>::~TransmissionHandler() {
}

/**
* \brief function used to output the data
* 
*/
template <class T>
void TransmissionHandler<T>::send(Streaming stream, DataSet<T>&data){
    

   myDATA_to_send=&data;
   scale_downAmplitude();
   
  if (myUSRP_TX->getusrpSampleRate()==myDATA_to_send->getSamplingRate())
  {    
      int i = 0;
      
      if (stream == Queue)
      {
          while (!abort() && i < 500)
          {
              myUSRP_TX->queue(*myDATA_to_send);
              i++;
          }
      }
      
      else if (stream == Pulse)
      {
          while (!abort() && i < 1)
          {
              myUSRP_TX->pulse(*myDATA_to_send);
              i++;
          }
      }
      
        myUSRP_TX->stop();
    }
  else
   throw DeviceRateAndDataRateMismatch();
}

  /**
  * \brief function used to check for a keyboard hit
 * 
*/
template <class T>
int TransmissionHandler<T>::kbhit()
{
    struct termios first,last;
    int gh,fcfirst;
    tcgetattr(STDIN_FILENO,&last);
    first=last;
    first.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO,TCSANOW, &last);
    fcfirst=fcntl(STDIN_FILENO,F_GETFL,0);
    
    fcntl(STDIN_FILENO,F_SETFL,fcfirst|O_NONBLOCK);
            
    gh= getchar();
    tcsetattr(STDIN_FILENO,TCSANOW,&first);
    fcntl(STDIN_FILENO,F_SETFL,fcfirst);
    
    
    if(gh!=EOF)
    {
        gh=getc(stdin);
        control_bit=char(gh);        
        return true;   
    }
    else
    {
    
       return false;
    }
    return false;
}

/**
* \brief function used to abort the transmission procedure
* \return bool true if the key a was pressed
*/

template <class T>
bool TransmissionHandler<T>::abort(){
    if (control_bit=='a') return true;
    kbhit();
       return  (control_bit=='a');
}

/**
* \brief function used to control the amplitude levels and prevent saturation of the usrp device
*/
template <class T>
void TransmissionHandler<T>::scale_downAmplitude(){
   float DataMax=myDATA_to_send->max(); //<<endl;
   float scale=1/DataMax;
   myDATA_to_send->multiply_by_const(scale);
}

#endif	/* TRANSMISSIONHANDLER_HPP */