#include <queue>
#include <cmath>
#include <iostream>
#include <fstream>
#include <boost/thread.hpp>

#ifndef FRAMEBUILDER_H
#define FRAMEBUILDER_H

/**
* \class FrameBuilder
* \brief A preprocessor class for signal processing of the raw USRP ouput. Effectively does packet energy analysis, those below a threshold are dumped. The object is modelled as a queue, raw packets pushed on, and processed packets popped off.
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \version 1.0
* \date 13/12/2012 
*/
template <class T>
class FrameBuilder{
	public:
		FrameBuilder(float s, int g);
		void push_back(DataSet<T> packet);
		DataSet<T> pop();
		int frameQueueSize();
		~FrameBuilder();
	private:
		queue<DataSet<T> > inputQueue;
		queue<DataSet<T> > frameQueue;
		float packetEnergy(DataSet<T> &packet);
		float max(DataSet<T> &packet);
		float sensitivity;
		int packetGranularity;
		bool frameToggle;
		int count;
		ofstream outfile;
};

/**
* \struct packetEnergyCalculator
* \brief structure for threaded packet energy calculation
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \version 1.0
* \date 20/11/2012 
*/
template <class T>
struct packetEnergyCalculator
{
    packetEnergyCalculator(){
    }
    void operator()()
    {}
};

/**
* Constructor to set up the FrameBuilder object. 
* @param s the energy threshold for dumping packets
* @param g the granularity of which energy analysis is done. Reducing increases performance but also reduces accuracy of analysis resulting in potential packet loss.
*/
template <class T>
FrameBuilder<T>::FrameBuilder(float s,int g):
	sensitivity(s),
	frameToggle(false),
	packetGranularity(g)
	{
		outfile.open("energy_plot.m");
		outfile<<"x=[";
}

/**
* Helper function to determine the energy of a packet. 
* @param &packet the packet to check
* @return float
*/
template <class T>
float FrameBuilder<T>::packetEnergy(DataSet<T> &packet){
	float energy = 0;
	int increment = round(packet.size()/packetGranularity);
	for(int i = 0; i <packet.size()-1;i=i+increment){
		energy = energy + sqrt(packet[i].real()*packet[i].real()+packet[i].imag()*packet[i].imag());	
	}
	energy=energy/packet.size();
	if(energy > sensitivity){
		return energy;
	}
	return 0;
}


/**
* Helper function to determine the energy of a packet. 
* @param &packet the packet to check
* @return float
*/
template <class T>
float FrameBuilder<T>::max(DataSet<T> &packet){
	float max = 0;
	for(int i = 0; i <packet.size()-1;i++){
		float magnitude = sqrt(packet[i].real()*packet[i].real()+packet[i].imag()*packet[i].imag());
		if(magnitude > max){
			max = magnitude;
		}
	}
	return max;
}

/**
* Function to raw packets onto the processing queue
* @param &packet the raw packet to be queued
*/
template <class T>
void FrameBuilder<T>::push_back(DataSet<T> packet){
	outfile<<max(packet)<<"\n"<<endl;
	if(frameToggle){
		if(max(packet) >= sensitivity){
			inputQueue.push(packet);
		}else{
			frameToggle = false;
			//build a new frame from the packet queue
			DataSet<T> newFrame = inputQueue.front();
			inputQueue.pop();
			int queueSize = inputQueue.size()-1;
			while (!inputQueue.empty()){
				newFrame.concatenate(inputQueue.front());
				inputQueue.pop();
		    }
			frameQueue.push(newFrame);
			cout<<"finished building frame, size: "<<newFrame.size()<<endl;
		}		
	}else{
		if(max(packet) >= sensitivity){
			cout<<"started building frame."<<endl;
			frameToggle = true;
			inputQueue.push(packet);
		}
	}
}

/**
* Function to query the size of the processed packets queue 
* @return int of number of packets in queue
*/
template <class T>
int FrameBuilder<T>::frameQueueSize(){
	return frameQueue.size();
}

/**
* Function to pop the front packet off the processed queue
* @return DataSet<T> 
*/
template <class T>
DataSet<T> FrameBuilder<T>::pop(){
	DataSet<T> temp = frameQueue.front();
	frameQueue.pop();
	return temp;
}

template <class T>
FrameBuilder<T>::~FrameBuilder()
{
	outfile<<"];\nplot(x);";
}

#endif