#include "Function.hpp"
//#include <random>
#include <ctime>
#include <boost/random.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>

using namespace std;
using namespace boost;

#ifndef NOISE_HPP
#define NOISE_HPP
/**
* \class Noise
* \brief Noise is a class that is akin to creating random numbers
*
* Noise allows for a user to create an amplitude capped noise signal. 
* the amplitude you hand to the noise function is the max value, not the 
* average value. typically, a value of 1.0 will offer a decent amount of noise for
* a sine wave with the same wave. 
* 
* Noise is a Gaussian distribution, and so one must expect white Gaussian
* noise at the output! 
* 
* \author Ashton Hudson
* \version 1.0
* \date 3/12/2012
*
*/
template<class T> 
class Noise : public Function<T>
{
public: 
	Noise(T amplitude=1.0, float mean=0.0, float variance=1.0); 
	T operator () (T input=0); 
private: 
	T amplitude; 
	normal_distribution<T> nd;
	mt19937 gen;
	variate_generator<mt19937&, normal_distribution<T> > norm_dis; 
}; 

template<class T>
Noise<T>::Noise(T amplitude, float mean, float variance): amplitude(amplitude), nd(mean,variance),gen(time(0)), norm_dis(gen, nd)
{}
template<class T>
T Noise<T>::operator() (T input)
{
	T d = norm_dis()*amplitude;
	return d; 
}

#endif 
