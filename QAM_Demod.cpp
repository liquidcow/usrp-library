#include "QAM_Demod.h"
#include "Correlator.hpp"

/**
* \brief Constructor for QAM class
* @param M-ary this is the size of the constellation the user wants to use
* @param spp this is the symbols per pusle rate
*/
QAM_Demod::QAM_Demod(QAM& myQAM, float erasure_cutoff, float phaseOffset): QAM_Size(myQAM.getSize()), erasure_cutoff(erasure_cutoff), frequency(myQAM.getFrequency()), periods(myQAM.getPeriod()), sample_rate(myQAM.getSampleRate()), phase(phaseOffset), constellation_structs(myQAM.getStructs())
{
	pi = 4*atan(1.0); 

        MQAM_Demod(); 
}		

/**
* @param inf DataSet to be demodulated
*/
void QAM_Demod::push_back(DataSet<float> inf)
{
	//empty dataset check
	if(inf.size() <= 0){
		string nothing = "";
		stringQueue.push(nothing);
	}else{
           //  inf.scale_upAmplitude();
		vector<unsigned> temp;
		int count =1;
		unsigned char TempChar;
		string tempString; 
		float sample_slice_size = (sample_rate/frequency)*periods;
                  
		for (int i=0; i<(inf.size()/sample_slice_size); i++){
                  temp.push_back(QAM_Decoder(inf.slice(i*sample_slice_size,(i+1)*sample_slice_size)));
			 if (count ==ceil (8/log2(QAM_Size)))
				{
                                    
					TempChar = getChar(temp, log2(QAM_Size));
					tempString+=TempChar; 
					count =0;  
					temp.clear();
				}
			count++;

		}
		stringQueue.push(tempString); // NOTE! must have an erasure character that handles negatives. 
	}
}		


/**
* @return demodulated string 
*/
string QAM_Demod::pop()
{
	 string D = stringQueue.front(); 
	 stringQueue.pop();
	 return D;
}

/**
* \fn set_erasure is a function that allows for a dymanic change in the erasure zone. 
* @param updated_erasure New erasure value
*/
void QAM_Demod::set_erasure(float updated_erasure)
{
	erasure_cutoff = updated_erasure;  
}

/**
* \fn QAM_Demod intializes M-QAM demodulation techniques if the user desires that 
*
* We will only compute 2 correlations to find out the right symbol, and so we only need a sine and a cosine
* wave. 
*/
void QAM_Demod::MQAM_Demod()
{

	// assigning the sine and cosine waves, this will be used to decompose the signals into their
	// corresponding components. 
	Sinusoid<float> sine(2.0, frequency, phase);
	Sinusoid<float> cosine(2.0, frequency, (pi/2)-phase); 


	DataSet<float> sine_data(sine,sample_rate,0.0, periods/frequency);
	sine_data.populate(); 
 
	DataSet<float> cosine_data(cosine,sample_rate, 0.0, periods/frequency); 
	cosine_data.populate(); 


	// pushing back onto the constellation the two components. 
	constellation.push_back(cosine_data); 
	constellation.push_back(sine_data);

}

/**
* \fn QAM_Decoder is a function that will take a symbol and then use correlation to find it's sine and cosine components
* This function is very efficient in that is only does two correlations and from there it finds the correct symbol. 
* 
* erasure boundaries are built into the system allowing for on the fly erasure manipulation! The decoder also features a 
* 
* note that the function requires a normalized signal to be useful! So the peak amplitudes must be 3, and the low amplitudes to be
* 1. This is part of the demodulation algorithm, sorry :(; 
*
*/
int QAM_Demod::QAM_Decoder(DataSet<float> input)
{   

	vector<float> points; 
	for(int i= 0; i<2; i++)
	{
		float temp = constellation[i].correlate(input); 
            
		points.push_back(temp); 
	}
        constellation_point P;
        P ("", points[0], points[1]);
        
        double min_distance=INT_MAX;
        int index=-1;
        for (int i=0; i<constellation_structs.size(); i++)
        {
            long double d=Distance(P, constellation_structs[i]);
            
            if (min_distance>d)
            {
                min_distance=d;
                index=i;
            }
        }

        return index;
}
/**
 * 
 * @param constellation_point first_Point First point
 * @param constellation_point sec_Point   Second point
 * @return long double The distance between two points
 */
long double QAM_Demod::Distance(constellation_point first_Point, constellation_point sec_Point)
{
    return sqrt(pow(first_Point.x_amplitude-sec_Point.x_amplitude,2)+pow(first_Point.y_amplitude-sec_Point.y_amplitude,2));
}

