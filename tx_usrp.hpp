/// USRP wrapper class - tx usrp1
/**
* \brief USRP tx Wrapper class, interface between ursp class and uhd driver 
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden 
*
* \date 26/11/2012
*/

#include "DataSet.hpp"
#include "Exceptions.hpp"
#include <iostream>
#include <queue>

#include <uhd/utils/thread_priority.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/utils/static.hpp>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/exception.hpp>
#include <boost/program_options.hpp>
#include <boost/math/special_functions/round.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <iostream>
#include <complex>
#include <csignal>
#include <cmath>
#include <string>

#ifndef TX_USRP_HPP
#define TX_USRP_HPP

using namespace boost;
using namespace std;

template <class T>
class tx_usrp{

public:
	//constructor
	tx_usrp();
        //verbose constructor
        tx_usrp(bool);
	//set the sample rate
	void setSampleRate(double);
	//set center frequency
	void setCenterFrequency(double);
	//set the gain
	void setGain(double);
	//set daughterboard IF filter bandwidth
	void setIFBandwidth(double);
	//set daughterboard antenna
	void setAntenna(string);
	//set daughterboard subdevice
	void setSubdevice(string);
	//set clock reference (internal, external, mimo)
	void setClockReference(string);
	//set USRP args
	void setArgs(string);
	//set over-the-wire sample mode
	void setOTW(string);
	//initialise the USRP
	void init();
	//transmit a DataSet once
	void pulse(DataSet<T> wave_table);
    //add a DataSet to be streamed
    void push_back(DataSet<T> &d);
    //start the stream of the queued datasets
    void stream();
    //check if the device is streaming
    bool streaming();
    // check sample size
    int sampleQueueSize();
    //check if the device is ready for shutdown
    bool usrpOffline();
	//stop streaming
	void stop();

private:
	string args, ant, subdev, ref, otw;
    size_t spb;
    double rate, centerFreq, gain, bw;
    float ampl;
    bool initialised;
    uhd::usrp::multi_usrp::sptr usrpDevice;
    bool streamFlag, streamThreadFlag, readyForShutDown;
    bool verbose;

    //transmission variables
    double step;
    queue<DataSet<T> > streamQueue;
    uhd::tx_streamer::sptr tx_stream;
    vector<complex<float> > buff;
    vector<complex<float> > buff2;
    vector<complex<float> *> buffs;
    vector<complex<float> *> buffs2;
    uhd::tx_metadata_t md;
};

/**
* /struct fillBuffer
* /brief structure for threaded filling of a buffer
* /author Ashton Hudson, Mike Geddes, Matthew van der Velden
* /version 1.0
* /date 20/11/2012 
*/
template <class T>
struct fillBuffer
{
    fillBuffer(vector<complex<T> > &buff, DataSet<T> &wave_table, long &index, size_t bufferSize){
        buff.clear();
        long tableSize = wave_table.size()-2;   //stop condition correction.
        for(long n = 0; n < bufferSize; n++){
            if(index < tableSize){
                index++;
            }else{
                break;
            }
            buff.push_back(wave_table[index]);
        }   
    }
    void operator()()
    {

    }
};


/** \fn Constructor
 */
template <class T>
tx_usrp<T>::tx_usrp():
	args(""),					//USRP args are default ""
	ant("TX/RX"), 				//recommended antenna
	subdev(""),				    //default subdevice is "" 
	ref("internal"),			//default  USRP Clock is the internal clock 
	otw("sc16"),				//default over-the-wire sample mode is sc16
    spb(0),					    //default samples per buffer 0;
    rate(2500000),				//default sample rate of 250k hz 
    centerFreq(1000000), 		//default center frequency of 3Mhz
    gain(0),					//default USRP gain of 0dB  
    bw(600000),				    //default IF bandwidth of 60MHz
   	initialised(false),			//the init flag starts as false
    streamFlag(true),
    streamThreadFlag(true),
    readyForShutDown(true),
    verbose(false)
        {

}


/** \fn Verbose Constructor
 */
template <class T>
tx_usrp<T>::tx_usrp(bool v):
    args(""),                   //USRP args are default ""
    ant("TX/RX"),               //recommended antenna
    subdev(""),                 //default subdevice is "" 
    ref("internal"),            //default  USRP Clock is the internal clock 
    otw("sc16"),                //default over-the-wire sample mode is sc16
    spb(0),                     //default samples per buffer 0;
    rate(2500000),              //default sample rate of 250k hz 
    centerFreq(1000000),        //default center frequency of 3Mhz
    gain(0),                    //default USRP gain of 0dB  
    bw(600000),                 //default IF bandwidth of 60MHz
    initialised(false),         //the init flag starts as false
    streamFlag(true),
    streamThreadFlag(true),
    readyForShutDown(true),
    verbose(v)
        {

}

/**
 * \brief set the sample rate
 * @param r double sample rate value
 */
template <class T>
void tx_usrp<T>::setSampleRate(double r){
    if(!initialised){
        if(r < 1) 
            throw InvalidUSRPSetting();
        else
            rate = r;
    }else{
        throw USRPAlreadyInitialised();
    }
}


/**
 * \brief set center frequency
 * @param c double center frequency
 */
template <class T>
void tx_usrp<T>::setCenterFrequency(double c){
    if(!initialised){
        if(c < 0 || c > 249000000) 
            throw InvalidUSRPSetting();
        else
            centerFreq = c;
    }else{
        throw USRPAlreadyInitialised();
    }
}

/**
 * \brief set the gain
 * @param g double gain value
 */
template <class T>
void tx_usrp<T>::setGain(double g){
    if(!initialised){
        if(g < -20 || g > 20)
            throw InvalidUSRPSetting();
        else
            gain = g;
    }else{
        throw USRPAlreadyInitialised();
    }
}

/**
 * \brief check if the device is streaming
 * @return boolean value
 */
template <class T>
bool tx_usrp<T>::streaming(){
    if(streamQueue.size()>0){
        return true;
    }else{
        return streamFlag;    
    }
    
}

/**
 * \brief set daughter-board IF filter bandwidth
 * @param b double bandwidth of the filter
 */
template <class T>
void tx_usrp<T>::setIFBandwidth(double b){
    if(!initialised){
        if(b < 0 || b > 249000000)
            throw InvalidUSRPSetting();
        else
            bw = b;
    }else{
        throw USRPAlreadyInitialised();
    }
}

/**
 * \brief set USRP args
 *@param a string args
 */
template <class T>
void tx_usrp<T>::setArgs(string a){
    if(!initialised){
        args = a;
    }else{
        throw USRPAlreadyInitialised();
    }
}


/**
 * \brief set daughter-board antenna
 * @param a string antenna
 */
template <class T>
void tx_usrp<T>::setAntenna(string a){
    if(!initialised){
        ant = a;
    }else{
        throw USRPAlreadyInitialised();
    }
}

/**
 * \brief set daughterboard subdevice
 * @param s string of subdevice
 */
template <class T>
void tx_usrp<T>::setSubdevice(string s){
    if(!initialised){
        subdev = s;
    }else{
        throw USRPAlreadyInitialised();
    }
}

/**
 * \brief set over-the-wire sample mode
 * @param o otw parameter
 */
template <class T>
void tx_usrp<T>::setOTW(string o){
    if(!initialised){
        otw = o;
    }else{
        throw USRPAlreadyInitialised();
    }
}


/**\brief set clock reference (internal, external, mimo)
 * @param r reference string
 */
template <class T>
void tx_usrp<T>::setClockReference(string r){
    if(!initialised){
        if(r != "internal" || r != "external" || r != "mimo")
            throw InvalidUSRPSetting();
        else
            ref = r;
    }else{
        throw USRPAlreadyInitialised();
    }
}


/**
 * \brief initializes the USRP
 */
template <class T>
void tx_usrp<T>::init(){
	if(!initialised){
    	//create the USRP device
    	if(verbose) cout<<endl<< boost::format("Creating the usrp device with: %s...") % args<<endl;
    	usrpDevice = uhd::usrp::multi_usrp::make(args);

    	//Lock mboard clocks
    	usrpDevice->set_clock_source(ref);

    	//set the sub device if specified
    	if (subdev != "") usrpDevice->set_tx_subdev_spec(subdev);
    	if(verbose) cout<< boost::format("Using Device: %s") % usrpDevice->get_pp_string() <<endl;

    	//set the sample rate
    	if(verbose) cout << boost::format("Setting TX Rate: %f Msps...") % (rate/1e6) <<endl;
    	usrpDevice->set_tx_rate(rate);
    	if(verbose) cout<< boost::format("Actual TX Rate: %f Msps...") % (usrpDevice->get_tx_rate()/1e6) <<endl<<endl;

    	//setup the channels
    	for(size_t chan = 0; chan < usrpDevice->get_tx_num_channels(); chan++) {
	    	//set the center frequency
	        if(verbose) cout<< boost::format("Setting TX Freq: %f MHz...") % (centerFreq/1e6) <<endl;
	        usrpDevice->set_tx_freq(centerFreq, chan);
	        if(verbose) cout << boost::format("Actual TX Freq: %f MHz...") % (usrpDevice->get_tx_freq(chan)/1e6) <<endl<<endl;

	        //set the rf gain
	        if(verbose) cout << boost::format("Setting TX Gain: %f dB...") % gain <<endl;
	        usrpDevice->set_tx_gain(gain, chan);
	        if(verbose) cout << boost::format("Actual TX Gain: %f dB...") % usrpDevice->get_tx_gain(chan) <<endl <<endl;

	        //set the IF filter bandwidth
	        if(verbose) cout << boost::format("Setting TX Bandwidth: %f MHz...") % bw <<endl;
	        usrpDevice->set_tx_bandwidth(bw, chan);
	        if(verbose) cout << boost::format("Actual TX Bandwidth: %f MHz...") % usrpDevice->get_tx_bandwidth(chan) <<endl<<endl;
	        
	        //set the antenna
	        usrpDevice->set_tx_antenna(ant, chan);
    	}

    	//allow for some hardware setup time

    	boost::this_thread::sleep(boost::posix_time::seconds(1));
        
        //create a transmit streamer
        //linearly map channels (index0 = channel0, index1 = channel1, ...)
        uhd::stream_args_t stream_args("fc32", otw);
        for (size_t chan = 0; chan < usrpDevice->get_tx_num_channels(); chan++)
            stream_args.channels.push_back(chan); //linear mapping
        tx_stream = usrpDevice->get_tx_stream(stream_args);

        //allocate a  double buffer which we re-use for each channel
        if (spb == 0) spb = tx_stream->get_max_num_samps();
        
        //buffer 1
        vector<complex<float> > tempBuff(spb);
        buff = tempBuff;
        vector<complex<float> *>tempBuffs(usrpDevice->get_tx_num_channels(), &buff.front());
        buffs = tempBuffs;
        //buffer 2
        vector<complex<float> > tempBuff2(spb);
        buff2 = tempBuff;
        vector<complex<float> *>tempBuffs2(usrpDevice->get_tx_num_channels(), &buff2.front());
        buffs2 = tempBuffs2;

        cout<<"num channels:"<<usrpDevice->get_tx_num_channels()<<endl;

        //setup the metadata flags
        md.start_of_burst = true;
        md.end_of_burst   = false;
        md.has_time_spec  = true;
        md.time_spec = uhd::time_spec_t(0.1);

        //cout << boost::format("Setting device timestamp to 0...") <<endl;
        usrpDevice->set_time_now(uhd::time_spec_t(0.0));

        //Check Ref and LO Lock detect
        vector<string> sensor_names;
        sensor_names = usrpDevice->get_tx_sensor_names(0);
        if (find(sensor_names.begin(), sensor_names.end(), "lo_locked") != sensor_names.end()) {
            uhd::sensor_value_t lo_locked = usrpDevice->get_tx_sensor("lo_locked",0);
            if(verbose) cout << boost::format("Checking TX: %s ...") % lo_locked.to_pp_string() <<endl;
            UHD_ASSERT_THROW(lo_locked.to_bool());
        }
        sensor_names = usrpDevice->get_mboard_sensor_names(0);
        if ((ref == "mimo") and (find(sensor_names.begin(), sensor_names.end(), "mimo_locked") != sensor_names.end())) {
            uhd::sensor_value_t mimo_locked = usrpDevice->get_mboard_sensor("mimo_locked",0);
            if(verbose) cout << boost::format("Checking TX: %s ...") % mimo_locked.to_pp_string() <<endl;
            UHD_ASSERT_THROW(mimo_locked.to_bool());
        }
        if ((ref == "external") and (std::find(sensor_names.begin(), sensor_names.end(), "ref_locked") != sensor_names.end())) {
            uhd::sensor_value_t ref_locked = usrpDevice->get_mboard_sensor("ref_locked",0);
            if(verbose) cout << boost::format("Checking TX: %s ...") % ref_locked.to_pp_string() <<endl;
            UHD_ASSERT_THROW(ref_locked.to_bool());
        }

        cout<<"TX USRP Initialised."<<endl; 

	}else
		throw USRPAlreadyInitialised();
	initialised = true;
}


/**
 * \brief //abrogate the streaming the dataset
 */
template <class T>
void tx_usrp<T>::stop(){
    streamFlag = false;
}



/**
 * \brief loop the DataSet to be transmitted
 * @param wave_table dataset to be transmitted
 */
template <class T>
void tx_usrp<T>::pulse(DataSet<T> wave_table){

    long tableSize = wave_table.size()-2; //stop condition correction.
    streamFlag = true;
    long i = 0;

    while(streamFlag){
         
        for(size_t n = 0; n < buff.size(); n++){
            if(i < tableSize){
                i++;
            }else{
                streamFlag = false;
                break;
            }
            buff[n] = wave_table[i];
        }
        //send the entire contents of the buffer
        tx_stream->send(buffs, buff.size(), md);

        md.start_of_burst = false;
        md.has_time_spec = false;
        
        //send a mini EOB packet
        //md.end_of_burst = true;
        //tx_stream->send("", 0, md);   
    }
}

template <class T>
void tx_usrp<T>::push_back(DataSet<T> &d){
    //cout<<"\tPacket size: "<<d.size()<<endl;
    streamQueue.push(d);
}

/**
 * \brief starts the stream of the queued data set
 */
template <class T>
void tx_usrp<T>::stream(){
    readyForShutDown = false;
    if(streamThreadFlag){
        streamThreadFlag = false;
        if(verbose) cout<<"TX streaming started."<<endl;
        bool toggleBuffer = true;
        int dataCount = 0;
        int toggleCount = 0;
        streamFlag = true;

        if(streamQueue.size()>1){
            long i = 0;
            //preload the second buffer
            DataSet<T> &currentTemp = streamQueue.front();
            fillBuffer<float> b(buff2, currentTemp, i, spb);
            thread bufferThread(b);
            bufferThread.join();

            while(streamFlag || streamQueue.size()>0){
                if(toggleBuffer){
                    DataSet<T> &current = streamQueue.front(); //potential optimization
                    //load the primary buffer
                    fillBuffer<float> a(buff, current, i, spb);
                    thread bufferThread(a);
                    //send the entire contents of the secondary buffer
                    tx_stream->send(buffs2, buff2.size(), md);
                    md.start_of_burst = false;
                    md.has_time_spec = false;

                    toggleBuffer = false;
                    bufferThread.join();
                }else{
                    DataSet<T> &current = streamQueue.front();
                    //load the secondary buffer
                    fillBuffer<float> b(buff2, current, i, spb);
                    thread bufferThread(b);
                    //send the entire contents of the primary buffer
                    tx_stream->send(buffs, buff.size(), md);
                    md.start_of_burst = false;
                    md.has_time_spec = false;

                    toggleBuffer = true;
                    bufferThread.join();
                }
                toggleCount++;
                if(i == streamQueue.front().size()-2){
                    dataCount++;
                    toggleCount = 0;
                    streamQueue.pop();
                    i = 0;

                }
            }
            streamFlag = false;
        }
        streamThreadFlag = true;
    }
    readyForShutDown = true;
}

/**
 * return integer value of Queue size 
 */
template <class T>
int tx_usrp<T>::sampleQueueSize(){
    return streamQueue.size();
}

/**
 * return boolean to verify shutdown mode
 */
template <class T>
bool tx_usrp<T>::usrpOffline(){
    return readyForShutDown;
}   

#endif
