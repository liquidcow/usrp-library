#include "DataSet.hpp"
#include <algorithm>
#include "PolynomialField.hpp"
#include "Analog_Prototype.h"

/* 
 * File:   Filter.h
 * Author: sakilo
 *
 * Created on 17 January 2014, 10:06 AM
 */

#ifndef DISCRETE_FILTER_H
#define	DISCRETE_FILTER_H

/**
* \class Discrete_Filter
* \brief Discrete filter class to filter a DataSet sequence
* 
* This class is used to implement discrete time filtering on a data sequence. The bilinear transformation method for IIR filtering is implemented (without pre-warping)
*
* \author Yves-Francois Rivard, Serge Mbamba, Noel Moyo
* \version 1.0
* \date 18/01/2014 
*
*  @TODO implement pre-warping, FIR filters, impulse invariance, step invariance, etc
*/

class Discrete_Filter{
        public:
            Discrete_Filter (double sampling_rate);
            DataSet <float> sequence_filtering(const DataSet<float>& myInput);
            void z_to_time ();
            void bilinear_transform (const vector <float>& continuous_numerator, const vector <float>& continuous_denominator);
            vector <transformation_data>  getTf_numerator() const;
            vector <transformation_data>  getTf_denominator() const;
            vector <float> Z_coefficientsComputator(const vector <transformation_data>& ) const;
            void display_z_coefficients();
            void display_time_coefficients();

            
private:
            double _sampling_rate;
            
            vector <float> time_coefficients_x; //time_coefficients_x coefficient to multiply the input terms (coefficients of x(n+i) terms)
            vector <float> time_coefficients_y; //time_coefficients_y coefficient to multiply the output terms  (coefficients of y(n+i) terms)
            
            vector <transformation_data> tf_numerator;
            vector <transformation_data> tf_denominator ;
            
            vector <float> z_numerator;
            vector <float> z_denominator;
};

#endif	/* DISCRETE_FILTER_H */