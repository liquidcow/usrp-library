#include <exception>

#ifndef EXCEPTIONS_HPP
#define EXCEPTIONS_HPP

using namespace std;

/** 
* 	\brief DataSet has not been populated
*/
class EmptyDataSet: public exception
{
  virtual const char* what() const throw()
  {
    return "DataSet has not been populated";
  }
};

/** 
* 	\brief Given index is not valid for this DataSet
*/
class InvalidIndex: public exception
{
  virtual const char* what() const throw()
  {
    return "Given index is not valid for this DataSet";
  }
};

/** 
* 	\brief Given time scale is invalid
*/
class InvalidTimeDuration: public exception
{
  virtual const char* what() const throw()
  {
    return "Given time scale is invalid";
  }
};

/** 
* 	\brief Given sample size is not valid
*/
class InvalidSampleSize: public exception
{
  virtual const char* what() const throw()
  {
    return "Given sample size is not valid";
  }
};

/** 
* 	\brief Given parameters are not valid for this sinusoid
*/
class InvalidSinusoidParameters: public exception
{
  virtual const char* what() const throw()
  {
    return "Given parameters are not valid for this sinusoid";
  }
};

/** 
* 	\brief Given parameters are not valid for this sinusoid
*/
class DataSetSizeMismatch: public exception
{
  virtual const char* what() const throw()
  {
    return "Given parameters are not valid for this sinusoid";
  }
};

/** 
* 	\brief USRP settings cannot be changed once the device is initialised
*/
class USRPAlreadyInitialised: public exception
{
  virtual const char* what() const throw()
  {
    return "USRP settings cannot be changed once the device is initialised";
  }
};

/** 
* 	\brief The given setting was invalid for the USRP1 device
*/
class InvalidUSRPSetting: public exception
{
  virtual const char* what() const throw()
  {
    return "The given setting was invalid for the USRP1 device";
  }
};

/** 
* 	\brief M_size must be less than equal to 8
*/
class InvalidNumOfBits: public exception
{
  virtual const char* what() const throw()
  {
    return "M_size must be less than equal to 8";
  }
};


/**
*  	\brief This is a class that is thrown when either an unavalible QAM is selected or a non-existance QAM is selected
*/
class notAvaliableQAM: public exception
{
  virtual const char* what() const throw()
  {
    return "The QAM constellation size chosen is either unavailble or non-existant, cry in a corner or learn to use QAM properly";
  }
};

/**
*  	\brief This is a class that is thrown when either an unavailable FSK is selected or a non-existing FSK is selected
*/
class invalidFSK: public exception
{
  virtual const char* what() const throw()
  {
    return "The FSK constellation size chosen is non-existant, cry in a corner or learn to use FSK properly";
  }
};

/**
*  	\brief This is a class that is thrown when the RX USRP hardware sends a timeout flag
*/
class RXUSRPTimeOut: public exception
{
  virtual const char* what() const throw()
  {
    return "The RX USRP device has timed out";
  }
};

/**
* 	\brief This is an exception class that is thrown when DataSet has a timing error during population
*/
class TimingError: public exception
{
public:
  virtual const char* what() const throw()
  {
    return "A timing error has occured. This error was thrown to prevent a system hang.";
  }
};

/**
* 	\brief This is an exception class that is thrown when DataSet has a time mismatch during the execution of an operator
*/
class TimeMismatch: public exception
{
public:
  virtual const char* what() const throw()
  {
    return "A time mismatch error has occured during a DataSet's execution of an operator. I.e. start time and end time did not match. This error was thrown to prevent a system hang.";
  }
};

/**
* 	\brief This is an exception class that is thrown when a DataSet cannot be populated
*/
class DataSetNotReadyForPopulation: public exception
{
public:
  virtual const char* what() const throw()
  {
    return "An attempt was made to populate a DataSet that cannot be populated due to its configuration";
  }
};

/**
* 	\brief This is an exception class that is thrown when a DataSet cannot be populated
*/
class wrongBitGraySelection: public exception
{
public:
  virtual const char* what() const throw()
  {
    return "An attempt was made to create a gray encoding sequence of 0 bits";
  }
};

/**
* 	\brief This is an exception class that is thrown when the argument for the FSK size is an invalid number such as non integers and negatives.
*/

class WrongSizeValue: public exception
{
public:
  virtual const char* what() const throw()
  {
    return "The argument of the size of the FSK  required is invalid";
  }
};

/**
* 	\brief This is an exception class that is thrown when the range of frequencies selected includes negative values or values violating the nyquist rate.
*/

class invalidFSKfreqRange: public exception
{
public:
  virtual const char* what() const throw()
  {
    return "The selected FSk frequency range is wrong. Try changing the center frequency of frequency separation";
  }
};


/**
* 	\brief This is an exception class that is thrown when the range of frequencies selected includes negative values or values violating the nyquist rate.
*/

class DeviceRateAndDataRateMismatch: public exception
{
public:
  virtual const char* what() const throw()
  {
    return "Verify that the dataset sampling rate and the device sampling rate are the same.";
  }
};

 /**
* 	\brief This is an exception class that is thrown when the coherence input of the FSK transmitter is wrong
*/

class wrongFSKCoherenceInput: public exception
{
public:
  virtual const char* what() const throw()
  {
    return "Wrong FSK transmitter coherence selected";
  }
};

/**
* 	\brief This is an exception class that is thrown when the binomial expansion selected sign is wrong
*/

class wrongBinomialExpansionSign: public exception
{
public:
  virtual const char* what() const throw()
  {
    return "Wrong Binomial Expansion sign selected";
  }
};

/**
* 	\brief This is an exception class that is thrown when the size of the indexes and coefficients in the discrete filter class do not match
*/

class sequenceFilteringIndexandCoefficientSizeMismatch: public exception
{
public:
  virtual const char* what() const throw()
  {
    return "Size mismatch between filtering index and coefficient vector";
  }
};

/**
* 	\brief This is an exception class that is thrown when invalid Butterworth specifications are selected
*/

class wrongButterWorthSpecifications: public exception
{
public:
  virtual const char* what() const throw()
  {
    return "The selected specifications are wrong!";
  }
};

/**
* 	\brief This is an exception class that is thrown when dataSet size mismatch in discrete filtering
*/

class sequenceFilteringTimeMismatch: public exception
{
public:
  virtual const char* what() const throw()
  {
    return "Time mismatch in discrete filtering, most likely check end times when generating DataSets";
  }
};

/**
* 	\brief This is an exception class that is thrown when the channel correction frequency sweep function is wrong
*/

class wrongFrequencySweepRange: public exception
{
public:
  virtual const char* what() const throw()
  {
    return "The selected channel correction frequency sweep range incorrect";
  }
};



#endif
