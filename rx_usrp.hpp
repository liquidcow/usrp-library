/// USRP wrapper class - rx usrp1
/**
* \brief USRP rx Wrapper class, interface between ursp class and uhd driver 
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden 
*
* \date 26/11/2012
*/
#include "DataSet.hpp"
#include "Exceptions.hpp"
#include <iostream>
#include <queue>

#include <uhd/utils/thread_priority.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/exception.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <iostream>
#include <fstream>
#include <complex>
#include <csignal>
#include <cmath>
#include <string>

#ifndef RX_USRP_HPP
#define RX_USRP_HPP

using namespace boost;
using namespace std;

template <class T>
class rx_usrp{

public:
	//constructor
	rx_usrp();
    //verbose constructor
    rx_usrp(bool);
	//set the sample rate
	void setSampleRate(double);
	//set center frequency
	void setCenterFrequency(double);
	//set the gain
	void setGain(double);
	//set daughterboard IF filter bandwidth
	void setIFBandwidth(double);
	//set daughterboard antenna
	void setAntenna(string);
	//set daughterboard subdevice
	void setSubdevice(string);
	//set clock reference (internal, external, mimo)
	void setClockReference(string);
	//set USRP args
	void setArgs(string);
	//set over-the-wire sample mode
	void setOTW(string);
    //set the samples per buffer
    void setSamplesPerBuffer();
	//initialise the USRP
	void init();
    //start collecting samples
	void startSampling();
    //stop collecting samples
    void stop();
    //get a sample out
    DataSet<T> sample();
    //query the size of the sample queue
    int RXQueueSize();
    //check if the device is collecting data.
    bool streaming();
    //check if the USRP device is ready for a program shutdown
    bool usrpOffline();	

private:
	string args, ant, subdev, ref, otw;
    size_t spb;
    double rate, centerFreq, gain, bw;
    float ampl;
    bool initialised;
    uhd::usrp::multi_usrp::sptr usrpDevice;
    bool streamFlag, readyForShutDown;
    bool verbose;
    //transmission variables
    double step;
    queue<DataSet<T> > sampleQueue;
    uhd::rx_streamer::sptr rx_stream;
    vector<complex<float> > buff;
    vector<complex<float> > buff2;
    vector<complex<T> *> buffs;
    vector<complex<T> *> buffs2;
    uhd::rx_metadata_t md;
};

/**
* \struct samplingStruct
* \brief structure for threaded filling of a buffer
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \version 1.0
* \date 20/11/2012 
*/
template <class T>
struct samplingStruct
{
    samplingStruct(queue<DataSet<T> > &sampleQueue, vector<complex<T> > &buff, double &rate){
        Const<T> temp(0);
        //DataSet<T> packet(temp,1000);
        DataSet<T> packet(temp,rate);
        int size = buff.size();
        for(int i = 0; i < size;i++){
            packet.push_back(buff[i]);
        }
        sampleQueue.push(packet);
    }
    void operator()()
    {}
};

/** \fn Constructor
 */
template <class T>
rx_usrp<T>::rx_usrp():
	args(""),					//USRP args are default ""
	ant("TX/RX"), 				//recommended antenna
	subdev(""),				    //default subdevice is "" 
	ref("internal"),			//default  USRP Clock is the internal clock 
	otw("sc16"),				//default over-the-wire sample mode is sc16
    spb(0),					    //default samples per buffer 0;
    rate(2500000),				//default sample rate of 250k hz 
    centerFreq(1000000), 		//default center frequency of 3Mhz
    gain(0),					//default USRP gain of 0dB  
    bw(60),				    //default IF bandwidth of 60MHz
   	initialised(false),			//the init flag starts as false
    streamFlag(false),
    readyForShutDown(true),
    verbose(false)
        {
}

/** \fn Verbose Constructor
 */
template <class T>
rx_usrp<T>::rx_usrp(bool v):
    args(""),                   //USRP args are default ""
    ant("TX/RX"),               //recommended antenna
    subdev(""),                 //default subdevice is "" 
    ref("internal"),            //default  USRP Clock is the internal clock 
    otw("sc16"),                //default over-the-wire sample mode is sc16
    spb(0),                     //default samples per buffer 0;
    rate(2500000),              //default sample rate of 250k hz 
    centerFreq(1000000),        //default center frequency of 3Mhz
    gain(0),                    //default USRP gain of 0dB  
    bw(60),                     //default IF bandwidth of 60MHz
    initialised(false),         //the init flag starts as false
    streamFlag(false),
    readyForShutDown(true),
    verbose(v)
        {
}

/**
 * \brief set the sample rate
 * @param r double sample rate value
 */
template <class T>
void rx_usrp<T>::setSampleRate(double r){
    if(!initialised){
        if(r < 1) 
            throw InvalidUSRPSetting();
        else
            rate = r;
    }else{
        throw USRPAlreadyInitialised();
    }
}

/**
 * \brief set center frequency
 * @param c double center frequency
 */
template <class T>
void rx_usrp<T>::setCenterFrequency(double c){
    if(!initialised){
        if(c < 0 || c > 249000000) 
            throw InvalidUSRPSetting();
        else
            centerFreq = c;
    }else{
        throw USRPAlreadyInitialised();
    }
}

/**
 * \brief set the gain
 * @param g double gain value
 */
template <class T>
void rx_usrp<T>::setGain(double g){
    if(!initialised){
        if(g < -20 || g > 20)
            throw InvalidUSRPSetting();
        else
            gain = g;
    }else{
        throw USRPAlreadyInitialised();
    }
}
/**
 * \brief check if the device is streaming
 * @return boolean value
 */
template <class T>
bool rx_usrp<T>::streaming(){
    return streamFlag;
}
/**
 * \brief set daughter-board IF filter bandwidth
 * @param b double bandwidth of the filter
 */
template <class T>
void rx_usrp<T>::setIFBandwidth(double b){
    if(!initialised){
        if(b < 0 || b > 249000000)
            throw InvalidUSRPSetting();
        else
            bw = b;
    }else{
        throw USRPAlreadyInitialised();
    }
}

/**
 * \brief set USRP args
 *@param a string args
 */
template <class T>
void rx_usrp<T>::setArgs(string a){
    if(!initialised){
        args = a;
    }else{
        throw USRPAlreadyInitialised();
    }
}

/**
 * \brief set daughter-board antenna
 * @param a string antenna
 */
template <class T>
void rx_usrp<T>::setAntenna(string a){
    if(!initialised){
        ant = a;
    }else{
        throw USRPAlreadyInitialised();
    }
}

/**
 * \brief set daughterboard subdevice
 * @param s string of subdevice
 */
template <class T>
void rx_usrp<T>::setSubdevice(string s){
    if(!initialised){
        subdev = s;
    }else{
        throw USRPAlreadyInitialised();
    }
}

/**
 * \brief set over-the-wire sample mode
 * @param o otw parameter
 */
template <class T>
void rx_usrp<T>::setOTW(string o){
    if(!initialised){
        otw = o;
    }else{
        throw USRPAlreadyInitialised();
    }
}

/**\brief set clock reference (internal, external, mimo)
 * @param r reference string
 */
template <class T>
void rx_usrp<T>::setClockReference(string r){
    if(!initialised){
        if(r != "internal" || r != "external" || r != "mimo")
            throw InvalidUSRPSetting();
        else
            ref = r;
    }else{
        throw USRPAlreadyInitialised();
    }
}

/**
 * \brief initializes the USRP
 */
template <class T>
void rx_usrp<T>::init(){
	if(!initialised){
    	//create the USRP device
    	if(verbose) cout<<endl<< boost::format("Creating the usrp device with: %s...") % args<<endl;
    	usrpDevice = uhd::usrp::multi_usrp::make(args);

    	//Lock mboard clocks
    	usrpDevice->set_clock_source(ref);

    	//set the sub device if specified
    	if (subdev != "") usrpDevice->set_rx_subdev_spec(subdev);
    	if(verbose) cout<< boost::format("Using Device: %s") % usrpDevice->get_pp_string() <<endl;

    	//set the sample rate
    	if(verbose) cout << boost::format("Setting RX Rate: %f Msps...") % (rate/1e6) <<endl;
    	usrpDevice->set_rx_rate(rate);
    	if(verbose) cout<< boost::format("Actual RX Rate: %f Msps...") % (usrpDevice->get_rx_rate()/1e6) <<endl<<endl;

    	//setup the channels
    	for(size_t chan = 0; chan < usrpDevice->get_rx_num_channels(); chan++) {

            //set the center frequency
            if(verbose) cout<< boost::format("Setting RX Freq: %f MHz...") % (centerFreq/1e6) <<endl;
            usrpDevice->set_rx_freq(centerFreq, chan);
            if(verbose) cout << boost::format("Actual RX Freq: %f MHz...") % (usrpDevice->get_rx_freq(chan)/1e6) <<endl<<endl;

	        //set the rf gain
	        if(verbose) cout << boost::format("Setting RX Gain: %f dB...") % gain <<endl;
	        usrpDevice->set_rx_gain(gain, chan);
	        if(verbose) cout << boost::format("Actual RX Gain: %f dB...") % usrpDevice->get_rx_gain(chan) <<endl <<endl;

	        //set the IF filter bandwidth
	        if(verbose) cout << boost::format("Setting RX Bandwidth: %f MHz...") % bw <<endl;
	        usrpDevice->set_rx_bandwidth(bw, chan);
	        if(verbose) cout << boost::format("Actual RX Bandwidth: %f MHz...") % usrpDevice->get_rx_bandwidth(chan) <<endl<<endl;
	        
	        //set the antenna
	        usrpDevice->set_rx_antenna(ant, chan);
    	}

    	//allow for some hardware setup time
    	boost::this_thread::sleep(boost::posix_time::seconds(1));
        
        ///create a receive streamer
        uhd::stream_args_t stream_args("fc32","sc16");
        uhd::rx_streamer::sptr rx_stream = usrpDevice->get_rx_stream(stream_args);

        //allocate a  double buffer which we re-use for each channel
        if (spb == 0) spb = rx_stream->get_max_num_samps();
        //buffer 1
        vector<complex<float> > tempBuff(spb);
        buff = tempBuff;
        //buffer 2
        vector<complex<float> > tempBuff2(spb);
        buff2 = tempBuff2;

        //setup the metadata flags
        md.start_of_burst = true;
        md.end_of_burst   = false;
        md.has_time_spec  = true;
        md.time_spec = uhd::time_spec_t(0.1);

        usrpDevice->set_time_now(uhd::time_spec_t(0.0));

        //Check Ref and LO Lock detect
        vector<string> sensor_names;
        sensor_names = usrpDevice->get_rx_sensor_names(0);
        if (find(sensor_names.begin(), sensor_names.end(), "lo_locked") != sensor_names.end()) {
            uhd::sensor_value_t lo_locked = usrpDevice->get_rx_sensor("lo_locked",0);
            if(verbose) cout << boost::format("Checking RX: %s ...") % lo_locked.to_pp_string() <<endl;
            UHD_ASSERT_THROW(lo_locked.to_bool());
        }
        sensor_names = usrpDevice->get_mboard_sensor_names(0);
        if ((ref == "mimo") and (find(sensor_names.begin(), sensor_names.end(), "mimo_locked") != sensor_names.end())) {
            uhd::sensor_value_t mimo_locked = usrpDevice->get_mboard_sensor("mimo_locked",0);
            if(verbose) cout << boost::format("Checking RX: %s ...") % mimo_locked.to_pp_string() <<endl;
            UHD_ASSERT_THROW(mimo_locked.to_bool());
        }
        if ((ref == "external") and (std::find(sensor_names.begin(), sensor_names.end(), "ref_locked") != sensor_names.end())) {
            uhd::sensor_value_t ref_locked = usrpDevice->get_mboard_sensor("ref_locked",0);
            if(verbose) cout << boost::format("Checking RX: %s ...") % ref_locked.to_pp_string() <<endl;
            UHD_ASSERT_THROW(ref_locked.to_bool());
        }

        cout<<"RX USRP Initialised."<<endl; 
	}else
		throw USRPAlreadyInitialised();
	initialised = true;
}


/**
 * \brief //abrogate the streaming the dataset
 */
template <class T>
void rx_usrp<T>::stop(){
    streamFlag = false;
}

/** \fn startSampling
 * \brief start collecting samples
 */
template <class T>
void rx_usrp<T>::startSampling(){
    readyForShutDown = false;
    if(!streamFlag){    
        if(verbose)cout<<"sampling started"<<endl;
        streamFlag = true;
        bool toggle = true;
        bool overflowMessage=true;

        //setup streaming
        uhd::stream_cmd_t stream_cmd(uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS);
        stream_cmd.num_samps = spb;
        stream_cmd.stream_now = true;
        stream_cmd.time_spec = uhd::time_spec_t();
        usrpDevice->issue_stream_cmd(stream_cmd);

        uhd::stream_args_t stream_args("fc32","sc16");
        uhd::rx_streamer::sptr rx_stream = usrpDevice->get_rx_stream(stream_args);

        size_t num_rx_samps = rx_stream->recv(&buff2.front(), buff2.size(), md, 1,true);

        while(streamFlag){
            if(toggle){
                toggle = false;
                samplingStruct<T> a(sampleQueue,buff2,rate);
                thread t(a);
                size_t num_rx_samps = rx_stream->recv(&buff.front(), buff.size(), md, 1,true);
                t.join();
            }else{
                toggle = true;
                samplingStruct<T> b(sampleQueue,buff,rate);
                thread t(b);
                size_t num_rx_samps = rx_stream->recv(&buff2.front(), buff2.size(), md, 1,true);
                t.join();
            }

            //check for errors via md flags
            if (md.error_code == uhd::rx_metadata_t::ERROR_CODE_TIMEOUT) {
                throw RXUSRPTimeOut();
                break;
            }
            if (md.error_code == uhd::rx_metadata_t::ERROR_CODE_OVERFLOW){
                if(overflowMessage){
                    overflowMessage = false;
                    cout<<"\tPC overflow detected. Minimum sustained write speed of "<<(usrpDevice->get_rx_rate()*sizeof(T)/1e6)<<"MBps required."<<endl;
                }
                continue;
            }
            if (md.error_code != uhd::rx_metadata_t::ERROR_CODE_NONE){
                throw std::runtime_error(str(boost::format(
                    "Unexpected error. USRP code: 0x%x"
                ) % md.error_code));
            }
        }

        //close down the streaming
        uhd::stream_cmd_t stream_cmd2(uhd::stream_cmd_t::STREAM_MODE_STOP_CONTINUOUS);
        stream_cmd2.num_samps = spb;
        stream_cmd2.stream_now = true;
        stream_cmd2.time_spec = uhd::time_spec_t();

        usrpDevice->issue_stream_cmd(stream_cmd2);
        //some hardware reaction time
        streamFlag = false;
        readyForShutDown = true;
    }
}

/**
 * @return  integer sample queue size
 */
template <class T>
int rx_usrp<T>::RXQueueSize(){
    return sampleQueue.size()-1;
}


/**
 * 
 * @return boolean value to check if the device is ready for shutdown
 */
template <class T>
bool rx_usrp<T>::usrpOffline(){
    return readyForShutDown;
}


/**
 * @return sample of the DataSet<T>
 */
template <class T>
DataSet<T> rx_usrp<T>::sample(){
    if(sampleQueue.size()>1){
        sampleQueue.pop();
        return sampleQueue.front();
    }else{
        Const<T> zero(0);
        DataSet<T> empty(zero,rate);
        return empty;
    }
    
}

#endif
