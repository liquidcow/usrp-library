#include "Sinusoid.hpp"
#include "DataSet.hpp"
#include "Const.hpp"


#ifndef CHANNEL_CORRECTION_H
#define	CHANNEL_CORRECTION_H

/** Channel_correction
* \class Channel_correction
* \brief Class used to correct the frequency response of the channel
* 
* This class is used to send a frequency sweep which can then be used at the receiver to correct the gain of various frequencies.
*
* \author Yves-Francois Rivard, Serge Mbamba, Noel Moyo
* \version 1.0
* \date 18/01/2014 
*
*/


class Channel_correction
{
        public:
            Channel_correction (float start_freq, float end_freq, float delta_freq, double sampling_rate);
            DataSet <float>& freqSweep ();
            bool is_valid_freq();
        private:
            float _current_freq;
            float _end_freq;
            float _delta_freq;
            double _sampling_rate;
            DataSet <float> set;
  
};



#endif	/* CHANNEL_CORRECTION_H */

