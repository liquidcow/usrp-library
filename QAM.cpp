#include "QAM.h"
#include "Noise.hpp"

/**
* \brief Constructor for QAM class
* @param M-ary this is the size of the constellation the user wants to use
* @param spp this is the symbols per pusle rate
*/
QAM::QAM(int M_ary ,float frequency, unsigned int periods, float sample_rate): QAM_Size(M_ary), frequency(frequency), periods(periods), sample_rate(sample_rate)
{
	pi = 4*atan(1.0); 
        MQAM();
}		

/**
* @param inf is a string of characters the user wants to send to the receiver. 
* a string offers the most flexibility in the transformation from bytes to signals, and 
* it also allows for the easy manipulation of the characters inside 
*/
void QAM::push_back(string inf)
{
	Const<float> z(0.0); 
	float p = periods;
	float f = p/frequency;
	int bit_size = log2(QAM_Size);
	DataSet<float> Padding(z,sample_rate,0,f);
	Const<float> a(0);
	DataSet<float> Temp(a,sample_rate,0,0);
    
	for(int i = 0; i<inf.length(); i++)
	{
		vector<unsigned> bit_value = getbits(inf[i],bit_size); 
              
		for(int j = 0; j<bit_value.size(); j++)
		{
			Temp.concatenate(constellation[bit_value[j]]); 
		} 
	}
	DataQueue.push(Temp); 
}		

/**
* \fn Retrieves the sequence received, symbol per symbol. 
* @return a DataSet is returned when the user wants to pop off the latest symbol for sending. That symbol is then deleted. 
* this is the best way to ensure that the user will only receive symbols in the correct pattern. 
*/
DataSet<float> QAM::pop()
{
	 DataSet<float> D = DataQueue.front(); 
	 DataQueue.pop();
	 return D;
}

/**
 * \fn   Splitter of bits
 * \brief splits the bits to give two bit size depending on the QAM_Size
 */
void QAM::bitsplitter()
{
    float bit_number= log2(QAM_Size);
    
    int first_bit= ceil(bit_number/2);
    int sec_bit= bit_number- first_bit;
    
    x_bit= first_bit;
    y_bit= sec_bit;
}
/** 
 * \fn  Generates the different amplitudes to be used as coordinates of the constellation points
 * @param  int (number_of_bits) Number of bits per symbol
 * @return vector<int> The  vector of different amplitudes to be used in the modulation scheme
 */

vector <int> QAM::amplitudeGenerator(const int number_of_bits )
{
    vector<int> qam_Amps;
    
    for (int i=0; i<number_of_bits*2; i++)
    {
        qam_Amps.push_back((2*(i+1)-1-number_of_bits*2));
    }
    
    return qam_Amps;   
} 
/**
 * \brief QAM modulate any Size, and push the DataSet into the constellation
 */
void QAM::MQAM()
{
     if (!PowerOfTwo (QAM_Size))
     {
          throw notAvaliableQAM(); 
     } 
    
    bitsplitter();
    
    vector <string> x_grayVector = grayGenerator (x_bit);
    vector <string> y_grayVector = grayGenerator (y_bit);
    vector <int> x_amplitudes = amplitudeGenerator(x_bit);
    while (x_amplitudes.size()!=x_grayVector.size()) x_amplitudes = amplitudeGenerator(x_bit++ );
    vector <int> y_amplitudes = amplitudeGenerator(y_bit );
    while (y_amplitudes.size()!=y_grayVector.size()) y_amplitudes = amplitudeGenerator(y_bit++);

    float base_amp =1;
    
    for (int i = 0; i < x_grayVector.size(); i++)
    {
        for (int j = 0; j < y_grayVector.size(); j++)
        {
            string temp_str = "";
            temp_str += x_grayVector[i];
            temp_str += y_grayVector[j];
            
            constellation_point Point;
            Point (temp_str, x_amplitudes[i], y_amplitudes [j]);
            constellation_structs.push_back(Point);
        }   
    }
    
    basisSort();

        for (int  i=0; i<QAM_Size; i++){
     
            Sinusoid<float> sine(base_amp, frequency,0); 
	    Sinusoid<float> cosine(base_amp, frequency,pi/2);
            
            Const<float> x_Amp(constellation_structs[i].x_amplitude); 
            Const<float> y_Amp(constellation_structs[i].y_amplitude); 
            
            mult<float> cos(&cosine,&x_Amp); 
	    mult<float> sin(&sine, &y_Amp); 
        
            DataSet<float> data (sin,cos,sample_rate,0.0, periods/frequency);
	    populateStruct<float> a (data);
	    boost::thread t(a);
            t.join();
            
            constellation.push_back(data);
    }
}
/**
 *  \brief binary values computed for the constellation_structs
 */
void QAM::basisSort()
{
        vector <constellation_point> dummy_const_struct = constellation_structs;

        int  str_int=0;
       
        for (int j=0; j<constellation_structs.size(); j++){
          for (int i=0; i<(constellation_structs[j].bits).size(); i++){
        
            str_int= str_int*2 +  ((constellation_structs[j].bits)[i]-'0') ;
         
          }
            dummy_const_struct[str_int]=constellation_structs[j];
         
           str_int=0;
        }
        constellation_structs= dummy_const_struct;
}

/**
 * 
 * @return Qam size for the modulation
 */
int QAM::getSize() const
{
    return QAM_Size;
}
/**
 * 
 * @return frequency
 */
float QAM::getFrequency() const
{
    return frequency;
}
/**
 * 
 * @return periods
 */
unsigned int QAM::getPeriod() const
{
    return periods;
}
/**
 * 
 * @return sample rate
 */
float QAM::getSampleRate() const
{
    return sample_rate;
}
/**
 * 
 * @return constellation_structs
 */
vector <constellation_point> QAM::getStructs() const
{
    return constellation_structs;
}
/**
 * 
 * @return constellation of the QAM
 */
vector <DataSet<float> > QAM::getConstellation() const
{
    return constellation;
}