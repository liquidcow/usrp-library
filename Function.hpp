#include "Function_Wrappers.hpp"
#ifndef FUNCTION_HPP
#define FUNCTION_HPP

/**
* \class Function
* \brief Function is the base class that we is inhereted by other functions
*
* An inhereted structure was chosen for the functions to allow for polymorphasm 
* and to give us maximum flexibility with dealing with functions. This structure
* gives the power to create powerful mathematical operator classes like add, sub, 
* mult and div. 
*
* Templates are used throughout to allow for more general code and to give programmers
* the power to have the code do what they want. 
*
* Function is a pure virtual class. 
*
* \author Ashton Hudson
* \version 1.0
* \date 19/11/2012
*/
template<class T>
class Function
{
public:
	virtual T operator()(T input) =0; 
};


#endif 
