/// functions usage example
///
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 07/01/2013

#include "../../Const.hpp"
#include "../../Function.hpp"
#include "../../Sinusoid.hpp"
#include <iostream>

using namespace std;

int main(){
	//define pi
	float pi = 4*atan(1);

	//create an analytical sinusoid function 
	Sinusoid<float> message(0.5,1000,0);		//amplitude = 0.5 , frequency = 1kHz , phase = 0 radians
	Sinusoid<float> carrier(1,100000,pi/2);	//amplitude = 1 , frequency = 100kHz , phase = pi/2 radians
	//define a constant
	Const<float> c(0.3);
	//add the constant and the sinusoid together, store in result. i.e. result = c + message 
	add<float> result(&c,&message);
	//store the product of result multiplied by the carrier in result2. i.e. result2 = result*carrier
	mult<float> result2(&result,&carrier);

	//this gives the following net equation:
	// result2(t) = [c + message(t)]*carrier(t)
	// result2(t) = [0.3 + 0.5*sin(2000πt)]*sin(200000πt + π/2)
	// result2(t) = [0.3 + 0.5*sin(2000πt)]*cos(200000πt)

	//the function result2(t) can now be evaluated for any t:
	float t = 0.1;

	cout<<"Evaluating the function at t="<<t<<"\tresult2("<<t<<")="<<result2(t)<<" as expected."<<endl;
}