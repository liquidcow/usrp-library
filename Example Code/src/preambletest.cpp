#include "PreambleTester.h"
#include <iostream>
#include <string>
#include "QAM.h"

using namespace std;

int main()
{
	string myString = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
	PreambleTester first;

	QAM myQAM(4, 1000000.0, 10, 4000000.0);

	for (int k = 0; k < 100; k++)
		first.run(myQAM, myString);
}
