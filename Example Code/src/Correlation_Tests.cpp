/// USRP wrapper usage example - 4QAM rx example
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 30/11/2012
///

#include <cmath>
#include <iostream>
#include <string>

#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../QAM.h"
#include "../../QAM_Demod.h"
#include "../../Syncro.h"
#include "../../FrameBuilder.hpp"
#include "../../Exceptions.hpp"
#include "../../Graph.hpp"

using namespace std;

int main(){

	int samplingRate = 4000000;

	//create the USRP connection
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(samplingRate);
	dev.setCenterFrequency(0);
	//dev.setSubdevice("A:AB");
	dev.setGain(0);

	//init the device
	dev.init();

	
	//create a FrameBuilder object
	FrameBuilder<float> fb(5e-5,128);

	float pi = 4*atan(1);
	

	//create a graphing object
	Graph<float> plotter(samplingRate);

	/*
	//create a matlab plotable output file to capture the frame for plotting
	ofstream output;
	output.open("correlation_plot.m");
	output<<"clear;\nx=[";

	float frequency = 100000;
	//create a QAM object
	QAM bob(16, frequency, 5, samplingRate); 
	QAM_Demod alice(16,0,frequency,5, samplingRate,0);

	string message = "";
	unsigned char a = 0xAB;
	unsigned char b = 0xFE;
	unsigned char c = 0x45;
	unsigned char d = 0x01;
	unsigned char e = 0x32;
	unsigned char f = 0x76;
	unsigned char g = 0xCD;
	unsigned char h = 0x98;

	message+=a;
	message+=b;
	message+=c;
	message+=d;
	message+=e;
	message+=f;
	message+=g;
	message+=h;

	message = "Hello";

	bob.push_back(message);
	DataSet<float> A = bob.pop();
	
	Const<float> zero(0);
	//DataSet<float> Z(zero,zero,samplingRate,0,0.001);
	//DataSet<float> Z1(zero,zero,samplingRate,0,0.001);
	//Z.populate();
	//Z1.populate();

	//A.concatenate(Z1);
	//Z.concatenate(A);


	
	plotter.plot("A",A);
	dev.startSampling();
	cout<<"Listening"<<endl;
	
	while (fb.frameQueueSize()<20)
	{
		if(dev.RXQueueSize()>10){
			//push the packet stream onto the FrameBuilder
			fb.push_back(dev.sample());
		}else{
			//chill for a bit if there are no packets ready from the usrp - reduces CPU load overall
			boost::this_thread::sleep(boost::posix_time::microseconds(400000));
			cout<<"fb size: "<<fb.frameQueueSize()<<"\tRX size: "<<dev.RXQueueSize()<<endl;
		}
		
	}

	dev.stop();
	DataSet<float> trash = fb.pop();
	

	DataSet<float> allData;
	while(fb.frameQueueSize()>2){
		DataSet<float> temp = fb.pop();
		allData.concatenate(temp);
	}
	DataSet<float> Z = fb.pop();
	plotter.plot("Z",Z);

	plotter.plot("allData",allData);

	cout<<"plots complete"<<endl;
	float min = 0;
	float max = 0;
	int trackmax = 0;
	int trackmin = 0;
	int total = Z.size()-A.size();
	for(int i = 0; i < Z.size()-A.size(); i++){
		DataSet<float> temp = Z.slice(i,i+A.size());
		float cor = temp.correlate(A);
		output<<cor<<"\n";
		if(cor<min){
			min = cor;
			trackmin = i;
		}
		if(cor>max){
			max = cor;
			trackmax = i;
		}
	}
	
	int track = 0;
	min*=-1;
	if(min > max){
		track = trackmin;
	}else{
		track = trackmax;
	}
	cout<<"IQ correlation complete"<<endl;
	cout<<"min: "<<min<<"\tmax: "<<max<<endl;
	cout<<"correlation value: "<<min<<"\ttracker: "<<track<<endl;
	DataSet<float> result = Z.slice(track,track+A.size());
	result.gainCorrect(-4.2);
	plotter.plot("result",result);
	alice.push_back(result);
	cout<<endl<<"Demodulated Message: "<<alice.pop()<<endl;
	//close off the matlab file
	output<<"];\nfigure(5);\nplot(x, \"k\");";
	//output<<"];\nfigure(5);\nplot(x, \"g\");\nhold on;\nplot(y, \"r\");\nhold on;\nplot(z);\nhold off;";
	output.close();
	*/

	dev.startSampling();
	int count = 0;
	DataSet<float> allData;
	while(count < 5000){
		if(dev.RXQueueSize()>1){
			DataSet<float> temp = dev.sample();
			allData.concatenate(temp);
		}
		cout<<count<<endl;
		count++;
	}

	dev.stop();
	cout<<"plotting"<<endl;
	plotter.plot("allData",allData);

}
