/// USRP wrapper usage example - alternating signals
///
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 19/11/2012
///

#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../Const.hpp"
#include "../../Function.hpp"
#include "../../Sinusoid.hpp"
#include "../../Exceptions.hpp"
#include "../../Function_Wrappers.hpp"
#include <cmath>
#include <iostream>

using namespace std;
using namespace boost;

int main(){

	//define pi
	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;

	int sampleRate = 2000000;
	//configure the device
	dev.setSampleRate(sampleRate);
	dev.setCenterFrequency(2000000);
	dev.setGain(0);

	//init the device
	dev.init();

	//create an analytical function to output to the device
	Sinusoid<float> signal1I(1,100000,0);		
	Sinusoid<float> signal1Q(1,100000,pi/2);		
	Sinusoid<float> signal2I(0.5,200000,0);	
	Sinusoid<float> signal2Q(0.5,200000,pi/2);	

	//convert the analytical function to a data sample
	DataSet<float> signal1Sample(signal1I,signal1Q,sampleRate,0, 1);	
	DataSet<float> signal2Sample(signal2I,signal2Q,sampleRate,0, 1);	
	
	//populate the data sample
	signal1Sample.populate();
	signal2Sample.populate();

	cout<<"streaming..."<<endl;
	int count = 0;
	bool toggle = false;
	while(count < 1000){
		if(toggle){
			toggle = false;
			//start sending
			dev.queue(signal1Sample);

		}else{
			toggle = true;
			//start sending
			dev.queue(signal2Sample);
		}
		count++;
		while(dev.TXQueueSize() > 10){
			boost::this_thread::sleep(boost::posix_time::microseconds(100000));
		}
		
	}
	
	dev.stop();
	
	cout<<"stream complete"<<endl;
}
