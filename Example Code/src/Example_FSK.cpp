#include "FSK.h"
#include "Modulator.h"
#include "DataSet.hpp"
#include "Sinusoid.hpp"
#include "usrp.hpp"
#include <cmath>
#include <iostream>

using namespace std;
using namespace boost; 


int main(){
	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(4000000);
	dev.setCenterFrequency(0);
	dev.setGain(0);

	//init the device
	dev.init();

	FSK FSK4(4, 1500, 1000000.0, 10, 4000000, noncoherent); 

	//string words = "Hello world! This is us testing out the 4-QAM modulation scheme!"; 
	//1500 bytes of data
	string words = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
	
	FSK4.push_back(words); 

	DataSet<float> Beeeg = FSK4.pop(); 
	cout<<"signal build complete"<<endl;
	for (int i =0; i<1000; i++)
	{
		dev.queue(Beeeg);
	}

	dev.stop();
}
