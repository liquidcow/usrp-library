#include "../../Sinusoid.hpp"
#include "../../DataSet.hpp"

using namespace std;

int main(){
	
	//construct the signal functions
	float pi = 4*atan(1);
	float frequency = 2000;
	Sinusoid<float> inphaseSignal(1,frequency,0);
	Sinusoid<float> quadratureSignal(1,frequency,pi/2);

	//define the constraints
	float startTime = 0;
	float endTime = 0.01;
	float sampleRate = 4000000;

	//create the DataSet object.
	DataSet<float> discreteSignal(inphaseSignal,quadratureSignal,sampleRate, startTime, endTime);

	//populate the DataSet
	discreteSignal.populate();
}