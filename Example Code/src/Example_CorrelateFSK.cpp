#include <cmath>
#include <iostream>

#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../Const.hpp"
#include "../../Function.hpp"
#include "../../Sinusoid.hpp"
#include "../../Exceptions.hpp"
#include "../../Function_Wrappers.hpp"
#include "../../QAM.h"
#include <boost/thread.hpp>
#include <fstream>

using namespace std;
using namespace boost;

int main()
{
	float pi = 4*atan(1);

	float f = 10000;
	// 10 periods for each symbol
	float fs = f/10;

	// change in frequency
	float Df = 0.5*fs;
	
	// define the carriers
	float f1 = f + Df;
	float f2 = f1 + Df;
	float f3 = f2 + Df;
	float f4 = f3 + Df;

	// define the period
	double sTime = 0;
	double eTime = 1;

	// generate the signal frequecies
	Sinusoid<float> s1(1, 2*pi*f1,0);
	Sinusoid<float> s2(1, 2*pi*f2,0);
	Sinusoid<float> s3(1, 2*pi*f3,0);
	Sinusoid<float> s4(1, 2*pi*f4,0);

	// generate the corresponding datasets
	DataSet<float> d1(s1, 4000000, sTime, eTime);
	DataSet<float> d2(s2, 4000000, sTime, eTime);
	DataSet<float> d3(s3, 4000000, sTime, eTime);
	DataSet<float> d4(s4, 4000000, sTime, eTime);

	// populate the datasets
	d1.populate();
	d2.populate();
	d3.populate();
	d4.populate();

	// correlate each signal and display result	
	cout << "d1 to d1 " << d1.correlate(d1) << endl;
	cout << "d2 to d2 " << d2.correlate(d2) << endl;
	cout << "d3 to d3 " << d3.correlate(d3) << endl;
	cout << "d4 to d4 " << d4.correlate(d4) << endl;	

	cout << "d1 to d2 " << d1.correlate(d2) << endl;
	cout << "d2 to d3 " << d2.correlate(d3) << endl;
	cout << "d3 to d4 " << d3.correlate(d4) << endl;
	cout << "d4 to d1 " << d4.correlate(d1) << endl;

	cout << "d1 to d3 " << d1.correlate(d3) << endl;
	cout << "d2 to d4 " << d2.correlate(d4) << endl;
	//cout << "d3 to d1 " << d3.correlate(d1) << endl;
	//cout << "d4 to d2 " << d4.correlate(d1) << endl;
}