#include "QAM.h"
#include "Modulator.h"
#include "Demodulator.h"
#include "QAM_Demod.h"
#include "DataSet.hpp"
#include "Sinusoid.hpp"
#include "Const.hpp"
#include "usrp.hpp"
#include "Graph.hpp"
#include <cmath>
#include <iostream>
#include <boost/progress.hpp>

using namespace std;
using namespace boost; 


int main(){

	//set up the communication parameters
	progress_timer t; 
	float pi = 4*atan(1);
	float sampleRate = 4000000;
	float QAMFrequency = 1000000;
	unsigned int periodsPerSymbol = 10;
	float erasurePercentage = 0.01;
	float phaseOffset = 0;
	Graph<float> plotter(sampleRate);

	//construct the transmit constellation
	QAM QAM16(4, QAMFrequency, periodsPerSymbol, sampleRate);
	//construct the recieve constellation 
	QAM_Demod QAM16_Demod(QAM16,erasurePercentage, phaseOffset); 

	//A message to modulate and demodulate
	string words = "Hello world! This is us testing out the 16-QAM modulation scheme!"; 
	//string words = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()_+1234567890-=[]|';:'{/.,<>?";
	//string words = "abcdefghijklmnopqrstwxyz";

	//modulate the message into a QAM16 signal
	cout<<"Moduating the message...";
	QAM16.push_back(words); 
	DataSet<float> QAMSignal = QAM16.pop(); 
	cout<<"complete"<<endl;

	//plot the QAM16 signal before gain correction
	plotter.plot("qam_to_noise",QAMSignal, QAMFrequency, periodsPerSymbol); 
	
	//gain correct in order to demodulate (this accounts for real world attenuation)
	QAMSignal.gainCorrect(6);

	//plot the QAM16 signal once gain corrected
	plotter.plot("Output",QAMSignal, QAMFrequency, periodsPerSymbol); 

	//demodulate the signal back into the message
	cout<<"Demodulating the message...";
	QAM16_Demod.push_back(QAMSignal); 
	cout<<"complete."<<endl;
	
	cout<<"Original message:\t"<<words<<endl<<"Recieved message:\t"<<QAM16_Demod.pop()<<endl;

	return 0; 
}
