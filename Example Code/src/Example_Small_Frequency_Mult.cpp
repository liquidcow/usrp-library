#include "../../DataSet.hpp"
#include "../../Sinusoid.hpp"
#include <cmath>
#include <iostream>

using namespace std;

int main()
{
	const float pi = 4*atan(1);

	for (int k = 0; k < 10000; k++){
		Sinusoid<float> sine(1,1000,0);
		Sinusoid<float> cosine(1,1000,pi/2);
		Sinusoid<float> smallSine(1, k, 0);

		DataSet<float> sineSample(sine,1000000,0,1);
		DataSet<float> cosineSample(cosine,1000000,0,1);
		DataSet<float> smallSineSample(smallSine, 1000000,0,1);

		sineSample.populate();
		cosineSample.populate();
		smallSineSample.populate();

		DataSet<float> newSample = sineSample*smallSineSample;
		sineSample = newSample;
		cout << k << "\t" << sineSample[5] << "\t" << sineSample.correlate(cosineSample) << endl;

	}
	cout << "end" << endl;
	return 0;
}