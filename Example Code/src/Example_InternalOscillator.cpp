#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../Const.hpp"
#include "../../Function.hpp"
#include "../../Sinusoid.hpp"
#include "../../Exceptions.hpp"
#include "../../Function_Wrappers.hpp"
#include <cmath>
#include <iostream>

using namespace std;
using namespace boost;

int main(){

	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(1000000);
	dev.setCenterFrequency(500000);
	dev.setGain(0);

	//init the device
	dev.init();

	//create an analytical function to output to the device (AM-DSB-FC in this case)
	//Sinusoid<float> sine(0.3,1000000,0);		//amplitude = 0.2 , frequency = 1kHz , phase = 0 radians
	//Sinusoid<float> cosine(0.3,1000,pi/2);	//amplitude = 1 , frequency = 1MHz , phase = 0 radians
	Const<float> c(0.2);
	//add<float> wave(&sine,&cosine);
	//mult<float> mult1(&wave,&carrier);

	//convert the analytical function to a data sample
	DataSet<float> waveSample(c,1000000, 0, 0.01);	
	
	//populate the data sample
	waveSample.populate();

	
	//start sending
	dev.pulse(waveSample);
	
	//prove that its threaded
	cout<<"counting..."<<endl;
	while(1){
		//start sending
		dev.pulse(waveSample);
	}
	cout<<"stream complete"<<endl;
	//stop the transmission
	dev.stop();
	
	
	
}