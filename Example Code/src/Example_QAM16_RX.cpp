/// USRP wrapper usage example - 16QAM rx example
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 30/11/2012
///

#include "usrp.hpp"
#include "DataSet.hpp"
#include "QAM_Demod.h"
#include "Syncro.h"
#include "FrameBuilder.hpp"
#include "Graph.hpp"
#include "Exceptions.hpp"
#include <cmath>
#include <iostream>
#include <string>

using namespace std;

int main(){

	float pi = 4*atan(1);
	
	float phase = 0;
	double sampleRate = 4000000;
	double QAMFrequency = 100000;
	double preambleFreq = 100000;
	//create a USRP device
	usrp<float> dev;


	//configure the device
	dev.setSampleRate(sampleRate);
	dev.setCenterFrequency(0);

	dev.setGain(0);
	//dev.setSamplesPerBuffer(30);

	//init the device
	dev.init();
	int count = 0;

	//create a plotting pbject
	Graph<float> plotter(sampleRate);
	//create a FrameBuilder object
	FrameBuilder<float> fb(5e-3,2048);
	//create a syncro

	QAM QAM16Preamble(16,preambleFreq,10,sampleRate);
	QAM QAM16(16,QAMFrequency,10,sampleRate);

	//string pre = "|bdebdbdebd";
	unsigned char a = 0xAB;
	unsigned char b = 0xFE;
	unsigned char c = 0x45;
	unsigned char d = 0x01;
	unsigned char e = 0x32;
	unsigned char f = 0x76;
	unsigned char g = 0xCD;
	unsigned char h = 0x98;


	string pre = "";
	pre+= a;
	pre+= b;
	pre+= c;
	pre+= d;
	pre+= e;
	pre+= f;
	pre+= g;
	pre+= h;

	//create a QAM16 demodulation object

	QAM_Demod QAM16_Demod(QAM16,0.1, phase); 


	QAM16Preamble.push_back(pre);
	DataSet<float> preamble = QAM16Preamble.pop();
	plotter.plot("preamble",preamble,preambleFreq,1);

	Syncro sync(QAM16,preamble);

	
	//start sampling
	dev.startSampling();

	cout<<"Listening..."<<endl;
	//start capturing the results 
	while (fb.frameQueueSize()<=10)
	{
		if(dev.RXQueueSize()>10){
			//push the packet stream onto the FrameBuilder
			count++;
			fb.push_back(dev.sample());
		}else{
			//chill for a bit if there are no packets ready from the usrp - reduces CPU load overall
			boost::this_thread::sleep(boost::posix_time::microseconds(200000));
			//cout<<"frame queue size: "<<fb.frameQueueSize()<<"\tUSRP queue:"<<dev.RXQueueSize()<<endl;
		}
		
	}
	cout<<"Completed "<<fb.frameQueueSize()<<" frame captures. Total processed packets: "<<count<<endl;
	dev.stop();

	
	
	DataSet<float> allFrames;
	DataSet<float> rawFrames;
	//write the frames to a file
	count = 0;
	//float gain = 5.2;
	while(fb.frameQueueSize()>1){
		//get the frame from the FrameBuilder
		DataSet<float> frame = fb.pop();
		if(frame.size()>100000){
			cout<<"Frame dropped: frame too large."<<endl;
		}
		//write the frame to a file for analysis
		frame.gainCorrect(-5); //needs to be upside down
		rawFrames.concatenate(frame);
		

		//remove the preamble and sync on the message
		DataSet<float> message = sync.RemovePreamble(frame); 

		//give it to the demodulator
		if (message.size()>2){
			message.gainCorrect(-6.2);
			QAM16_Demod.push_back(message);
			string msg = QAM16_Demod.pop();
			//cout<<"Gain: "<<gain<<endl;
			cout<<count<<": Demodulated message: "<<msg<<endl;
			count++;
			//gain += 0.1;
		}
		else
			cout << "Empty DataSet so frame dropped"<<endl;

		if(fb.frameQueueSize() == 2){
			plotter.plot("result",message);
		}
	}
		
	//plot the recieved frames
	cout<<"plotting data..."<<endl;
	plotter.plot("rawFrames",rawFrames,QAMFrequency,10);
	cout<<"complete"<<endl;
	return 0;
}
