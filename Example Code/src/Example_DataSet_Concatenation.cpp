#include "../../DataSet.hpp"
#include "../../Sinusoid.hpp"
#include "../../Graph.hpp"
using namespace std;

int main(){
	//construct the signal functions
	float pi = 4*atan(1);
	float frequencyA = 2000;
	float frequencyB = 4000;
	Sinusoid<float> inphaseSignalA(1,frequencyA,0);
	Sinusoid<float> quadratureSignalA(1,frequencyA,pi/2);
	Sinusoid<float> inphaseSignalB(0.5,frequencyB,0);
	Sinusoid<float> quadratureSignalB(0.5,frequencyB,pi/2);

	//define the constraints
	float startTime = 0;
	float endTime = 0.01;
	float sampleRate = 4000000;

	//make a graphing tool
	Graph<float> plotter(sampleRate);

	//create the DataSet object.
	DataSet<float> A(inphaseSignalA,quadratureSignalA,sampleRate, startTime, endTime);

	//create a second DataSet object.
	DataSet<float> B(quadratureSignalB,inphaseSignalB,sampleRate, startTime, endTime);

	//populate the DataSets
	A.populate();
	B.populate();
	
	//concatenate B to the end of A
	A.concatenate(B);

	plotter.plot("A",A);
}