#include "QAM.h"
#include "Modulator.h"
#include "DataSet.hpp"
#include "Sinusoid.hpp"
#include "usrp.hpp"
#include "Syncro.h"
#include <cmath>
#include <iostream>
#include <cstdio>
#include "Demodulator.h"
#include "QAM_Demod.h"
#include "Graph.hpp"

#include <fstream>

using namespace std;
using namespace boost; 



int main(){
	float pi = 4*atan(1);
	const double SAMPLE_RT = 4000000;
	int QAMfreq = 100000;

	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(SAMPLE_RT);
	dev.setCenterFrequency(0);
	dev.setGain(0);

	//init the device
	dev.init();

	//create a graph/plot
	Graph<float> plotter(SAMPLE_RT);

	//create 2 QAM modulators
	QAM QAM16(16, QAMfreq, 10, SAMPLE_RT); 
	QAM QAMpre(16, QAMfreq, 10, SAMPLE_RT);

	string pre = "";

	//string words = "Hello";
	//string words = "a"; 
	//string words = "1234567890 abcdefghij klmnopqrst uvwxyz";
	//string words = "Hi Jaco & Ling!";
	//string words = "hello world hello again";
	string words = "Hello there. This is a test message to check that a long message can be sent with our QAM16 system.";
	//string words = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	//string words = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789.";
	

	Const<float> zero(0);
	DataSet<float> zeros(zero, SAMPLE_RT, 0, 0.1);
	zeros.populate();


	Const<float> longzero(0);
	DataSet<float> longzeros(zero, SAMPLE_RT, 0, 0.05);

	longzeros.populate();

	unsigned char a = 0xAB;
	unsigned char b = 0xFE;
	unsigned char c = 0x45;
	unsigned char d = 0x01;
	unsigned char e = 0x32;
	unsigned char f = 0x76;
	unsigned char g = 0xCD;
	unsigned char h = 0x98;

	pre+=a;
	pre+=b;
	pre+=c;
	pre+=d;
	pre+=e;
	pre+=f;
	pre+=g;
	pre+=h;

	//string words = pre;

	cout<<"preamble: "<<pre<<endl;

	
	QAMpre.push_back(pre);
	DataSet<float> tmo = QAMpre.pop();
	//tmo.gainCorrect(426);
	cout<<"Preamble DataSet size: "<<tmo.size()<<endl;

	//add zeros to end of preamble as part of preamble
	//tmo.concatenate(zeros);
	Syncro synced(QAM16, tmo);

	cout << "Preamble autocorrelation: " << tmo.correlate(tmo) << endl;
	
	// add preamble to header
	DataSet<float> HeaderPacket = synced.AddPreamble(words);

	//save the preamble signal
	plotter.plot("preamble", tmo);

	plotter.plotConstellation("preamble", tmo, SAMPLE_RT, 10);

	QAM16.push_back(words);

	//save the payload signal
	DataSet<float> dWords = QAM16.pop();
	plotter.plot("PureModulator", dWords);

	plotter.plot("HeaderWithZeros", HeaderPacket);	
	//HeaderPacket.gainCorrect(1);

	//zeros.concatenate(HeaderPacket);
	HeaderPacket.concatenate(zeros);
	plotter.plot("sent", HeaderPacket);

	cout<<"signal build complete"<<endl;

	//longzeros.concatenate(dWords);

	//plotter.plot("longzeros", longzeros);

	//plot headerpacket constenlation
	plotter.plotConstellation("HeaderConst", dWords, SAMPLE_RT, 10);

	
	cout << "TX Streaming..." << endl; 
	// stream to USRP
	while (dev.TXQueueSize() < 500){
		dev.queue(HeaderPacket);
	}
	
	cout<<"\tPacket size: "<<HeaderPacket.size()<<endl;
	dev.stop();

	zeros.gainCorrect(4.246);
	
	HeaderPacket.gainCorrect(4.246);
	
	cout << endl << "Finding preamble..." << endl << endl;
	DataSet<float> Removed = synced.RemovePreamble(HeaderPacket);
	if(Removed.size()>0){
		cout<<"Actually found the payload!"<<endl;
		plotter.plot("removed", Removed);
	}

	
	// demodulate modulated and signal with preamble removed
	QAM_Demod QD(QAM16, 0.1, 0);
	QAM_Demod QD2(QAMpre, 0.1, 0);

	cout << "Attempting to demodulate..." << endl;

	// correct gain to demodulate correctly
	Removed.gainCorrect(-5);
	dWords.gainCorrect(4.246);
	plotter.plot("PureModulator", dWords);

	QD2.push_back(dWords);		//straight from modulator
	QD.push_back(Removed);		//from syncroniser

	//obtain demodulated string
	string out = QD.pop();		
	string rel = QD2.pop();

	cout << "\nResult:\n";
	cout << "Synced: " << out << endl << "Expected: " << rel << endl;
	cout << Removed.size() << " " << dWords.size() << endl;

	if (out == rel)
		cout << "Correctly Synced!" << endl;
	else
		cout << "Incorrectly Synced!" << endl;

	cout << "DataSet size... ";
	if (Removed.size() == dWords.size())
		cout << "same size " << endl;
	else
		cout << "Not same" << endl;

	return 0;
}
