
#include "../../Sinusoid.hpp"
#include "../../Const.hpp"
#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../FrameBuilder.hpp"
#include "../../Exceptions.hpp"
#include <cmath>
#include <iostream>
#include <string>


using namespace std;

int main(){

	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;


	//configure the device
	dev.setSampleRate(256000);
	dev.setCenterFrequency(1000000);
	dev.setGain(0);
	//dev.setSubdevice();
	//init the device
	dev.init();
	//dev.setSamplesPerBuffer(30);

	Sinusoid<float> c1(0.5, 100000, 0); 
	Sinusoid<float> s1(0.5, 100000, pi/2);

	Sinusoid<float> c2(-0.0, 100000, 0); 
	Const<float> s2(-0.5); 

	DataSet<float> data(c1, s1,256000, 0.0, 0.0001);
	data.populate();
	DataSet<float> data2(c2, s1, 256000, 0.0,0.0001); 
	data2.populate();
	DataSet<float> data3(c2, s2, 256000, 0.0,0.000001); 
	data3.populate();
	DataSet<float> data4(c1, s2, 256000, 0.0,0.000001); 
	data4.populate();

	data.concatenate(data2); 
	//data.concatenate(data3); 
	//data.concatenate(data4); 

	for (int i=0; i<10; i++)
	{
		data.concatenate(data); 
	}



	for (int i =0; i<200; i++)
	{
		dev.queue(data);
	}

	dev.stop();
	return 0; 
}