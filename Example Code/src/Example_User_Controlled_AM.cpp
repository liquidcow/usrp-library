///INCOMPLETE!

#include "../../usrp.hpp"
#include "../../Sinusoid.hpp"
#include "../../DataSet.hpp"
#include <iostream>

using namespace std;

int main(){
	//define pi
	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;

	int sampleRate = 1000000;

	//configure the device
	dev.setSampleRate(sampleRate);
	dev.setCenterFrequency(1000000);
	dev.setGain(0);

	//init the device
	dev.init();
	float amplitude = 1;
	float frequency = 1000;
	float carrierFrequency = 100000;
	double t = 0;

	//create an analytical function to output to the device (AM-DSB-FC in this case)
	Sinusoid<float> messageI(amplitude,frequency,0);		
	Sinusoid<float> messageQ(amplitude,frequency,pi/2);
	Sinusoid<float> carrierI(amplitude,carrierFrequency,0);		
	Sinusoid<float> carrierQ(amplitude,carrierFrequency,pi/2);

	Const<float> c(0.1);
	add<float> tempI(&c,&messageI);
	add<float> tempQ(&c,&messageQ);

	mult<float> sigI(&tempI, &carrierI);
	mult<float> sigQ(&tempQ, &carrierQ);
	

	//get user input
	cout<<"Use character inputs to control the modulation:\na - increase amplitude\nz - decrease amplitude\nl - increase frequency\nm - decrease frequency"<<endl;
	
	for(int i = 1 ;i <= 300;i++){
		//convert the analytical function to a data sample
		DataSet<float> waveSample(messageI,messageQ,sampleRate, t, t+1);	
		
		//populate the data sample
		waveSample.populate();
		//start sending
		dev.queue(waveSample);
		t+=1;
		cout<<"TXQueueSize: "<<dev.TXQueueSize()<<" Processed packets: "<<i<<endl;
	}
	//cout<<"queue complete"<<endl;
	
	while(dev.TXQueueSize()>2){
		cout<<"TXQueueSize: "<<dev.TXQueueSize()<<" Processed packets: "<<300<<endl;
		boost::this_thread::sleep(boost::posix_time::microseconds(100000));

	}
	
	//stop the transmission
	dev.stop();
	cout<<"device stopped"<<endl;
	return 0;
}