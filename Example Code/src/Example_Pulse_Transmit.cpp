/// USRP wrapper usage example - pulse transmit
///
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 19/11/2012
///

#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../Const.hpp"
#include "../../Function.hpp"
#include "../../Sinusoid.hpp"
#include "../../Exceptions.hpp"
#include "../../Function_Wrappers.hpp"
#include <cmath>
#include <iostream>




using namespace std;

int main(){

	//define pi
	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(4000000);
	dev.setCenterFrequency(0);
	dev.setGain(0);

	//init the device
	dev.init();

	//create an analytical function to output to the device 
	Sinusoid<float> pulse(1,1000000,0);		
	Sinusoid<float> pulse2(1,1200000,0);		
	

	//convert the analytical function to a data sample
	DataSet<float> waveSample(pulse,4000000,0,0.001);				
	DataSet<float> waveSample2(pulse2,4000000,0.001,0.002);		
	
	//populate the data sample
	waveSample.populate();
	waveSample2.populate();

	//concatenate the two datasets
	waveSample.concatenate(waveSample2);		//add the data from the second DataSet to the back of the first one


	//start sending pulses of the concatenated DataSet
	for(int i = 0; i < 10000; i++){
		dev.pulse(waveSample);	
	}	
}
