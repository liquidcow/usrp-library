#include <cmath>
#include <iostream>

#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../Const.hpp"
#include "../../Function.hpp"
#include "../../Sinusoid.hpp"
#include "../../Exceptions.hpp"
#include "../../Function_Wrappers.hpp"
#include "../../QAM.h"
#include <boost/thread.hpp>
#include <fstream>

using namespace std;
using namespace boost;

int main()
{
	//set up the QAM parameters
	float pi = 4*atan(1);
	float sampleRate = 4000000;
	float QAMFrequency = 1000000;

	//construct the signal
	Sinusoid<float> signal(1,2000*pi,0);
	DataSet<float> signalSample(signal,sampleRate, 0, 1);	//careful not to make the data sets too large as you'll get underruns
	signalSample.populate();

	//construct the constellations
	QAM QAM16(16, QAMFrequency, 10, sampleRate); 
	QAM QamSlice(16, QAMFrequency, 10, sampleRate);

	//string word = "(";

	string words = "Hello world! This is us testing out the 4-QAM modulation scheme!"; 
	//string words = "(hello\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n(\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";	
	
	QAM16.push_back(words); 
	QamSlice.push_back(words);

	DataSet<float> symb = QamSlice.pop();
	DataSet<float> QAMSignal = QAM16.pop();
	cout << "symb size " << symb.size() << endl;
	cout << "QAMSignal size " << QAMSignal.size() << endl;
	//DataSet<float> q = symb.slice(0,symb.size());
	cout << "here" <<endl;
	for (unsigned long k = 0; k<QAMSignal.size()/8-symb.size(); k++){
		//cout << "in loop " << endl;
		//cout << "k = " << k << " symb " << symb.size() << endl;
		DataSet<float> t = QAMSignal.slice(k*8, symb.size()+(k*8));
		//cout << "t size " << t.size() << endl;
		//DataSet<double> q = signalSample.slice(1000000,1000040);
		//cout<<"size " << t.size() << " " << q.size() << endl;
		//if (t.size()==40){
			//out << symb.correlate(t)*symb.correlate(t)*symb.correlate(t) << endl;
		//out << t.size() << endl;	
	
	}

	
	//cout << "original dataset size: " << signalSample.size() << endl;
	//cout << "new size: " << t.size() << endl;
}