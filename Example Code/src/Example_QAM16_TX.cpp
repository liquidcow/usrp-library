#include "QAM.h"
#include "Modulator.h"
#include "DataSet.hpp"
#include "Sinusoid.hpp"
#include "Const.hpp"
#include "usrp.hpp"
#include <cmath>
#include <iostream>
#include <boost/progress.hpp>
#include <fstream>

using namespace std;
using namespace boost; 


int main(){
	progress_timer t; 
	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(4000000);
	dev.setCenterFrequency(1000000);
	dev.setGain(0);
	dev.setSubdevice("");

	//init the device
	dev.init();

	QAM QAM16(16, 100000.0, 10, 4000000); 

	string words = "Hello world! This is us testing out the 16-QAM modulation scheme!"; 

	//string words = "bdebd";
	Const<float> cons(0.0); 
	DataSet<float> zeros(cons, 4000000,0, 0.1); 
	zeros.populate();
	QAM16.push_back(words); 



	DataSet<float> Beeeg = QAM16.pop(); 
	cout<<"signal build complete"<<endl;

	/*ofstream out;
	out.open("preamble.m");
	out << "x=[" << endl;
	for (int k = 0; k < Beeeg.size()-1; k++)
		out << Beeeg[k].real() << endl;
	out << "];\n plot(x);";
	out.close();
*/
	Beeeg.concatenate(zeros); 
	for (int i =0; i<1500; i++)
	{
		dev.queue(Beeeg);
	}
	dev.stop();
	cout<<"complete"<<endl;
}
