/// Correlation test file
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 26/11/2012
///

#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../Const.hpp"
#include "../../Function.hpp"
#include "../../Sinusoid.hpp"
#include "../../Exceptions.hpp"
#include "../../Function_Wrappers.hpp"
#include <cmath>
#include <iostream>
#include <boost/thread.hpp>


using namespace std;
using namespace boost;

// test the correlation function

template <class T>
struct populateStruct
{
    populateStruct(DataSet<T> &w){
        w.populate();
    }
    void operator()()
    {}
};

int main(){

	//define pi
	float pi = 4*atan(1);
	
	//create an analytical function 
	
		Sinusoid<double> signal(5,2000*pi,0);
		DataSet<double> signalSample(signal,4000000, 0, 0.01);	//careful not to make the data sets too large as you'll get underruns
		//	cout << "after dataset creation, before population t1" << endl;
		populateStruct<double> a(signalSample);
		thread t1(a);

		t1.join();
	for (int k=0;k<1000;k++){
		//cout << "before signals" << endl;
		
		Sinusoid<double> signal2(1,2000*pi,k*pi*0.01);
		//cout << "after signals, before dataset creation" << endl;
		//construct the DataSet equivalent of the analytical signal		



		DataSet<double> signalSample2(signal2,4000000,0,0.01);
		//	cout << "after dataset creation, before population t2" << endl;
		populateStruct<double> b(signalSample2);
		thread t2(b);
		
		//compute the signal
		t2.join();

		//cout << "populated " << signalSample.size() << endl;
		float result = signalSample.correlate(signalSample2);
		//cout<<"Test Complete with result = " << result<<endl;
		cout << result << endl;
	}
}
