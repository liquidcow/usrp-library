/// USRP wrapper usage example - 4QAM rx example
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 30/11/2012
///

#include <cmath>
#include <iostream>
#include <string>

#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../QAM_Demod.h"
#include "../../Syncro.h"
#include "../../FrameBuilder.hpp"
#include "../../Exceptions.hpp"

using namespace std;

int main(){

	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;


	//configure the device
	dev.setSampleRate(4000000);
	dev.setCenterFrequency(1000000);
	dev.setGain(0);
	//dev.setSamplesPerBuffer(30);

	//init the device
	dev.init();
	int count = 0;

	//create a FrameBuilder object
	FrameBuilder<float> fb(10e-4,1024);
	
	//start sampling
	dev.startSampling();

	cout<<"Listening..."<<endl;
	//start capturing the results 

	while (fb.frameQueueSize()<=1)
	{
		if(dev.RXQueueSize()>10){
			//push the packet stream onto the FrameBuilder
			count++;
			fb.push_back(dev.sample());
		}else{
			//chill for a bit if there are no packets ready from the usrp - reduces CPU load overall
			boost::this_thread::sleep(boost::posix_time::microseconds(200000));
			//cout<<"frame queue size: "<<fb.frameQueueSize()<<"\tUSRP queue:"<<dev.RXQueueSize()<<endl;
		}
		
	}
	cout<<"Frame capture complete. Total processed packets: "<<count<<endl;
	dev.stop();

	DataSet<float> allFrames = fb.pop();	
	

	//allFrames.plot("QuadraturePart.m");
	//allFrames.plotImag("InphasePart.m");

	
	cout<<"complete"<<endl;
	return 0;
}
