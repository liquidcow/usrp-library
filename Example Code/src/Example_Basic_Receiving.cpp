#include <iostream>

#include "../../usrp.hpp"

using namespace std;

int main(){
	
	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(4000000);
	dev.setCenterFrequency(1000000);
	dev.setGain(0);

	//init the device
	dev.init();
	
	//start sampling
	dev.startSampling();

	cout<<"Listening..."<<endl;
	//start capturing the results 

	while (dev.RXQueueSize()<=10000)
	{
		//chill for a bit if there are not enough packets ready from the usrp - reduces CPU load overall
		boost::this_thread::sleep(boost::posix_time::microseconds(200000));
		cout<<"USRP sample queue size: "<<dev.RXQueueSize()<<endl;
	}

	dev.stop();

	//get a packet off the USRP queue
	DataSet<float> packet = dev.sample();	
	
	cout<<"Complete."<<endl;
}