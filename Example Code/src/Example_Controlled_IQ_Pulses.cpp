/// USRP wrapper usage example - IQ pulse example
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 22/11/2012
///

#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../Const.hpp"
#include "../../Function.hpp"
#include "../../Sinusoid.hpp"
#include "../../Exceptions.hpp"
#include "../../Function_Wrappers.hpp"
#include <cmath>
#include <iostream>

using namespace std;
using namespace boost;

int main(){

	//define pi
	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(4000000);
	dev.setCenterFrequency(0);
	dev.setGain(0);

	//init the device
	dev.init();

	//make an I and Q signal
	Sinusoid<float> Isignal(1,1000000,0);
	Sinusoid<float> Qsignal(0.5,1200000,0);

	//construct the DataSet equivalent of the analytical signal		
	DataSet<float> signalSample(Isignal,Qsignal,4000000, 0, 0.001);	//careful not to make the data sets too large as you'll get underruns
	//compute the signal
	signalSample.populate();

	//pulse the signal a few times
	for(int i = 0;i < 10000; i++){	
		//transmit the signal
		dev.pulse(signalSample);
	}
	cout<<"stream complete"<<endl;
}
