/// USRP wrapper usage example - Simple signal transmit
///
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 6/12/2012
///

#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../Const.hpp"
#include "../../Function.hpp"
#include "../../Sinusoid.hpp"
#include "../../Exceptions.hpp"
#include "../../Function_Wrappers.hpp"
#include <cmath>
#include <iostream>

using namespace std;

int main(){

	//define pi
	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(4000000);
	dev.setCenterFrequency(0);
	dev.setGain(0);

	//init the device
	dev.init();

	//create an analytical function to output to the device 
	Sinusoid<float> sine(1,100000,0);		
	Sinusoid<float> cosine(1,100000,pi/2);		
	Const<float> zero(0.5);
	

	//convert the analytical function to a data sample
	DataSet<float> sineSample(sine,zero, 4000000,0,0.01);				
	DataSet<float> cosineSample(zero,cosine,4000000,0,0.01);	
	DataSet<float> bothSample(sine,cosine,4000000,0,0.1);
	DataSet<float> constSample(zero,zero,4000000,0,0.1);	


	//populate the data sample
	sineSample.populate();
	cosineSample.populate();
	bothSample.populate();
	constSample.populate();
	//bothSample.plot("bothI.m");
	//bothSample.plotImag("bothQ.m");

	//concatenate the two datasets
	//waveSample.concatenate(waveSample2);		//add the data from the second DataSet to the back of the first one
	//padding.concatenate(bothSample);

	//start sending pulses of the concatenated DataSet
	cout<<"Sending"<<endl;
	for(int i = 0; i < 100; i++){

			dev.queue(bothSample);

	}

	dev.stop();
	cout << "complete" << endl;
}
