#include "Modulator.h"
#include "DataSet.hpp"
#include "Exceptions.h"
#include <queue>
#include <string>
#include <fstream>

using std::string;
using std::queue; 

#ifndef PREAMBLETESTER_H
#define PREAMBLETESTER_H

/**
* /class PreambleTester 
* /brief 
* 
*  
*
* /author
* /version 1.0
* /date 28/11/2012 
*/

class PreambleTester{
public:
	PreambleTester();
	~PreambleTester();
	void run(Modulator &scheme, string s);

private:
	std::ofstream dump;
	std::ofstream preamblelog;
	string generatePreamble();
	unsigned char rPreamble[10];
	int index; 

};

#endif