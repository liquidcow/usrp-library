#include "../../Noise.hpp"
#include "../../Function.hpp"
#include "../../Sinusoid.hpp"
#include "../../Function_Wrappers.hpp"
#include <iostream>
#include <fstream>
#include <boost/random.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <ctime>
using namespace boost;


using namespace std; 

int main()
{
	Noise<float> random(1.0, 0.0,0.8);
	Sinusoid<float> sine(1.0,100,0);

	add<float> add1(&sine, &random); 
	ofstream output;

	output.open("Sine_noise.m");
	output<<"x=["<<endl; 
	float place=0.0; 

	for (int i = 0; i<300;i++)
	{
		output<<add1(place)<<endl;
		place+=0.001;
	}

	output<<"]; \n plot(x);"<<endl;

	output.close(); 


	return 0; 

}