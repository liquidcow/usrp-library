/// USRP wrapper usage example - Graphing example
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 10/12/2012
///

#include "../../DataSet.hpp"
#include "../../QAM_Demod.h"
#include "../../QAM.h"
#include "../../Graph.hpp"
#include "../../Exceptions.hpp"
#include <cmath>
#include <iostream>
#include <string>

using namespace std;

int main(){
	//define pi
	float pi = 4*atan(1);

	//define QAM parameters
	double sampleRate = 8000000;
	int periodsPerSymbol = 10;
	double freq = 100000;

	//a message to "transmit"
	string message = "hello world. This is an example of a message!";
	//string message = "Wireless communication is the transfer of information between two or more points that are not connected by an electrical conductor. The most common wireless technologies use electromagnetic wireless telecommunications, such as radio. With radio waves distances can be short, such as a few metres for television remote control, or as far as thousands or even millions of kilometres for deep-space radio communications. It encompasses various types of fixed, mobile, and portable applications, including two-way radios, cellular telephones, personal digital assistants (PDAs), and wireless networking. Other examples of applications of radio wireless technology include GPS units, garage door openers, wireless computer mice, keyboards and headsets, headphones, radio receivers, satellite television, broadcast television and cordless telephones. Less common methods of achieving wireless communications include the use of light, sound, magnetic, or electric fields.";
	
	//create a signal of the message
	QAM QAM16(16,freq,periodsPerSymbol,sampleRate);
	QAM16.push_back(message);
	DataSet<float> data = QAM16.pop();

	//create a QAM16 demodulation object
	QAM_Demod QAM16_Demod(16,0.1,freq,periodsPerSymbol, sampleRate, 0); 

	//create a graphing object
	Graph<float> plotter(sampleRate);
	
	//data.gainCorrect(6);
	cout<<"started graphing..."<<endl;
	plotter.plot("data",data, freq, periodsPerSymbol);
	//plotter.plot("data",data);
	//plotter.plotInphase("data",data);
	//plotter.plotQuadrature("data",data);
	//plotter.plotConstellation("data",data);
	
	//output the demodulated message
	cout<<"Complete."<<endl;
	
	QAM16_Demod.push_back(data);
	string demodMessage = QAM16_Demod.pop();
	cout<<"Original Message: "<<message<<"\nDemodulated Message: "<<demodMessage<<endl;
	return 0;
}
