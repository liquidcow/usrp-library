/// USRP wrapper usage example - frequency sweep using pulses
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 19/11/2012
///


#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../Const.hpp"
#include "../../Function.hpp"
#include "../../Sinusoid.hpp"
#include "../../Exceptions.hpp"
#include "../../Function_Wrappers.hpp"
#include <cmath>
#include <iostream>

using namespace std;
using namespace boost;

int main(){

	//define pi
	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(40000000);
	dev.setCenterFrequency(0);
	dev.setGain(0);

	//init the device
	dev.init();

	//create a starting frequency 
	Sinusoid<float> signal(1,1000000,0);		
	DataSet<float> signalSample(signal,40000000,0,0.01);


	cout<<"Building the sweep..."<<endl;
	for(double i = 0.01;i < 5; i = i +0.01){
		//create incremental sinusoids
		Sinusoid<float> temp(1,i*10000000,0);		
		DataSet<float> tempSample(temp,4000000,i,i+0.01);
		tempSample.populate();

		//append the incremented sinusoid to the growing DataSet
		signalSample.concatenate(tempSample);
	}
	cout<<"Sweep built."<<endl;

	//transmit the sweep through the usrp 10 times
	for(int i = 0; i < 10; i++){
		dev.pulse(signalSample);
	}
	
	dev.stop();
	cout<<"stream complete"<<endl;
}
