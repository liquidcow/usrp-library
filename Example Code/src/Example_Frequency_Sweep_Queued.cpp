/// USRP wrapper usage example - frequency sweep
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 19/11/2012
///

#include "../../Const.hpp"
#include "../../Function.hpp"
#include "../../Sinusoid.hpp"
#include "../../Exceptions.hpp"
#include "../../Function_Wrappers.hpp"
#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include <cmath>
#include <iostream>

using namespace std;
using namespace boost;

int main(){

	//define pi
	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(4000000);
	dev.setCenterFrequency(0);
	dev.setGain(0);

	//init the device
	dev.init();

	for(int i = 10000;i < 2000000; i = i +1000){
		//create an analytical function to output to the device 
		Sinusoid<float> signal(1,i,0);
		//construct the DataSet equivalent of the analytical signal		
		DataSet<float> signalSample(signal,4000000, 0, 0.1);	//careful not to make the data sets too large as you'll get underruns
		//compute the signal
		signalSample.populate();
		//transmit the signal
		dev.queue(signalSample);
	}
	cout<<"stream complete"<<endl;
}
