/// USRP wrapper usage example - 4QAM rx example
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 30/11/2012
///

#include "usrp.hpp"
#include "DataSet.hpp"
#include "QAM_Demod.h"
#include "Syncro.h"
#include "FrameBuilder.hpp"
#include "Exceptions.hpp"
#include <cmath>
#include <iostream>
#include <string>

using namespace std;

int main(){

	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;


	//configure the device
	dev.setSampleRate(4000000);
	dev.setCenterFrequency(6000000);
	dev.setGain(0);
	//dev.setSamplesPerBuffer(30);

	//init the device
	dev.init();
	int count = 0;

	//create a FrameBuilder object
	FrameBuilder<float> fb(10e-4,1024);
	//create a syncro
	QAM QAM4Preamble(4,400000,1,4000000);
	QAM QAM4(4,500000,1,4000000);
	//string pre = "|bdebdbdebd";
	unsigned char a = 0x99;

	string pre = "";
	pre+= a;

	//create a QAM16 demodulation object
	QAM_Demod QAM4_Demod(QAM4,0.2, 0); 

	QAM4Preamble.push_back(pre);
	DataSet<float> preamble = QAM4Preamble.pop();
	//preamble.plot("preamble.m");
	Syncro sync(QAM4,preamble);

	
	//start sampling
	dev.startSampling();

	cout<<"Listening..."<<endl;
	//start capturing the results 
	while (fb.frameQueueSize()<=10)
	{
		if(dev.RXQueueSize()>10){
			//push the packet stream onto the FrameBuilder
			count++;
			fb.push_back(dev.sample());
		}else{
			//chill for a bit if there are no packets ready from the usrp - reduces CPU load overall
			boost::this_thread::sleep(boost::posix_time::microseconds(200000));
			//cout<<"frame queue size: "<<fb.frameQueueSize()<<"\tUSRP queue:"<<dev.RXQueueSize()<<endl;
		}
		
	}
	cout<<"Frame capture complete. Total processed packets: "<<count<<endl;
	dev.stop();

	Const<float> zero(0);
	DataSet<float> allFrames(zero,1000000,0,0);
	//write the frames to a file
	while(fb.frameQueueSize()>2){
		//get the frame from the FrameBuilder
		DataSet<float> frame = fb.pop();
		allFrames.concatenate(frame);
		
		//frame.plot("pregain.m");
		//write the frame to a file for analysis
		cout<<"writing frame: "<<fb.frameQueueSize()<<endl;
		
		frame.gainCorrect(1);
		DataSet<float> sig = sync.RemovePreamble(frame); 
		//sig.plot("removed.m");
		
		
		//give it to the demodulator
		cout<<"Demodulating..."<<endl;
		for(int i = 0; i < sig.size() && i < 200;i++){
			DataSet<float> tagCorrected = sig.slice(i,sig.size());
			QAM4_Demod.push_back(tagCorrected);
			string message = QAM4_Demod.pop();
			if(message[0] == 'h' && message[1] == 'e' && message[2] == 'l'){
				//output the result 
				cout<<endl<<"Message: "<<QAM4_Demod.pop()<<endl<<endl;
				break;
			}
			
		}
		cout<<"Failed."<<endl;
		
		
	}
	//allFrames.plot("output.m");
	
	

	
	cout<<"complete"<<endl;
	return 0;
}
