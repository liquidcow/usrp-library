
#include <vector>

#include <iostream>
#include <cmath>
#include "Exceptions.hpp"


#include <boost/math/special_functions/round.hpp>

using namespace std;
using namespace boost::math;

std::vector<int> getbits(unsigned char byte, int M_size)
{
	std::vector<int> v;
	unsigned char tmp = 0;
	unsigned char mask = 0xff>>(8-M_size);
	unsigned int max = 8;
        
        if (M_size > 8)
        {
                throw InvalidNumOfBits(); 
        }
	
        max = ceil (8/(double)M_size);
	
	for (int k = 0; k<max; k++){
		
		tmp = byte & mask;
		byte >>= M_size;
             
		v.push_back((int)tmp);
	}

	return v;
}

int main()
{
	std::vector<int> v;
	for (int s = 1; s<=8; s++)
		cout << s << "  " << iround(8/s) << endl;
	v = getbits(255, 3);
	for (int k = 0; k<v.size(); k++)
		cout << v.at(k) << ",";
}

