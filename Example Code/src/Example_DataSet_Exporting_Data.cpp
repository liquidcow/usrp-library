#include "../../DataSet.hpp"

#include <iostream>

using namespace std;

int main(){

	//create an empty DataSet
	DataSet<float> A;

	//export the inphase function
	Function<float>& func = A.inphaseFunction();
	
	//export the quadrature function
	Function<float>& qfunc = A.quadratureFunction();

	//export the complex vector storing the discrete values
	vector<complex<float> > data = A.exportData();

	cout<<"func(10) = "<<func(10)<<"\t"<<"qfunc(10) = "<<qfunc(10)<<"\t"<<"IQdata size: "<<data.size()<<endl;
}