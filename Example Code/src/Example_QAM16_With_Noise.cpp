#include "../../QAM.h"
#include "../../Modulator.h"
#include "../../DataSet.hpp"
#include "../../Sinusoid.hpp"
#include "../../usrp.hpp"
#include "../../Graph.hpp"
#include <cmath>
#include <iostream>
#include <boost/progress.hpp>

using namespace std;
using namespace boost; 


int main(){

	progress_timer t; 
	float pi = 4*atan(1);
	float sampleRate = 10000000;
 
	QAM QAM16_With_Noise (16, 1000000.0, 10, sampleRate);

	string words = "Hello world! This is us testing out the 16-QAM modulation scheme!"; 
	//string words = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()_+1234567890-=[]|';:'{/.,<>?";
	//string words = "abcdef";
	

	//QAM16.push_back(words); 
	QAM16_With_Noise.push_back(words);

	//DataSet<float> NoNoise = QAM16.pop(); 
	DataSet<float> WithNoise= QAM16_With_Noise.pop();
	//NoNoise.plot("Testing_QAM_No_Noise.m"); 
	Graph<float> plotter(sampleRate);
	plotter.plot("Testing_QAM_With_Noise",WithNoise);


	return 0; 
}