#include "QAM.h"
#include "Modulator.h"
#include "DataSet.hpp"
#include "Sinusoid.hpp"
#include "usrp.hpp"
#include "Syncro.h"
#include <cmath>
#include <iostream>
#include <cstdio>
#include "Demodulator.h"
#include "QAM_Demod.h"
#include "Graph.hpp"

#include <fstream>

using namespace std;
using namespace boost; 



int main(){

	cout<<"Building signals..."<<endl;
	float pi = 4*atan(1);
	const double SAMPLE_RT = 4000000;

	//create a graph/plot
	Graph<float> plotter(SAMPLE_RT);

	//create 2 QAM modulators
	QAM QAM16(16, 100000, 10, SAMPLE_RT); 
	QAM QAMpre(16, 100000, 5, SAMPLE_RT);

	// demodulate modulated and signal with preamble removed
	QAM_Demod QD(QAM16, 0.1, 0);

	string pre = "";

	//string words = "Hello";
	//string words = "a"; 
	//string words = "1234567890 abcdefghij klmnopqrst uvwxyz";
	//string words = "Hi Jaco & Ling!";
	//string words = "hello world hello again";
	string words = "Hello there. This is a test message to check that a long message can be sent with our QAM16 system.";
	//string words = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789.";
	//string words = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	Const<float> zero(0);
	DataSet<float> zeros(zero, SAMPLE_RT, 0, 0.00175);
	zeros.populate();

	Const<float> longzero(0);
	DataSet<float> longzeros(zero, SAMPLE_RT, 0, 0.05);

	longzeros.populate();

	unsigned char a = 0xAB;
	unsigned char b = 0xFE;
	unsigned char c = 0x45;
	unsigned char d = 0x01;
	unsigned char e = 0x32;
	unsigned char f = 0x76;
	unsigned char g = 0xCD;
	unsigned char h = 0x98;

	pre+=a;
	pre+=b;
	pre+=c;
	pre+=d;
	pre+=e;
	pre+=f;
	pre+=g;
	pre+=h;

	
	QAMpre.push_back(pre);
	DataSet<float> tmo = QAMpre.pop();

	Syncro synced(QAM16, tmo);
	
	// add preamble to header
	DataSet<float> HeaderPacket = synced.AddPreamble(words);


	//QAM16.push_back(words);

	//save the payload signal
	//DataSet<float> dWords = QAM16.pop();
	//plotter.plot("PureModulator", dWords);
	//plotter.plot("HeaderPacket", HeaderPacket);	
	zeros.concatenate(HeaderPacket);
	plotter.plot("zeros", zeros);
	cout<<"\tcomplete"<<endl;
	
	cout<<"Syncronising..."<<endl;
	DataSet<float> Removed = synced.RemovePreamble(zeros);
	cout<<"\tcomplete."<<endl;
	
	//plotter.plot("removed",Removed);
	

	cout << "Demodulating..."<<endl;

	// correct gain to demodulate correctly
	Removed.gainCorrect(-5);

	QD.push_back(Removed);		//from syncroniser

	//obtain demodulated string
	string out = QD.pop();		
	cout<<"\tcomplete."<<endl;
	cout <<endl<< "\nResult:\n";
	cout << "Synced: " << out << endl << "Expected: " << words << endl;

	return 0;
}
