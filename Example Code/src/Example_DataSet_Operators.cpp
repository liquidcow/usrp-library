#include "../../Sinusoid.hpp"
#include "../../DataSet.hpp"
#include "../../Graph.hpp"

using namespace std;

int main(){

	//construct the signal functions
	float pi = 4*atan(1);
	float frequency = 2000;
	Sinusoid<float> inphaseSignal(1,frequency,0);
	Sinusoid<float> quadratureSignal(1,frequency,pi/2);

	Const<float> c(2);

	//define the constraints
	float startTime = 0;
	float endTime = 0.01;
	float sampleRate = 4000000;

	//make a graphing tool
	Graph<float> plotter(sampleRate);

	//create the DataSet object.
	DataSet<float> A(inphaseSignal,quadratureSignal,sampleRate, startTime, endTime);
	//create a second DataSet object.
	DataSet<float> B(quadratureSignal,inphaseSignal,sampleRate, startTime, endTime);
	//create a constant dataset (straight line value 2)
	DataSet<float> constant(c,c,sampleRate,startTime,endTime);

	//populate the DataSets
	A.populate();
	B.populate();
	constant.populate();

	//create an empty DataSet
	DataSet<float> C;

	//addition of two sinusoids
	C = A+B;
	plotter.plot("A_plus_B",C);
	//subtraction of two sinusoids
	C = A-B;
	plotter.plot("A_minus_B",C);
	//multiplication of two sinusoids
	C = A*A;
	plotter.plot("A_multiply_A",C);
	//multiplication of sinusoid by constant
	C = A*constant;
	plotter.plot("A_multiply_constant",C);
	//division of sinusoid by constant
	C = A/constant;
	plotter.plot("A_divided_constant",C);

	cout<<"complete"<<endl;
}