#include "../../DataSet.hpp"
#include "../../usrp.hpp"
#include <cmath>
#include <iostream>
#include <fstream>

using namespace std;
using namespace boost; 


int main(){
	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(4000000);
	dev.setCenterFrequency(1000000);
	dev.setGain(0);


	//init the device
	dev.init();
	int count = 0;

	//start sampling
	dev.startSampling();
	
	
	//ensure the device has stopped streaming and "process" the remaining results in the queue
	while (count< 10000)
	{
		if(dev.RXQueueSize()>1){
			DataSet<float> samples = dev.sample();
			count++;
		}else{
			boost::this_thread::sleep(boost::posix_time::seconds(1));
		}
		//cout<<"queue size: "<<dev.RXQueueSize()<<"\t"<<"processed packets "<<count<<endl;
	}
	cout<<"queue size: "<<dev.RXQueueSize()<<"\t"<<"processed packets"<<count<<endl;
	
	dev.stop();
	
	//chill out while the devices shutdown
	dev.stop();
	
	cout<<"complete"<<endl;
	return 0;
}