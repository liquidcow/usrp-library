#include "QAM.h"
#include "Modulator.h"
#include "DataSet.hpp"
#include "Sinusoid.hpp"
#include "usrp.hpp"
#include "Syncro.h"
#include "Demodulator.h"
#include "QAM_Demod.h"
#include "Graph.hpp"
#include <cmath>
#include <iostream>


#include <fstream>

using namespace std;
using namespace boost; 



int main(){
	float pi = 4*atan(1);
	float sampleRate = 4000000;
	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(sampleRate);
	dev.setCenterFrequency(6000000);
	dev.setGain(0);

	//init the device
	dev.init();

	QAM QAM16(4, 500000, 10, sampleRate); 
	QAM QAMpre(4, 500000.0, 1, sampleRate);


	string pre = "";


	string pre2 = " ";

	QAM16.push_back(pre2);
	string words = "Hello world! This is us testing out the 4-QAM modulation scheme!"; 
	DataSet<float> randData = QAM16.pop();

	Const<float> zero(0);
	DataSet<float> zeros(zero, sampleRate, 0, 0.05);
	zeros.populate();

	unsigned char q4 = 0x99;

	pre+=q4;
	//randData.concatenate(zeros);
	//zeros.concatenate(randData);

	
	//string words = "my test string";	
	QAMpre.push_back(pre);
	DataSet<float> tmo = QAMpre.pop();
	//tmo.gainCorrect(426);

	cout << "Autocorrelation of preamble: " << tmo.correlate(tmo) << endl;

	Syncro synced(QAM16, tmo);
	
	DataSet<float> HeaderPacket = synced.AddPreamble(words);
	Sinusoid<float> s1(1e-5,12345677,2.14);	//some dirty noise
	DataSet<float> d1(s1, sampleRate, 0, 0.001);
	d1.populate();

	//save the pramble signal
	ofstream preambleSave;
	preambleSave.open("preamble.m");
	preambleSave<<"x=[" << std::endl;

	//tmo.normalise(1);
	for(int i = 0; i < tmo.size()-1;i++){
		preambleSave<<tmo[i].real()<<endl;
	}

	preambleSave << "];" << std::endl;
	preambleSave << "plot(x)";
	preambleSave.close();

	QAM16.push_back(words);

	//save the payload signal
	preambleSave.open("signal.m");
	preambleSave<<"x=[" << std::endl;
	DataSet<float> dWords = QAM16.pop();
	//dWords.gainCorrect(4.246);
	for(int i = 0; i < dWords.size()-1;i++){
		preambleSave<<dWords[i].real()<<endl;
	}
	preambleSave << "];" << std::endl;
	preambleSave << "plot(x)";
	preambleSave.close();


	HeaderPacket.gainCorrect(4.26);

	d1.concatenate(HeaderPacket);
	//d1.normalise(1);
	zeros.concatenate(HeaderPacket);
	//zeros.plot("zeros.m");
	cout<<"signal build complete"<<endl;



	for (int i =0; i<100; i++)
	{
		dev.queue(zeros);
	//	boost::this_thread::sleep(boost::posix_time::seconds(1));
	}
	dev.stop();

	//ofstream signalcorrelation;
	//HeaderPacket.gainCorrect(4.246);
	//signalcorrelation.open("sc.m");
	//signalcorrelation << "x=[" << endl;
	//for (int k = 0; k < HeaderPacket.size()-1; k++)
	//	signalcorrelation << tmo.correlate(HeaderPacket) << endl;
	//signalcorrelation << "];\n plot(x);";
	//signalcorrelation.close();
	

	zeros.gainCorrect(4.26);
	cout << endl << "autocorrelation of noise: " << d1.correlate(d1) << endl;
	//zeros.plot("data1.m");
	DataSet<float> Removed = synced.RemovePreamble(d1);
	//Removed.gainCorrect(4.26);
	if(Removed.size()>0){
		cout<<"Actually found the payload!"<<endl;
		//Removed.plot("removed.m");
	}
	
	// demodulate modulated and signal with preamble removed
	QAM_Demod QD(QAM16, 0.1, 0);
	QAM_Demod QD2(QAMpre, 0.1, 0);
	// correct gain to demodulate correctly
	dWords.gainCorrect(1);
	QD2.push_back(dWords);
	QD.push_back(Removed);

	string out = QD.pop();
	string rel = QD2.pop();
	cout <<"Syncro: "<< out << endl <<"Expected: "<< rel << endl;
	cout << Removed.size() << " " << dWords.size() << endl;
	if (Removed.size() == dWords.size())
		cout << "same size " << endl;
	else
		cout << "Not same" << endl;

	return 0;
}
