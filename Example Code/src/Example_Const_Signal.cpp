#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../Const.hpp"
#include "../../Function.hpp"
#include "../../Sinusoid.hpp"
#include "../../Exceptions.hpp"
#include "../../Function_Wrappers.hpp"
#include <cmath>
#include <iostream>

using namespace std;
using namespace boost;


int main(){

	//define pi
	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(4000000);
	dev.setCenterFrequency(1000000);
	dev.setGain(0);

	//init the device
	dev.init();

	//create a starting frequency 
	Const<float> signal(0.5);		
	DataSet<float> signalSample(signal,4000000,0,0.25);
	signalSample.populate();

	cout<<"Sweep built."<<endl;

	//transmit the sweep through the usrp 10 times
	for(int i = 0; i < 50; i++){
		dev.pulse(signalSample);
	}
	
	cout<<"stream complete"<<endl;
}

