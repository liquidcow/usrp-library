/// USRP wrapper usage example - FrameBuilder example
///
/// By 
///	Ashton Hudson
///	Mike Geddes
///	Matthew van der Velden 

/// Date: 29/11/2012
///
#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../FrameBuilder.hpp"
#include "../../Exceptions.hpp"
#include <cmath>
#include <iostream>

using namespace std;

int main(){

	float pi = 4*atan(1);
	
	//create a USRP device
	usrp<float> dev;

	//create a matlab plotable output file to capture the frame for plotting
	ofstream output;
	output.open("output.m");
	output<<"x=[";

	//configure the device
	dev.setSampleRate(4000000);
	dev.setCenterFrequency(0);
	dev.setGain(0);
	//dev.setSamplesPerBuffer(30);

	//init the device
	dev.init();
	int count = 0;

	//create a FrameBuilder object
	FrameBuilder<float> fb(0,4096);
	//start sampling
	dev.startSampling();
	//start "processing the results" 
	
	//ensure the device has stopped streaming and "process" the remaining results in the queue
	while (fb.frameQueueSize()<5)
	{
		if(dev.RXQueueSize()>10){
			//push the packet stream onto the FrameBuilder
			count++;
			fb.push_back(dev.sample());
		}else{
			//chill for a bit if there are no packets ready from the usrp
			//cout<<"frameQueueSize: "<<fb.frameQueueSize()<<"\t"<<"RXQueueSize: "<<dev.RXQueueSize()<<"\t"<<"processed packets"<<count<<endl;
			boost::this_thread::sleep(boost::posix_time::microseconds(2000000));
		}
		cout<<"frameQueueSize: "<<fb.frameQueueSize()<<"\t"<<"RXQueueSize: "<<dev.RXQueueSize()<<"\t"<<"processed packets"<<count<<endl;
	}
	cout<<"Frame capture complete. Total processed packets: "<<count<<endl;
	dev.stop();
	
	//write the frames to a file
	while(fb.frameQueueSize()>1){
		//get the frame from the FrameBuilder
		cout<<"writing frame: "<<fb.frameQueueSize()<<endl;
		DataSet<float> frame = fb.pop();
		for(int i = 0; i < frame.size()-1;i++){
			output<<frame[i].real()<<endl;
		}	
	}
	

	//close off the matlab file
	output<<"];\nplot(x);";
	output.close();

	//chill out while the devices shutdown
	while(dev.streaming()){
		boost::this_thread::sleep(boost::posix_time::seconds(1));
	}
	
	cout<<"complete"<<endl;
	return 0;
}
