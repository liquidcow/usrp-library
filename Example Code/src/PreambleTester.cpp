#include "PreambleTester.h"
#include "QAM.h"
#include <cmath>

using std::endl;

PreambleTester::PreambleTester()
{
	dump.open("dumpfile.m");
	dump << "x=[";
	preamblelog.open("pre.txt");
	srand(time(NULL));
	index = 1;
}

void PreambleTester::run(Modulator &scheme, string s)
{
	//std::cout << generatePreamble() << endl;
	//string prea = generatePreamble();
	string prea = "a";

	QAM QAMloc(4, 1000000.0, 10, 4000000); 
	QAMloc.push_back(prea);
	DataSet<float> d2 = QAMloc.pop();
	//d2.normalise(1);

	string loc = prea + s;
	scheme.push_back(loc);
	DataSet<float> d1 = scheme.pop();
	//d1.normalise(1);
	for (int k = 0; k < d1.size()/1 - d2.size(); k++){
		DataSet<float> t = d1.slice(k*1, d2.size() + k*1);
		dump << d2.correlate(t) << endl;
	}
}

PreambleTester::~PreambleTester()
{
	dump << "];" << endl;
	dump << "plot(x);";
	dump.close();
	preamblelog.close();
}

string PreambleTester::generatePreamble()
{
	string tmp = "";
	
	if (index < 10){
		if (rPreamble[index] > 255){
			rPreamble[index]++;
			index++;
		}
		else
			rPreamble[index]++;
	}

	for (int k=0; k < 10; k++){
		//unsigned rInt = rand() % 255;
		//char rChar = rInt;
		tmp += rPreamble[k];
		//int t = std::abs(rChar);
		//preamblelog << t << endl;
	}
	preamblelog << "---" << endl;
	return tmp;
}