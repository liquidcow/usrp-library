#include "../../usrp.hpp"
#include "../../DataSet.hpp"
#include "../../Sinusoid.hpp"
#include <cmath>

using namespace std;

int main(){
	//define pi
	float pi = 4*atan(1);

	//create a USRP device
	usrp<float> dev;

	//configure the device
	dev.setSampleRate(4000000);
	//set Center Frequency to 0 to operante in baseband
	dev.setCenterFrequency(0);
	dev.setGain(0);

	//init the device
	dev.init();

	//create an analytical function to output to the device - 2Mhz sinewave
	Sinusoid<float> signal(1,1000,0);
	Sinusoid<float> signal2(1,1000,pi/2);					
	
	//convert the analytical function to a data sample
	DataSet<float> discreteSignal(signal,signal2,4000000,0,0.1);    //DONT FORGET THE QUADRATURE COMPONENT!						

	//populate the data sample
	discreteSignal.populate();

	//start queuing up the signal's DataSet
	for(int i = 0; i < 1000; i++){
		dev.queue(discreteSignal);
	}
	
	//stop the device
	dev.stop();	
}	