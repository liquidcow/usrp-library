#include "Function.hpp"
#include "Sinusoid.hpp"
#include "DataSet.hpp"
#include <string>
#include <vector>
#include <iostream>
#include <climits>

using std::string;
using std::vector; 

#ifndef DEMODULATOR_H
#define DEMODULATOR_H

/**
* \class Demodulator
* \brief Demodulator is the base class for all demodulator techniques, including QAM and FSK
* \author Ashton Hudson
* \modified Serge Mbamba
* \version 2.0
* \date 06/12/2013 
*/
class Demodulator
{
public:
	virtual void push_back(DataSet<float>) = 0; 
	virtual string pop() = 0; 
protected:
	unsigned char getChar(vector<unsigned> , int M_size); 
}; 

#endif
