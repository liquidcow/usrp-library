#include "Sinusoid.h"
#include "Function.h"
#include "Const.h"
#include "Sinc.h"
#include "DataSet.hpp"
#include "Exceptions.h"
#include "usrp.hpp"
#include <gtest/gtest.h>
#include <cmath>


/*
static const size_t wave_table_len = 8192;
class wave_table_class{
public:
    wave_table_class(const std::string &wave_type, const float ampl):
        _wave_table(wave_table_len)
    {
        //compute real wave table with 1.0 amplitude
        std::vector<double> real_wave_table(wave_table_len);
        if (wave_type == "CONST"){
            for (size_t i = 0; i < wave_table_len; i++)
                real_wave_table[i] = 1.0;
        }
        else if (wave_type == "SQUARE"){
            for (size_t i = 0; i < wave_table_len; i++)
                real_wave_table[i] = (i < wave_table_len/2)? 0.0 : 1.0;
        }
        else if (wave_type == "RAMP"){
            for (size_t i = 0; i < wave_table_len; i++)
                real_wave_table[i] = 2.0*i/(wave_table_len-1) - 1.0;
        }
        else if (wave_type == "SINE"){
            static const double tau = 2*std::acos(-1.0);
            for (size_t i = 0; i < wave_table_len; i++)
                real_wave_table[i] = std::sin((tau*i)/wave_table_len);
        }
        else throw std::runtime_error("unknown waveform type: " + wave_type);

        //compute i and q pairs with 90% offset and scale to amplitude
        for (size_t i = 0; i < wave_table_len; i++){
            const size_t q = (i+(3*wave_table_len)/4)%wave_table_len;
            _wave_table[i] = std::complex<float>(ampl*real_wave_table[i], ampl*real_wave_table[q]);
        }
    }

    inline std::complex<float> operator()(const size_t index) const{
        return _wave_table[index % wave_table_len];
    }

private:
    std::vector<std::complex<float> > _wave_table;
};

*/
using namespace std;

///main function to compile and run tests
int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

//Sinusoid Tests
/*TEST(sinusoid, constructor_test){
    float pi = 4*atan(1);     //definition of pi
	float A = 2;		//amplitude
	float f = 1000;		//frequency 
	float ph = pi;		//phase shift
    
    //a specified sinusoid
/* TODO
    Sinusoid<float> wave(A,f,ph);
    EXPECT_TRUE(wave.amplitude()==A);
    EXPECT_TRUE(wave.frequency()==f);
    EXPECT_TRUE(wave.phase()==ph);
    */
    //a defualt sinusoid = sin(2pi)
/*
    Sinusoid<float> wave2;
    EXPECT_TRUE(wave2.amplitude()==1);
    EXPECT_TRUE(wave2.frequency()==1);
    EXPECT_TRUE(wave2.phase()==0);
*/
    //some bad declarations of a sinusoid
   // EXPECT_THROW(Sinusoid<float> wave3(1,-1000,pi),1);
//}




TEST(sinusoid, evaluation_test){
    float pi = 4*atan(1);     //definition of pi

    Sinusoid<float> defaultWave(1,1,0);
    EXPECT_TRUE(defaultWave(0)==0);            // @ t=0
    EXPECT_EQ(defaultWave(1/4),1.0);         // @ t=pi/2
    EXPECT_EQ(defaultWave(1/2),0);           // @ t=pi
    EXPECT_EQ(defaultWave(3/4),-1);   // @ t=(3*pi)/2
    
    //default sine wave test

    float A = 2;        //amplitude
    float f = 1;        //frequency 
    float ph = pi;      //phase shift
    
    //a specified sinusoid
    Sinusoid<float> wave(A,f,ph);

    EXPECT_EQ(wave(0),0);            // @ t=0
    EXPECT_EQ(wave(1/4),-2);         // @ t=pi/2
    EXPECT_EQ(wave(1/2),0);          // @ t=pi
    EXPECT_EQ(wave((3)/4),2);     // @ t=(3*pi)/2
    EXPECT_EQ(wave(1),0);         // @ t=pi/2
    

    A = 1;          //amplitude
    f = 1000;       //frequency 
    ph = 0;         //phase sshifthift
    
    //a specified sinusoid
    Sinusoid<float> wave2(A,f,ph);

    EXPECT_TRUE(wave2(0)==0);               // @ t=0
    EXPECT_TRUE(wave2(0.001*0.5*pi)==-0430302);       // @ t=pi/2
    EXPECT_TRUE(wave2(0.001*pi)==0.776854);         // @ t=pi
    EXPECT_TRUE(wave2(0.001*(3*pi)/2)==-0.97221);   // @ t=(3*pi)/2
    EXPECT_TRUE(wave2(0.001*2*pi)==0.97834);       // @ t=pi/2

    A = 1;          //amplitude
    f = 1;          //frequency 
    ph = pi/2;      //phase shift
    
    //a specified cosine wave
    Sinusoid<float> cosine(A,f,ph);

    EXPECT_TRUE(cosine(0)==1);              // @ t=0
    EXPECT_TRUE(cosine(pi/2)==0);           // @ t=pi/2
    EXPECT_TRUE(cosine(pi)==-1);            // @ t=pi
    EXPECT_TRUE(cosine((3*pi)/2)==0);       // @ t=(3*pi)/2
    EXPECT_TRUE(cosine(2*pi)==1);           // @ t=pi/2
}

/* TODO:
TEST(Sinusoid, operators_test){
    float pi = 4*atan(1);             //definition of pi
    Sinusoid<float> result;    
    Sinusoid<float> sine(1,1,0);       //unit sine wave
    Sinusoid<float> cosine(1,1,2*pi);  //unit cosine wave


    result = sine+cosine;
    EXPECT_TRUE(result(0)==1);              // @ t=0
    EXPECT_TRUE(result(pi/2)==1);           // @ t=pi/2
    EXPECT_TRUE(result(pi)==-1);            // @ t=pi
    EXPECT_TRUE(result((3*pi)/2)==-1);      // @ t=(3*pi)/2
    EXPECT_TRUE(result(2*pi)==1);           // @ t=pi/2
    
    result = sine-cosine;
    EXPECT_TRUE(result(0)==-1);                 // @ t=0
    EXPECT_TRUE(result(pi/2)==1);               // @ t=pi/2
    EXPECT_TRUE(result(pi)==1);                 // @ t=pi
    EXPECT_TRUE(result((3*pi)/2)==-1);          // @ t=(3*pi)/2
    EXPECT_TRUE(result(2*pi)==-1);              // @ t=pi/2

    result = sine*cosine;
    EXPECT_TRUE(result(0)==0);                      // @ t=0
    EXPECT_TRUE(result(pi/2)==6.1232e-17);          // @ t=pi/2
    EXPECT_TRUE(result(pi)==-1.2246e-16);           // @ t=pi
    EXPECT_TRUE(result((3*pi)/2)==-1.2246e-16);     // @ t=(3*pi)/2
    EXPECT_TRUE(result(2*pi)==-1.2246e-16);         // @ t=pi/2
    
    result = sine/cosine;
    //not evaluated due to inifnity issues
} */

//dataset tests

TEST(DataSet,constructor_tests){
    //DataSet<float> def;
    //EXPECT_THROW(def[12],EmptyDataSet);
   // EXPECT_TRUE(def.size() == 0);

    float pi = atan(1)*4;

    int samples = 100000;
    float start = 0;
    float end = 2*pi;
    //Const<float> defaultFn(0);
    //DataSet<float> def(defaultFn,samples,start,end);

    Sinusoid<float> sine(1,1000,0);
   
    DataSet<float> A(sine,samples,start,end);
    //EXPECT_TRUE(def.size() == 10000);
    //EXPECT_TRUE(def.start() == start);
    //EXPECT_TRUE(def.end() == end);

    start = 1;
    end = 0;
    EXPECT_THROW(DataSet<float> A(sine,samples,start,end), InvalidTimeDuration);

    samples = -1;
    EXPECT_THROW(DataSet<float> A(sine,samples,start,end), InvalidSampleSize);
}

TEST(DataSet, evaluation_test){
    float pi = 4*atan(1);             //definition of pi

    Sinusoid<float> sine(1,1,0);
    int samples = 100000;
    float start = 0;
    float end = 2*pi;
    
    DataSet<float> A(sine,samples,start,end);
    A.populate();

    EXPECT_TRUE(A[0] == 0);
    EXPECT_TRUE(A[1] == 6.2832e-05);
    EXPECT_TRUE(A[10] == 6.2832e-04);
    EXPECT_TRUE(A[100] == 0.0062831);
    EXPECT_TRUE(A[1000] == 0.062791);
    EXPECT_TRUE(A[10000] == 0.58779);
    EXPECT_TRUE(A[99999] == -6.2832e-05);
    EXPECT_THROW(A[-1] == 0, InvalidIndex);
    EXPECT_THROW(A[100001] == 0,InvalidIndex);

}

TEST(DataSet,operators_test){
 //   DataSet<float> result;
   // DataSet<float> def;
    //EXPECT_THROW(def[12],EmptyDataSet);
    //EXPECT_TRUE(result.size() == 0);


    // no default constructor so need to make own
 //   Const<float> defaultFn(0);
    DataSet<float> result;
    DataSet<float> def;
  //  EXPECT_THROW(def[12],EmptyDataSet);
   // EXPECT_TRUE(result.size() == 0);

    float pi = 4*atan(1);

    Sinusoid<float> sine(1,1,0);
    Sinusoid<float> cosine(1,1,2*pi);
    int samples = 100000;
    float start = 0;
    float end = 2*pi;
    
    DataSet<float> A(sine,samples,start,end);
    DataSet<float> B(cosine,samples,start,end);
    
    A.populate();
    B.populate();

   // result = A;
    //EXPECT_TRUE(result.size() == samples);
    //EXPECT_TRUE(result[10]==A[10]);

    //result = A + B;
    //for(int i = 0; i < samples; i++){
    //    EXPECT_TRUE(result[i]==(A[i]+B[i]));
   // }
    
    //result = A - B;
    //for(int i = 0; i < samples; i++){
    //    EXPECT_TRUE(result[i]==(A[i]-B[i]));
   // }

    /// TODO: following operators to be overloaded
/*
    result = A * B;
    for(int i = 0; i < samples; i++){
        EXPECT_TRUE(result[i]==(A[i]*B[i]));
    }

    result = A / B;
    for(int i = 0; i < samples; i++){
        EXPECT_TRUE(result[i]==(A[i]/B[i]));
    }
*/

}

TEST(Sinc, Sinc_Tests)
{
    Sinc<float> sinc(1, 0.1591549431, 0); \
    float e = 0.0001; 

    if (sinc(0)>1-e){
        EXPECT_TRUE(1); 
    }
    else 
    {
        EXPECT_TRUE(0); 
    }

    if (sinc(3.14159)<0+e||sinc(3.14159)>0-e)
    {
        EXPECT_TRUE(1); 
    }
    else 
    {
        EXPECT_TRUE(0); 
    }
}

TEST(Const, Constant_Tests)
{
    Const<float> con(312.23);
    EXPECT_TRUE(con(2)==con(21)); 
    EXPECT_TRUE(con(2)==312.23); 
}

TEST(USRP, construction_tests){
    usrp<float> testDevice;
    EXPECT_THROW(testDevice.setSampleRate(-1),InvalidUSRPSetting);
}

