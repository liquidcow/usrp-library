#include "DataSet.h"
template <class T>
DataSet<T>::DataSet():
  startTime(0),
  endTime(0),
  samples(0),
  readyForPopulation(false){

}

//constructor
template <class T>
DataSet<T>::DataSet(Function<T> &f,int samp,float sT,float eT):
  func(f),
  startTime(sT),
  endTime(eT),
  samples(samp),
  readyForPopulation(false){
    init();
}

// Overload subtraction.
template <class T> 
DataSet<T> DataSet<T>::operator-(const DataSet& rhs){ 
  DataSet temp;
  if(!sizeMatch(rhs)) throw DataSetSizeMismatch();
  for(int i = 0; i < size() ; i++){
     temp[i] = data[i]-rhs[i];
  }
  return temp; 
}
 
// Overload +. 
template <class T>
DataSet<T> DataSet<T>::operator+(const DataSet& rhs){ 
  DataSet temp;
  if(!sizeMatch(rhs)) throw DataSetSizeMismatch();
  for(int i = 0; i < size() ; i++){
     temp[i] = data[i]+rhs[i];
  }
  return temp; 
} 
 
// Overload assignment.
template <class T> 
DataSet<T> DataSet<T>::operator=(const DataSet& rhs){ 
  DataSet temp;
  if(!sizeMatch(rhs)) throw DataSetSizeMismatch();
  for(int i = 0; i < size() ; i++){
     temp[i] = data[i]-rhs[i];
  }
  return temp;
} 

template <class T>
T& DataSet<T>::operator[](int i){
  if(i >=0 && i < samples){
    return data[i];
  }
} 

//evaluate and populate the DataSet
template <class T>
void DataSet<T>::populate(){
  if(readyForPopulation){
    for(int i = startTime; i < endTime; i+=(endTime-startTime)/samples){
      data.push_back(func(i));
    }  
  }
}

//query DataSet Size
template <class T>
int DataSet<T>::size(){
  return data.size();
}

//query start time
template <class T>
float DataSet<T>::start(){
  return startTime;
}

//query end time
template <class T>
float DataSet<T>::end(){
  return endTime;
}

template <class T>
bool DataSet<T>::validTimeScales(){
  if(endTime > startTime)
    return true;
  else
    return false;
}

template <class T>
bool DataSet<T>::validSampleSize(){
  if(samples > 0){
    return true;
  }else{
    return false;
  }
}

template <class T>
bool DataSet<T>::sizeMatch(const DataSet& rhs){
  if(size()==rhs.size())
    return true;
  else
    return false;
}

template <class T>
void DataSet<T>::init(){
  data.clear();
  if(!validTimeScales()) throw InvalidTimeDuration();
  if(!validSampleSize()) throw InvalidSampleSize();
  readyForPopulation = true;
}