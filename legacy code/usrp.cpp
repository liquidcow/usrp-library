#include "usrp.h"
//constructor
template <class T>
usrp<T>::usrp():
	args(""),					//USRP args are default ""
	ant("TX/RX"), 				//recommended antenna
	subdev(""),				//default subdevice is "" 
	ref("internal"),			//default  USRP Clock is the internal clock 
	otw("sc16"),				//default over-the-wire sample mode is sc16
    spb(0),					//default samples per buffer 0;
    rate(250000),				//default sample rate of 250k hz 
    centerFreq(3000000), 		//default center frequency of 3Mhz
    gain(0),					//default USRP gain of 0dB  
    bw(60000000),				//default IF bandwidth of 60MHz
   	initialised(false),			//the init flag starts as false
   	stream(false)
    {

}

//set the sample rate
template <class T>
void usrp<T>::setSampleRate(double r){
	if(r < 1) 
		throw InvalidUSRPSetting();
	else
		rate = r;
}

//set center frequency
template <class T>
void usrp<T>::setCenterFrequency(double c){
	if(c < 1000000 || c > 249000000) 
		throw InvalidUSRPSetting();
	else
		centerFreq = c;
}

//set the gain
template <class T>
void usrp<T>::setGain(double g){
	if(g < 0 || g > 20)
		throw InvalidUSRPSetting();
	else
		gain = g;
}

//set daughterboard IF filter bandwidth
template <class T>
void usrp<T>::setIFBandwidth(double b){
	if(b < 0 || b > 249000000)
		throw InvalidUSRPSetting();
	else
		bw = b;
}

//set USRP args
template <class T>
void usrp<T>::setArgs(string a){
	args = a;
}

//set daughterboard antenna
template <class T>
void usrp<T>::setAntenna(string a){
	ant = a;
}

//set daughterboard subdevice
template <class T>
void usrp<T>::setSubdevice(string s){
	subdev = s;
}

//set over-the-wire sample mode
template <class T>
void usrp<T>::setOTW(string o){
	otw = o;
}

//set clock reference (internal, external, mimo)
template <class T>
void usrp<T>::setClockReference(string r){
	if(r != "internal" || r != "external" || r != "mimo")
		throw InvalidUSRPSetting();
	else
		ref = r;
}

//initialise the USRP
template <class T>
void usrp<T>::init(){
	if(!initialised){
    	//create the USRP device
    	cout<<endl<< boost::format("Creating the usrp device with: %s...") % args<<endl;
    	usrpDevice = uhd::usrp::multi_usrp::make(args);

    	//Lock mboard clocks
    	usrpDevice->set_clock_source(ref);

    	//set the sub device if specified
    	if (subdev != "") usrpDevice->set_tx_subdev_spec(subdev);
    	cout<< boost::format("Using Device: %s") % usrpDevice->get_pp_string() <<endl;

    	//set the sample rate
    	cout << boost::format("Setting TX Rate: %f Msps...") % (rate/1e6) <<endl;
    	usrpDevice->set_tx_rate(rate);
    	cout<< boost::format("Actual TX Rate: %f Msps...") % (usrpDevice->get_tx_rate()/1e6) <<endl<<endl;

    	//setup the channels
    	for(size_t chan = 0; chan < usrpDevice->get_tx_num_channels(); chan++) {
	    	//set the center frequency
	        cout<< boost::format("Setting TX Freq: %f MHz...") % (centerFreq/1e6) <<endl;
	        usrpDevice->set_tx_freq(centerFreq, chan);
	        cout << boost::format("Actual TX Freq: %f MHz...") % (usrpDevice->get_tx_freq(chan)/1e6) <<endl<<endl;

	        //set the rf gain
	        cout << boost::format("Setting TX Gain: %f dB...") % gain <<endl;
	        usrpDevice->set_tx_gain(gain, chan);
	        cout << boost::format("Actual TX Gain: %f dB...") % usrpDevice->get_tx_gain(chan) <<endl <<endl;

	        //set the IF filter bandwidth
	        cout << boost::format("Setting TX Bandwidth: %f MHz...") % bw <<endl;
	        usrpDevice->set_tx_bandwidth(bw, chan);
	        cout << boost::format("Actual TX Bandwidth: %f MHz...") % usrpDevice->get_tx_bandwidth(chan) <<endl<<endl;
	        
	        //set the antenna
	        usrpDevice->set_tx_antenna(ant, chan);
    	}

    	//allow for some hardware setup time

    	boost::this_thread::sleep(boost::posix_time::seconds(1));
    	cout<<"USRP Initialised."<<endl; 

	}else
		throw USRPAlreadyInitialised();
	initialised = true;
}

//stop streaming
template <class T>
void usrp<T>::stop(){
	stream = false;
}

//transmit a DataSet
template <class T>
void usrp<T>::send(DataSet<T> wave_table){
    //compute the step size
    const size_t step = boost::math::iround(usrpDevice->get_tx_rate() * wave_table.size());
    //const size_t step = boost::math::iround(wave_freq/usrp->get_tx_rate() * wave_table.size());
	
    //create a transmit streamer
    //linearly map channels (index0 = channel0, index1 = channel1, ...)
    uhd::stream_args_t stream_args("fc32", otw);
    for (size_t chan = 0; chan < usrpDevice->get_tx_num_channels(); chan++)
        stream_args.channels.push_back(chan); //linear mapping
    uhd::tx_streamer::sptr tx_stream = usrpDevice->get_tx_stream(stream_args);

    //allocate a buffer which we re-use for each channel
    if (spb == 0) spb = tx_stream->get_max_num_samps()*10;
    vector<complex<float> > buff(spb);
    vector<complex<float> *> buffs(usrpDevice->get_tx_num_channels(), &buff.front());

    //setup the metadata flags
    uhd::tx_metadata_t md;
    md.start_of_burst = true;
    md.end_of_burst   = false;
    md.has_time_spec  = true;
    md.time_spec = uhd::time_spec_t(0.1);

    cout << boost::format("Setting device timestamp to 0...") <<endl;
    usrpDevice->set_time_now(uhd::time_spec_t(0.0));

    //Check Ref and LO Lock detect
    vector<string> sensor_names;
    sensor_names = usrpDevice->get_tx_sensor_names(0);
    if (find(sensor_names.begin(), sensor_names.end(), "lo_locked") != sensor_names.end()) {
        uhd::sensor_value_t lo_locked = usrpDevice->get_tx_sensor("lo_locked",0);
        cout << boost::format("Checking TX: %s ...") % lo_locked.to_pp_string() <<endl;
        UHD_ASSERT_THROW(lo_locked.to_bool());
    }
    sensor_names = usrpDevice->get_mboard_sensor_names(0);
    if ((ref == "mimo") and (find(sensor_names.begin(), sensor_names.end(), "mimo_locked") != sensor_names.end())) {
        uhd::sensor_value_t mimo_locked = usrpDevice->get_mboard_sensor("mimo_locked",0);
        cout << boost::format("Checking TX: %s ...") % mimo_locked.to_pp_string() <<endl;
        UHD_ASSERT_THROW(mimo_locked.to_bool());
    }
    if ((ref == "external") and (std::find(sensor_names.begin(), sensor_names.end(), "ref_locked") != sensor_names.end())) {
        uhd::sensor_value_t ref_locked = usrpDevice->get_mboard_sensor("ref_locked",0);
        cout << boost::format("Checking TX: %s ...") % ref_locked.to_pp_string() <<endl;
        UHD_ASSERT_THROW(ref_locked.to_bool());
    }

    cout << "Signal is streaming..." <<endl;

    //send data until the stop command is given
    while(stream){
        //fill the buffer with the waveform
        for (size_t n = 0; n < buff.size(); n++){
            buff[n] = wave_table(index += step);
        }

        //send the entire contents of the buffer
        tx_stream->send(buffs, buff.size(), md);

        md.start_of_burst = false;
        md.has_time_spec = false;
    }

    //send a mini EOB packet
    md.end_of_burst = true;
    tx_stream->send("", 0, md);
}