#include "Function.h"
#include "Sinusoid.h"
#include "Sinc.h"
#include "DataSet.hpp"
#include <iostream>
#include <cmath>

using namespace std; 

int main()
{
	float pi = 4*atan(1);
	float phase =0.0; 
	float amp = 1.0; 
	float freq = 1000; 
	Sinusoid<float> sine(amp, freq);  
	cout<<sine(0.001*3/2*pi)<<endl;

	DataSet<float> D1(sine, 10, 0, 0.001); 
	DataSet<float> D2(sine, 10, 0, 0.001); 
	return 0; 
}
