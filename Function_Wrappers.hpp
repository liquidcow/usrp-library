#ifndef FUNCTION_WRAPPERS_HPP
#define FUNCTION_WRAPPERS_HPP
#include "Function.hpp"
// Function Wrappers are simple fucntion based operations. We will allow for the 
// functions to be added, subtracted etc with these'wrappers'. 


// First up, an addition Function


/**
* \class add
* \brief an addition class that allows for non-unique function addition. 
*
* This is an addition class that operates on any Function based object. 
* The main focus is to have a method of adding the results of two functions
* makign it easier to use and create useful functions. The addition class 
* can be treated as a function after is have been created, allowing us
* to created complex functions. Works with other classes such as mult, 
* sub, div, add. 
*
* \author Ashton Hudson
* \version 1.0
* \date 19/11/2012
*/
template <class T>
class add: public Function<T>
{
public:
	add(Function<T> *add1, Function<T> *add2);
	virtual T operator()(T input);
private:
	Function<T> *a1; 
	Function<T> *a2; 
};

/**
* The constructor is the only point of entry into this class. 
* @param *add1 This is one of the functions we want to add
* note that this is a pointer, so please pass it a reference. \
* @param *add2 This is the second added function. 
*
* We can add any objects that have function as the base class. 
*	
*/
template<class T>
add<T>::add(Function<T> *add1, Function<T> *add2) 
{
	a1 = add1;
	a2 = add2;
}

/**
* The round brackets have been overloaded, as is the convention with all functions
* this allows for general usage of code. 
* @param input We accept a type T input and operate on that. 
*/
template <class T>
T add<T>::operator()(T input)
{ 
	float in = (*a1)(input)+(*a2)(input);
	T x = in; 
	return x; 
}


/**
* \class sub
* \brief a subtraction class that allows for non-unique function subtraction. 
*
* This is a subtraction class that operates on any Function based object. 
* The main focus is to have a method of subtracting the results of two 
* functions makign it easier to use and create useful functions. The 
* subtraction class can be treated as a function after is have been 
* created, allowing us to created complex functions. Works with over
* operation types such as mult, div, add, sub. 
*
* \author Ashton Hudson
* \version 1.0
* \date 19/11/2012
*/
template <class T>
class sub: public Function<T>
{
public: 
	sub(Function<T> *sub1, Function<T> *sub2);
	virtual T operator()(T input);
private: 
	Function<T> *s1; 
	Function<T> *s2;
}; 

/**
* Subtract class constructor allows for analytical subtraction of two
* function based objects. 
*
* @param *sub1 This is the first function based object
* @param *sub2 Thus is the second function based object
*
* Note that if sub1 is a constant with value 1, and sub2 is a constant with 
* value 2, the returned value will be -1. 
*
*/
template <class T>
sub<T>::sub(Function<T> *sub1, Function<T> *sub2)
{
	s1 = sub1; 
	s2 = sub2;
}

/**
* This is the overloaded operator, that allows for the subtracted functions
* values to be retrieved. 
* @param input This is a user defined type that returns that template type. 
*/
template <class T>
T sub<T>::operator() (T input)
{
	float in = (*s1)(input); 
	float in2 = (*s2)(input);
	in = in-in2; 
	T x = in; 
	return x; 
}

/**
* \class mult 
* \brief a multiplication class that allows for non-unique function multiplication. 
*
* This is a multiplication class that operates on any Function based object. 
* The main focus is to have a method of multiplying the results of two 
* functions makign it easier to use and create useful functions. The 
* multiplication class can be treated as a function after is have been 
* created, allowing us to created complex functions. Works with over
* operation types such as sub, div, add, mult. 
*
* \author Ashton Hudson
* \version 1.0
* \date 19/11/2012
*/
template <class T>
class mult : public Function<T>
{
public:
	mult ( Function<T> *mult1,  Function<T> *mult2);
	virtual T operator()(T input);
private:
	Function<T> *m1; 
	Function<T> *m2; 
}; 

/**
* The constructor of mult provides the input of two function-based classes. 
* @param *mult1 This is the first argument, please either pass a 
* pointer or a reference to an object to use this constructor. 
* @param *mult2 This is the second argument.
*/
template <class T>
mult<T>::mult( Function<T> *mult1, Function<T> *mult2)
{
	m1 = mult1; 
	m2 = mult2; 
}

/**
* This is the overloaded round bracket that returns the multiplied 
* values of the two functions. 
*
* @param input this is the user defined type input value. (a float is recommended)
*/
template <class T>
T mult<T>::operator()(T input)
{
	float in = (*m1)(input); 
	float in2= (*m2)(input); 
	T out = in*in2; 
	return out; 
}

/**
* \class divide
* \brief a division class that allows for non-unique function division. 
*
* This is a division class that operates on any Function based object. 
* The main focus is to have a method of division the results of two 
* functions making it easier to use and create useful functions. The 
* division class can be treated as a function after is have been 
* created, allowing us to created complex functions. Works with over
* operation types such as mult, div, add, sub. 
*
* \author Ashton Hudson
* \version 1.0
* \date 19/11/2012
*/
template <class T>
class divide : public Function<T>
{
public:
	divide( Function<T> *div1, Function<T> *div2);
	virtual T operator()(T input);
private:
	Function<T> *d1; 
	Function<T> *d2; 
};

/**
* The constructor is where we assign the function objects to this class. 
* @param *div1 is the first parameter 
* @param *div2 is the second function input
*/
template <class T>
divide<T>::divide(Function<T> *div1, Function<T> *div2)
{
	d1 = div1; 
	d2 = div2; 
}

/** 	
* we have overloaded the round brackets to allow for easy access to the dividend. 
* @param input this is the user defined type input that returns that same type. 
* the dividend is returned. 
*/
template <class T>
T divide<T>::operator()(T input)
{
	float in = (*d1)(input); 
	float in2= (*d2)(input); 
	T out = in/in2; 
	return out; 
}
#endif
