#include "Function.hpp"
#include "Sinusoid.hpp"
#include "DataSet.hpp"
#include "Exceptions.hpp"
#include <string>
#include <vector>
#include <iostream>
#include <climits>

using std::string;
using std::vector; 

#ifndef MODULATOR_H
#define MODULATOR_H

/**
* \class Modulator
* \brief Modulator is the base class for all modulator techniques, including QAM and MFSK
* \author Mike Geddes, Matt van der Velden, Ashton Hudson
* \modified Serge Mbamba, Noel Moyo, Yves-Francois Rivard
* \version 2.0
* \date 06/12/2013
*/
class Modulator
{
public:
     /** 
      * Pure virtual function that needs to be overridden in the derived classes.
      * It defines a way of mapping a dataset with a particular number 
      * representing a character.
      */
	virtual void push_back(string) = 0; 
        
      /** 
      * Pure virtual function that needs to be overridden in the derived classes.
      * It defines a way of retrieving an integer value from the dataset that was
      * previously received.
      */
	virtual DataSet<float> pop() = 0; 
        virtual bool PowerOfTwo(int );
        
        
      /** 
      * Pure virtual function that needs to be overridden in the derived classes.
      * It implements the sorting of Basis Functions according to the numerical
      * valued of the binary sequence each represents.
      */
        virtual void basisSort() = 0;
        
protected:
	vector<unsigned> getbits(unsigned char byte, int M_size); 
        vector<string> grayGenerator(int bits);
}; 

template <class T>
struct populateStruct
{
    populateStruct(DataSet<T> &w){
        w.populate();
    }
    void operator()()
    {}
};

#endif
