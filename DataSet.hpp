#include "Function.hpp"
#include "Sinusoid.hpp"
#include "Const.hpp"
#include "Function_Wrappers.hpp"
#include "Exceptions.hpp"
#include <boost/thread.hpp>
#include <cmath>
#include <cstring>
#include <vector>
#include <complex>
#include <string>
#include <fstream>

#ifndef DATASET_HPP
#define	DATASET_HPP


static Const<float> zeroFunctionForDefaultConstructor(0);

/**
* \class DataSet
* \brief data structure for signal processing in conjunction with the USRP wrapper.
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \version 1.0
* \date 19/11/2012 
* @todo Default constructor required
*/
template <class T>
class DataSet{

public:
//default constructor
DataSet();
//constructor - 1 second time
  DataSet(Function<T> &f,double samp);
  //constructor - defined time
  DataSet(Function<T> &f,double samp,double sT,double eT);
  //constructor - defined time - IQ signals
  DataSet(Function<T> &i,Function<T> &q,double samp,double sT,double eT);
  // constructor - create a dataset with complex vector
  DataSet(Function<T> &i,Function<T> &q, double samp, vector<complex<T> > vec);
//overload addition
DataSet operator+(const DataSet& rhs); 
  DataSet operator+(float rhs); 
//overload assignment
DataSet operator=(const DataSet& rhs);
//overload subtraction
DataSet operator-(const DataSet& rhs); 
  //overload multiplication
  DataSet operator*(const DataSet& rhs);
  //overload division
  DataSet operator/(const DataSet& rhs);  
//overload index operator
complex<T>& operator[](double i);
  bool operator==(const DataSet& rhs);
  //very bad gain correction thingy 
  void gainCorrect(float);
//evaluate and populate the DataSet
void populate();
  //push data onto the DataSet
  void push_back(complex<T> d);
  //concatenate two datasets
  void concatenate(DataSet<T> &d);
//query DataSet Size
  int size();
  //query the samplerate
  double sampleRate();
  //query sampling_rate
  double getSamplingRate();
  //query start time
  double start();
  //query end time
  double end();
  //export the inphase function
  Function<T>& inphaseFunction();
  //export the quadrature function
  Function<T>& quadratureFunction();
  //export the vector
  vector<complex<T> > exportData();
  void setData(vector<complex<T> > );
  // return the correlation value
  float correlate(DataSet<T>& inSignal);
  //return correlation of real part
  float correlateI(DataSet<T>& inSignal);
  //return correlation of imag part
  float correlateQ(DataSet<T>& inSignal);
  // slice a dataset and return a sub-dataset
  DataSet<T> slice(int sSample, int eSample);
   float max();
  void exportToCSV(string filename);
  void normalise(float to);
  
  void multiply_by_const(const float);
  // a class that correlates two datasets fast
  friend class Correlator;
  
  

private:
  //data holders
  vector<complex<T> > IQdata;

  //seed functions
Function<T> &func;
  Function<T> &qfunc;

  //computation parameters
double samples;
double sampling_rate;
long double startTime;
long double endTime;
        
  //flags
bool readyForPopulation;
bool IQmode;

  //helper functions
void init();
bool validTimeScales();
bool validSampleSize();
bool sizeMatch(const int & size);
};

/**
* \struct computeRealStruct
* \brief structure for threaded packet energy calculation of the real component
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \version 1.0
* \date 13/12/2012 
*/
template <class T>
struct computeRealStruct
{
  computeRealStruct(vector<complex<T> > &w1, vector<complex<T> > &w2, float &res){
      if(w1.size()!=w2.size()) throw DataSetSizeMismatch();
    for (int i = 0; i<w1.size();i++){
      res += w1[i].real()*w2[i].real();
    } 
  }
  void operator ()()
  {}
};

/**
* \struct computeImagStruct
* \brief structure for threaded packet energy calculation of the imaginary component
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \version 1.0
* \date 13/12/2012 
*/
template <class T>
struct computeImagStruct
{
  computeImagStruct(vector<complex<T> > &w1, vector<complex<T> > &w2, float &res){
      if(w1.size()!=w2.size()) throw DataSetSizeMismatch();
    for (int i = 0; i<w1.size();i++){
      res += w1[i].imag()*w2[i].imag();
    }
  }
  void operator ()()
  {}
};

/**
* Simple default constructor. Makes an empty DataSet. The IQ functions are zero constants, time duration is 0s to 0s, sampling rate is 0, IQ mode is false and this DataSet cannot be populated as is.
*/
template <class T>
DataSet<T>::DataSet():
  func(zeroFunctionForDefaultConstructor),
  qfunc(zeroFunctionForDefaultConstructor),
  startTime(0),
  endTime(0),         
  samples(0),
  sampling_rate(0),
  IQmode(false),
  readyForPopulation(false){
}

/**
* Simple constructor to generate 1 second worth of data at a given sample rate 
* @param &f Analytical function which will seed the DataSet   
* @param samp Sample rate at which data is generated. Directly affects the resulting size of the DataSet in memory.
*/
template <class T>
DataSet<T>::DataSet(Function<T> &f,double samp):
  func(f),
  qfunc(f),
  startTime(0),
  endTime(0),         
  samples(samp),
  sampling_rate(samp),
  IQmode(false),
  readyForPopulation(false){
}

/**
* More detailed constructor to generate and arbitrary time length of data at a given sample rate 
* @param &f Analytical function which will seed the DataSet   
* @param samp Sample rate at which data is generated. Directly affects the resulting size of the DataSet in memory.
* @param sT starting time in seconds
* @param eT ending time in seconds
*/
template <class T>
DataSet<T>::DataSet(Function<T> &f,double samp,double sT,double eT):
  func(f),
  qfunc(f),           //for no defined qfunc, the inphase function is reused but not evaluated
  startTime(sT),
  endTime(eT),
  samples(samp),
  sampling_rate(samp),
  IQmode(false),
  readyForPopulation(false){
    init();
}

/**
* Most detailed constructor to generate and arbitrary time length of data at a given sample rate using the IQ structure. 
* @param &i Analytical function which will seed the inphase part of the DataSet   
* @param &q Analytical function which will seed the quadrature part of the DataSet   
* @param samp Sample rate at which data is generated. Directly affects the resulting size of the DataSet in memory.
* @param sT starting time in seconds
* @param eT ending time in seconds
*/
template <class T>
DataSet<T>::DataSet(Function<T> &i,Function<T> &q,double samp,double sT,double eT):
  func(i),
  qfunc(q),
  startTime(sT),
  endTime(eT),
  samples(samp),
  sampling_rate(samp),
  IQmode(true),
  readyForPopulation(false){
    init();
}

/**
* Constructor to generate a dataset given a complex vector
* @param &i Analytical function which will seed the inphase part of the DataSet   
* @param &q Analytical function which will seed the quadrature part of the DataSet
* @param samp Sample rate at which data is generated
* @param vec complex vector to populate the DataAet with
*/
template <class T>
DataSet<T>::DataSet(Function<T> &i,Function<T> &q, double samp, vector<complex<T> > vec):
  func(i),
  qfunc(q),
  startTime(0),
  endTime(0),
  samples(samp),
  sampling_rate(samp),
  IQmode(true),
  readyForPopulation(false),
  IQdata(vec){
}
/** Add a point to a DataSet
*  @param d complex point to be added
*/
template <class T>
void DataSet<T>::push_back(complex<T> d){
  IQdata.push_back(d);
  endTime = endTime+1/samples;
}

/**
* Overloaded minus operator. subtracts the rhs DataSet from the lfs DataSet element by element.
* @return A resultant DataSet
*/
template <class T> 
DataSet<T> DataSet<T>::operator-(const DataSet<T>& rhs){ 
    
if(!sizeMatch(rhs.IQdata.size())) throw DataSetSizeMismatch();
  if(startTime != rhs.startTime && endTime != rhs.endTime) throw TimeMismatch();  
  sub<T> subI(&func, &rhs.func);
  sub<T> subQ(&qfunc, &rhs.qfunc); 
  DataSet<T> temp(subI,subQ, samples, startTime, endTime);
  temp.readyForPopulation = true;
  for(int i = 0; i < size() ; i++){
     temp.push_back(IQdata[i]-rhs.IQdata[i]);
  }
  return temp;
}
 
/**
* Overloaded multiply operator. 
* @return A resultant DataSet
*/
template <class T> 
DataSet<T> DataSet<T>::operator*(const DataSet<T>& rhs){ 
    
  if(!sizeMatch(rhs.IQdata.size())) throw DataSetSizeMismatch();
  if(startTime != rhs.startTime && endTime != rhs.endTime) throw TimeMismatch();  
  mult<T> multI(&func, &rhs.func);
  mult<T> multQ(&qfunc, &rhs.qfunc); 
  DataSet<T> temp(multI,multQ, samples, startTime, endTime);
  temp.readyForPopulation = true;
  for(int i = 0; i < size() ; i++){
    complex<T> a(IQdata[i].real()*rhs.IQdata[i].real(),IQdata[i].imag()*rhs.IQdata[i].imag());
    temp.IQdata.push_back(a);
  }
  return temp;
}
 
/**
* Overloaded division operator. 
* @return A resultant DataSet
*/
template <class T> 
DataSet<T> DataSet<T>::operator/(const DataSet<T>& rhs){ 
   
  if(!sizeMatch(rhs.IQdata.size())) throw DataSetSizeMismatch();
  if(startTime != rhs.startTime && endTime != rhs.endTime) throw TimeMismatch();  
  divide<T> divideI(&func, &rhs.func);
  divide<T> divideQ(&qfunc, &rhs.qfunc); 
  DataSet<T> temp(divideI,divideQ, samples, startTime, endTime);
  temp.readyForPopulation = true;
  for(int i = 0; i < size() ; i++){
    complex<T> a(IQdata[i].real()/rhs.IQdata[i].real(),IQdata[i].imag()/rhs.IQdata[i].imag());
    temp.IQdata.push_back(a);
  }
  return temp;
}

/**
* Overloaded addition operator. Adds the rhs DataSet to the lfs DataSet element by element
* @return A resultant DataSet
*/ 
template <class T>
DataSet<T> DataSet<T>::operator+(const DataSet<T>& rhs){
   
  if(!sizeMatch(rhs.IQdata.size())) throw DataSetSizeMismatch();
  if(startTime != rhs.startTime && endTime != rhs.endTime) throw TimeMismatch();  
  add<T> addI(&func, &rhs.func);
  add<T> addQ(&qfunc, &rhs.qfunc); 
  DataSet<T> temp(addI,addQ, samples, startTime, endTime);
  temp.readyForPopulation = true;
  for(int i = 0; i < size() ; i++){
    temp.push_back(IQdata[i]+rhs.IQdata[i]);
  }
  return temp; 
} 

/**
* Overloaded addition operator.
* @return A resultant DataSet
*/ 

template <class T>
DataSet<T> DataSet<T>::operator+(float rhs){   
  DataSet<T> temp;//(func, qfunc, samples, startTime, endTime);
  temp.readyForPopulation = true;
  for (unsigned i = 0; i < size(); i++)
    temp.push_back(IQdata[i]+rhs);
  return temp;
} 

/**
* Overloaded assignment operator. "Atomically" copies one DataSet to another
* @return A resultant DataSet
*/
template <class T> 
DataSet<T> DataSet<T>::operator=(const DataSet<T>& rhs){ 
 
  func = rhs.func;
  qfunc = rhs.qfunc;
  samples = rhs.samples;
  sampling_rate = rhs.sampling_rate;
  readyForPopulation = rhs.readyForPopulation;
  startTime = rhs.startTime;
  endTime = rhs.endTime;
  IQdata = rhs.IQdata;
  
  return *this;
} 

/**
* Overloaded indexing operator. 
* @param double i index input
* @return a complex tuple of an IQ value at the given index 
* @todo convert input to  long integer. A decimal input would cause segmentation fault
*/
template <class T>
complex<T>& DataSet<T>::operator[](double i){
  if(i >=0 && i < IQdata.size()-1){
    return IQdata[i];
  }
} 
/**
* Overloaded equality  operator. 
* @param DataSet, DataSet 
* @return true if both data IQdata vector are the same, a population is done prior to comparison 
*/
template<class T>
bool DataSet<T>::operator==(const DataSet<T>& rhs){
    this->populate();
    rhs ->populate();
return (IQdata.size()!=rhs.IQdata)?false:(IQdata.size()==rhs.IQdata);
}


/**
* Function to compute and populate the DataSet. This computation was separated from the construction since it is 
* definitely not an atomic process and is particularly suited to multithreading.  
* @todo This process can/should be sub threaded
* @todo Memory usage of this structure can be reduced by 2 thirds. make everything read IQdata vs separate I Q vectors
*/

template <class T>
void DataSet<T>::populate(){
    if (IQdata.size()==0){
  if(readyForPopulation){
    try{
      if(IQmode){ 

        //compute the inphase and quadrature signals
        T prevI = -1;
        for(long double i = startTime; i <= endTime+0.5*(endTime-startTime)/(samples-1); i+=(endTime-startTime)/(samples-1)){
           
          IQdata.push_back(complex<T>(func(i), qfunc(i)));
          if(i == prevI){
            throw TimingError();
          }else{
            prevI = i;
          }
        }
      }else{
    
        for(long double i = startTime; i <= endTime+0.5*(endTime-startTime)/(samples-1); i+=(endTime-startTime)/(samples-1)){
          IQdata.push_back(complex<T>(func(i), func(i)));
        }
       
      }
       
    }catch(TimingError e){
      cout<<"ERROR:\t\t"<<e.what()<<endl<<"\t\tThis was mostly likely caused by an overflow in the time parameters given to the DataSet"<<endl<<"\nDETAILS:\tstartTime: "<<startTime<<" endtime: "<<endTime<<" step: "<<(endTime-startTime)/samples<<endl<<"\t\tDataSet Size: "<<IQdata.size()<<endl<<"RECCOMENDATION:\tTry reduce sample rate or compute smaller duration of signal."<<endl;
    }
  }else{
    throw DataSetNotReadyForPopulation();
  }
}
}


/**
* Function to query the size of the dataset
* @return integer size of the IQ vector in the DataSet 
*/
template <class T>
int DataSet<T>::size(){
  return IQdata.size();
}

/**
* Function to query the starting time of the DataSet
* @return double value of the time in seconds
*/
template <class T>
double DataSet<T>::start(){
  return startTime;
}

/**
* Function to query the ending time of the DataSet
* @return double value of the time in seconds
*/
template <class T>
double DataSet<T>::end(){
  return endTime;
}

/**
* Function to compare the given timescale of the DataSet. Confirms that it is logical for linear time.
* @return boolean as to whether the timescale is valid or not
*/
template <class T>
bool DataSet<T>::validTimeScales(){
  if(endTime >= startTime)
    return true;
  else
    return false;
}

/**
*  Function to check that the number of samples requested of the DataSet is logical.
* @return boolean as to whether the sample rate is valid or note
* @todo extend the testing to have boundaries for USRP's max sampling rate
*/
template <class T>
bool DataSet<T>::validSampleSize(){
  if(samples >0 && samples <250000000){  //hard coded for 1-250Mhz USRP1
    return true;
  }else{
    return false;
  }
}

/**
* Function to check that two DataSets match in size
* @return boolean as to whether the size of the compared DataSets is the same.
*/
template <class T>
bool DataSet<T>::sizeMatch(const int &size){
  if(IQdata.size()==size)
    return true;
  else
    return false;
}

/**
* Function to initialize the DataSet. Performs checks on the configuration of the DataSet. Clears any previous signal stored in the DataSet.
* @throw InvalidTimeDuration
* @throw InvalidSampleSize
*/
template <class T>
void DataSet<T>::init(){
  //clear the existing data
  IQdata.clear();
  
  //validate the configuration
  if(!validTimeScales()) throw InvalidTimeDuration();
  if(!validSampleSize()) throw InvalidSampleSize();

  //correct for time duration
  samples = samples*(endTime-startTime);

  //flag the DataSet as ready for population  
  readyForPopulation = true;
}

/**
* A concatenation function. Appends the input DataSet to the subject DataSet. 
* The input DataSet is not altered. i.e. DataSet1.concatenate(DataSet2) = DataSet1DataSet2
* @param &d DataSet to be concatenated with
*/
template <class T>
void DataSet<T>::concatenate(DataSet<T> &d){
  

  vector<complex<T> > IQdata2 = (d.exportData());
  IQdata.insert(IQdata.end(),IQdata2.begin(),IQdata2.end());
}

/**
* Function to export the internal data of the DataSet. Used for concatenation and debugging. 
* @return a complex vector of type T.
* @todo implement a reference instead of a pass by value.
*/
template <class T>
vector<complex<T> > DataSet<T>::exportData(){
  return IQdata;
}


/**
* Function to set the internal data of the DataSet. Used for concatenation and debugging. 

*/
template <class T>
void DataSet<T>::setData(vector<complex<T> > Data){
  IQdata=Data;
}

/** 
* Find the max magnitude of the DataSet
* @todo check if this is still used and/or required
* @return float of the max value
*/
template <class T>
float DataSet<T>::max(){
  float max = 0;
  long index;
  for (int k=0;k<IQdata.size();k++)
    if (max <= abs(IQdata[k])){
      max = abs(IQdata[k]);
     index=k; 
    }
    else
      max = max;
  return max;
}

/**
*   Function to correlate current dataset to a given dataset
*   @param &inSignal dataset to correlate with
*   @return correlation The correlation value
*/
template <class T>
float DataSet<T>::correlate(DataSet<T> &inSignal){

  if (!inSignal.size()==IQdata.size()){
    throw DataSetSizeMismatch();
  }
   
  vector<complex<T> > tempvec = (inSignal.exportData());
  float correlation = 0;
  float f1 = 0;
  float f2 = 0;
  float mag = 0;
      
  computeRealStruct<T> a(IQdata, tempvec, f1);
  boost::thread t1(a);

  computeImagStruct<T> b(IQdata, tempvec, f2);
  boost::thread t2(b);

  correlation = f1 + f2;

  t1.join();
  t2.join();
 
  return correlation/(IQdata.size()); 
}

/** 
*   Function to return the result of a correlation of only the Inphase components of two DataSets
*   @param &inSignal the RHS of the correlation
*   @return float of Inphase correlation
*/
template<class T>
float DataSet<T>::correlateI(DataSet<T> &inSignal){
  vector<complex<T> > tempvec = (inSignal.exportData());
  float correlation = 0;
  float f1 = 0;
  computeRealStruct<T> a(IQdata, tempvec, f1);
  boost::thread t1(a);

  correlation = f1;

  t1.join();

  return correlation/(tempvec.size());
}

/** 
*   Function to return the result of a correlation of only the Quadrature components of two DataSets
*   @param &inSignal the RHS of the correlation
*   @return float of Quadrature correlation
*/
template<class T>
float DataSet<T>::correlateQ(DataSet<T> &inSignal){
  vector<complex<T> > tempvec = (inSignal.exportData());
  float correlation = 0;
  float f1 = 0;
  computeImagStruct<T> a(IQdata, tempvec, f1);
  boost::thread t1(a);

  correlation = f1;

  t1.join();

  return correlation/(tempvec.size());
}

/** 
*   Function to return a slice of the dataset
*   @param sSample start sample
*   @param eSample end sample
*   @return DataSet
*/
template <class T>
DataSet<T> DataSet<T>::slice(int sSample, int eSample){
  int samp = eSample - sSample;
  vector<complex<T> > tmp(IQdata.begin()+sSample,IQdata.begin()+eSample);
  DataSet<T> d(func, qfunc, samp, tmp);
  return d;
}

/** 
*   Function to normalize the dataset to a given value
*   @param float normalization value
@todo fix and check if this is needed (may be redundant? see GainCorrect)
*/
template <class T>
void DataSet<T>::normalise(float to)
{
  float total = 0;
  for (unsigned long k = 0; k < IQdata.size(); k++){
    total += sqrt(IQdata[k].real()*IQdata[k].real() + IQdata[k].imag()*IQdata[k].imag());
  }
  std::complex<T> tmp(0.5,0.5);
  float normPower = total / IQdata.size()*(1/to);
  for (unsigned long i = 0; i < IQdata.size()-1; i++){
    IQdata[i] /= normPower;

  }
}

/** 
*   Function to multiply the dataset to a given value
*   @param float normalization value

*/
template <class T>

void DataSet<T>::multiply_by_const(const float scale)
{
   vector < complex <T> > v=IQdata;
  for (unsigned long i = 0; i < IQdata.size(); i++){
    IQdata[i] =IQdata[i]*std::complex<T>(scale);
 
  }
   
}

/** 
*   Function to scale the DataSet up to a given maximum value. The highest amplitude in the DataSet is found, then the appropriate sacling factor is determined
*   in order to make that maximum the same as the desired maximum, all other values in the DataSet are then multiplied by the same scaling factor.
*   @param val new maximum amplitude
*/
template <class T>
void DataSet<T>::gainCorrect(float val){
  T maxI = 0;
  T maxQ = 0;
  T max = 0;
  for(int i = 0; i < IQdata.size()-1;i++){
    if(IQdata[i].real()>maxI){
      maxI = IQdata[i].real();
    }
    if(IQdata[i].imag()>maxQ){
      maxQ = IQdata[i].imag();
    }
    if(maxI>=maxQ)
      max = maxI;
    else
      max = maxQ;
  }
 for(int i = 0; i < IQdata.size()-1;i++){
   IQdata[i]*=(val/max);
  }
   
}

/** 
*   Function to query the sample rate of the dataset
*   @return double sample rate
*/
template <class T>
double DataSet<T>::sampleRate(){
  return (samples/(endTime-startTime));
}

/**
* Function to export the Inphase function
* @return a function
*/
template <class T>
Function<T>& DataSet<T>::inphaseFunction(){
  Function<T>& a = func;
  return a;
}

/**
* Function to export the Quadrature function
*	@return a function
*/
template <class T>
Function<T>& DataSet<T>::quadratureFunction(){
  Function<T>& a = qfunc;
  return a;
}

/**
* Function to export a DataSet to csv file
*	@param filename to be used, file will have ".csv" extension
*/
template <class T>
void DataSet<T>::exportToCSV(string filename){
  std::ofstream outfile;
  filename += ".csv";
  outfile.open(filename.c_str());
  for (int k = 0; k < IQdata.size(); k++){
    outfile<<IQdata[k].real()<<","<<IQdata[k].imag()<<"\n";
  }
  outfile.close();
}

/** 
*   Function to query the sampling rate of the dataset
*   @return double sampling rate
*/

template <class T>
double DataSet<T>::getSamplingRate()
{
    return sampling_rate;
}

#endif