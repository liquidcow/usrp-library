var searchData=
[
  ['add',['add',['../classadd.html',1,'add&lt; T &gt;'],['../classadd.html#aad1a87347f2b9eeed2a89a88aa2537d6',1,'add::add()']]],
  ['addpreamble',['AddPreamble',['../classSyncro.html#a8dba71613e4a2d78cdb35a5cd24ecc24',1,'Syncro']]],
  ['analog_5fprototype',['Analog_Prototype',['../classAnalog__Prototype.html',1,'Analog_Prototype'],['../classAnalog__Prototype.html#a2a6a92830d18cef77d1008ee3ca71139',1,'Analog_Prototype::Analog_Prototype(int order, Filter_type type, float freq_low, float freq_high)'],['../classAnalog__Prototype.html#aadb16f5177392509423c6f51d0acf97a',1,'Analog_Prototype::Analog_Prototype(int order, Filter_type type, float freq_cutoff)']]]
];
