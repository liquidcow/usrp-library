var searchData=
[
  ['samplingstruct',['samplingStruct',['../structsamplingStruct.html',1,'']]],
  ['sequencefilteringindexandcoefficientsizemismatch',['sequenceFilteringIndexandCoefficientSizeMismatch',['../classsequenceFilteringIndexandCoefficientSizeMismatch.html',1,'']]],
  ['sequencefilteringtimemismatch',['sequenceFilteringTimeMismatch',['../classsequenceFilteringTimeMismatch.html',1,'']]],
  ['sinc',['Sinc',['../classSinc.html',1,'']]],
  ['snr',['SNR',['../classSNR.html',1,'']]],
  ['sub',['sub',['../classsub.html',1,'']]],
  ['symboldetector',['SymbolDetector',['../structSymbolDetector.html',1,'']]],
  ['syncbackwardsthread',['syncBackwardsThread',['../structsyncBackwardsThread.html',1,'']]],
  ['syncforwardthread',['syncForwardThread',['../structsyncForwardThread.html',1,'']]],
  ['syncro',['Syncro',['../classSyncro.html',1,'']]]
];
