#include "Function.hpp"

#ifndef CONST_HPP
#define CONST_HPP
/**
* \class Const
* \brief Const is a class that is akin to a DC signal
* 
* Const is a simple signal that allows for a constant value to
* be returned for any time value. This has applications in DSB-FC
* AM transmission for example. 
* 
* \author Ashton Hudson
* \version 1.0
* \date 20/11/2012
*
*/
template<class T>
class Const : public Function<float>
{
public:
	Const(float amplitude);
	virtual float operator()(T input);
private:
	float amp; 
};

/**
* Const constructor that allows for us to set amplitude
* @param float amp This is the amplitude of the wave
*/

template <class T>
Const<T>::Const(float amp): amp(amp)
{}

/**
* The round brackets have been overloaded for when the user wants to 
* evaluate their constant wave at time T. Just pass the time into the 
* wave that the user wants. 
* @param T input this is a user defined type, and the input is time. 
*/
template <class T>
float Const<T>::operator()( T input)
{
	return amp; 
}
#endif
