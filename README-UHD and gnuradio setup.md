This is a brief installation guide for ubuntu based systems (14/11/2012)

Intended for use with the USRP1 devices.

NOTE: As far as I'm aware at the time of writing, this procedure cannot be run as root.
NOTE: Sometimes the downloading of certain files may fail. Double check your proxy settings.
NOTE: This procedure can take up to three hours depeding on internet connection speed and how fast your PC is.

Instructions taken from the build guide at:
http://code.ettus.com/redmine/ettus/projects/uhd/wiki/UHD_Build

remove any previous installation of gnuradio

update OS completely: 	
					sudo apt-get update
					sudo apt-get upgrade

install git:			
					sudo apt-get install git

create a new file to hold the source files for the UHD driver:				
					mkdir uhd_source
					cd uhd_source

Download the source files for the UHD driver from Ettus' repository:		
					sudo git clone git://code.ettus.com/ettus/uhd.git

Install the dependencies:	 
					sudo apt-get install libboost-all-dev libusb-1.0-0-dev python-cheetah doxygen python-docutils

Ensure you have gcc installed. Don't change the gcc version type until GnuRadio and the UHD driver have been installed and are working. To install gcc run:	
					sudo apt-get install gcc

Ensure you have cmake installed: 
					sudo apt-get install cmake

Navigate to the host file in the source files that were downloaded:		
					cd uhd_source/uhd/host

make a new file called build:	
					mkdir build
					cd build

run cmake from inside the build file:	
					sudo cmake ../

Note: This next step should take a few minutes to build if its operating correctly.
The required make files will be placed into the build file. Run make in the build file:	
					sudo make

Test that the make was successful:	
					sudo make test

If 100% of the tests pass:	
					sudo make install

Check that the installer placed a file called libuhd.so in the directory /usr/lib or in /usr/local/lib. This is the dynamic library that gets used when working in c++.Run the LD path configuration to add it to the library paths:
					sudo ldconfig

NOTE: Depending on your OS, it might be installed in /usr/local/share/uhd/utils
Since the USRP1's are USB based, rules for the plug and play events need to be added:	
					cd /usr/share/uhd/utils
					sudo cp uhd-usrp.rules /etc/udev/rules.d
					sudo udevadm control --reload-rules

The USRP1's load a firmware from the computer every time it is power cycled, the firmware is dependent on the OS and build version. The image file it downloads should be around 6-7mb. Download the appropriate firmware for you by running:
					cd
					sudo python /usr/share/uhd/utils/uhd_images_downloader.py
or:
					sudo python /usr/local/share/uhd/utils/uhd_images_downloader.py

The UHD driver is now installed. Now to download, build and install gnuradio.

NOTE: This will download the source files for gnuradio as well as its dependencies. This could be up to 200mb. The building of gnuradio will take atleast an 30 minutes, up to 3 hours. Take a coffee break.

NOTE: This may also download an additional USRP1 image/firmware file if the script deems it necessary

run the following set of commands:
					cd
					sudo wget http://www.sbrac.org/files/build-gnuradio
					sudo chmod a+x ./build-gnuradio
					sudo ./build-gnuradio	
 
 You'll be prompted to proceed. enter y;
 If asked if you have sudo privalages. enter y. 




