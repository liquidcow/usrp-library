#include "Demodulator.h"
#include "QAM.h"
#include "DataSet.hpp"
#include "Exceptions.hpp"
#include "Sinusoid.hpp"
#include "Function_Wrappers.hpp"
#include <string>
#include <queue>
#include <cmath>
#include <iostream>
#include <boost/thread.hpp>

using std::string; 
using std::queue; 

#ifndef QAM_DEMOD_H
#define QAM_DEMOD_H



/**
* \class QAM_Demod 
* \brief QAM_Demod is the QAM demodulator that will be used to send information
* 
* QAM is a modulator that has the ability of a user setting a constellation size, and then 
* the modualtor will operate chars from there on. 
*
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \modified Noel Moyo, Serge Mbamba, Yves-Francois Rivard
* \version 2.0
* \date 06/12/2013
*/
class QAM_Demod : public Demodulator
{
public:
	QAM_Demod(QAM &,float erasure_cutoff, float phaseOffset); 

	void push_back(DataSet<float> information);

	string pop(); // returns a string of chars that have been successfully demodulated. 

	void set_erasure(float updated_erasure); 

	//remember we are inheretanting the getbits function 
private:
	int QAM_Size;   			// what size is this beast? 
	int erasure_cutoff; 					// symbols per pulse rate
	char information ;			// the thing we will want to convert into signals
	float frequency; 
	unsigned int periods; 
	float sample_rate;
	float phase;
	float pi;

	queue< string > stringQueue; 	  // the queue of data that will buffer information
	 

	// the waves that will be used to compute 

	Sinusoid<float> sine; 
	Sinusoid<float> cosine;

	// below are the functions that will initialize the QAM object. 
	void MQAM_Demod(); 
        int QAM_Decoder(DataSet<float> input); // The correct symbol intrepter
        long double Distance(constellation_point first_Point, constellation_point sec_Point);
        
        vector<DataSet<float> > constellation;// this is the calculated value for the constellation
        vector <constellation_point> constellation_structs;
};

/**
* \struct SymbolDetector
* \brief Launch a thread for detecting symbols
*/
template <class T>
struct SymbolDetector
{
    SymbolDetector(DataSet<T> &symbol, float &result, DataSet<T> &slice){
        result = symbol.correlate(&slice);
    }
    void operator()()
    {}
};

#endif