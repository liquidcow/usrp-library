#include "Modulator.h"
#include "DataSet.hpp"
#include "Exceptions.hpp"
#include "Sinusoid.hpp"
#include "Function_Wrappers.hpp"
#include <string>
#include <queue>
#include <cmath>
#include <iostream>
#include <boost/thread.hpp>

#ifndef FSK_H
#define FSK_H

enum coherence {coherent, noncoherent};

/**
* \class FSK 
* \brief FSK is the Frequency Shift Keying modulator that will be used to send information
* 
* FSK is a modulator that has the ability of a user setting a constellation size, and then 
* the modulator will operate chars from there on.
*
* \author Yves-Francois Rivard, Serge Mbamba, Noel Moyo
* \version 2.0
* \date 26/11/2013
*/
class FSK : public Modulator
{

public:
	FSK(int M_Ary_Size, int seperation, float frequency, unsigned int periods, float sample_rate, coherence _coherence); 
	void push_back(string information);
	DataSet<float> pop();
        vector<string>   getGrayVector() const;
        int getSeparation() const;
        int getSize() const;
        float getFrequency() const;
        unsigned int getPeriods() const;
        float getSamplingRate() const;
        vector<float>  basisFrequencies() const;
        void basisSort();
private:
	int FSK_Size;  
	int seperation; 					
	char information;			// the thing we will want to convert into signals
	float frequency;//centre frequency
	unsigned int periods; 
	float sample_rate;
	float pi;
        coherence _coherence;

	queue <DataSet<float> > DataQueue; 	  // the queue of data that will buffer information
	vector <DataSet<float> > constellation; // this is the calculated value for the constellation
        vector <float> frequencies;
        vector <string> GrayVector;
        void MFSK(); //function that will initialize the FSK object.     
};

#endif