#include "FSK_demod.h"

/**
 * \brief function to obtain the demodulated string from the demodulator.
* @return string demodulated string
*/
string FSK_demod::pop()
{
   	 string D = stringQueue.front(); 
	 stringQueue.pop();
	 return D;
}

/**
 * \brief function used to push back a dataset which much then be demodulated by the device.
* @param inf DataSet to be demodulated
*/
void FSK_demod::push_back(DataSet <float> inf)
{
     string binary_sequenceReceived = "";
    //empty dataset check
     if(inf.size() <= 0){
     stringQueue.push(binary_sequenceReceived);
     }else{
        int count =1;
        unsigned char TempChar;
        vector<unsigned> temp;
        string tempString;
        int str_int=0;
        float sample_slice_size = round ((sample_rate/frequency)*periods);  
               
                for(int i=0; i<(inf.size()/sample_slice_size);i++)  
                { 
                        DataSet <float> myslice= inf.slice(i*sample_slice_size,(i+1)*sample_slice_size);
                        binary_sequenceReceived= demodulator(myslice);
                       
                        for (int i=0; i<(binary_sequenceReceived).size(); i++){
       
                                str_int= str_int*2 +  ((binary_sequenceReceived)[i]-'0') ;
                        }
                        
                        temp.push_back(str_int);
                        str_int=0;
                       
                        if (count==ceil(8/log2(FSK_Size)))
                        {
                                TempChar = getChar(temp, log2(FSK_Size));
                                tempString+=TempChar;
                                count =0; 
                                temp.clear();
                        }
                        count++;
                }
        stringQueue.push(tempString);
        }  
}       
/** \brief FSK_Demod Constructor
 * 
 * @param myFSK is an FSK object argument
 * @param _coherence is a coherent argument for non-coherent or coherent demodulation
 */
FSK_demod::FSK_demod (FSK & myFSK, coherence _coherence):
        FSK_Size(myFSK.getSize()),
        seperation(myFSK.getSeparation()),
        frequencies(myFSK.basisFrequencies()),
        periods(myFSK.getPeriods()),
        sample_rate(myFSK.getSamplingRate()),
        _coherence(_coherence),
        grayVector(myFSK.getGrayVector()),
        frequency(myFSK.getFrequency())
{  
    pi = 4*atan(1.0);
    MFSK_demod();
}

/**
* \brief Function used to rectify the basis due to variations in frequency cause by the channel.
* @param freq float original transmitted basis frequency
* @param input received dataSet
* @param range desired range of the frequency sweep.
*/

float FSK_demod::freqSweep(float freq, DataSet <float> input, float range)
{
    Correlator myCorrelator;
    float tempfreq = tempfreq - ceil(range/2.0);
    float lockedFreq = freq;
    float correlation = 0;
    float correlation_temp = 0;
    
    for (float i = 0; i < range; i++)
    {
        Sinusoid <float> sine (1.0, tempfreq, pi/2);
        DataSet <float> sineData (sine, sample_rate, 0.0, periods/frequency);
        sineData.populate();
        correlation_temp = myCorrelator.correlate(input, sineData);
        
        if (correlation_temp > correlation)
        {
            correlation = correlation_temp;
        }
        
        correlation_temp = 0;
        tempfreq++;
    }
   
    return lockedFreq;
}
/** 
 * \brief MFSK_demod function to demodulate the message for any FSK size which is a power of 2.
 */
void FSK_demod::MFSK_demod()
{
       vector < Sinusoid<float> > IBasis;
       vector < Sinusoid<float> > QBasis;
       float freq = 0;
    
       for (int  i=0; i < FSK_Size; i++){
            IBasis.push_back(Sinusoid<float> (1.0,frequencies [i],pi/2));
            QBasis.push_back(Sinusoid<float> (1.0,frequencies [i],0));
            DataSet<float> IPoint(IBasis[i],sample_rate,0.0, periods/frequency); 
            DataSet<float> QPoint(QBasis[i],sample_rate,0.0, periods/frequency);
            populateStruct<float> a(IPoint);
            Iconstellation.push_back(IPoint);
            populateStruct<float> b(QPoint);
            Qconstellation.push_back(QPoint);
       }   
}
/**
 * \brief demodulator 
 * @param input a Dataset argument
 * @return binary values in the constellation
 */
string FSK_demod::demodulator (DataSet <float> input)
{
    Correlator myCorrelator;
    float correlation = 0.0;
    float correlation_temp = 0.0;
    float temp;
    int received_freq_position = 0;
  
    for (int i = 0; i< FSK_Size; i++)
    {
        temp = myCorrelator.correlate(input, Iconstellation[i]);
        correlation_temp += temp * temp;
        temp = myCorrelator.correlate(input, Qconstellation[i]);
        correlation_temp += temp * temp;
       
        if (correlation_temp > correlation)
        {
            correlation = correlation_temp;
            received_freq_position = i;
        }
    
        correlation_temp = 0;
    }
    return grayVector[received_freq_position];
}