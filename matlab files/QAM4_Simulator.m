clear;
sl = 2048;
t =linspace(0,0.00001, sl);

sine = sin(1000000*t*pi*2); 
cosine = cos(1000000*t*pi*2); 

%transmitting 4-QAM, sending the letter 'a'

output(1:sl) = -sine(1:sl) +cosine(1:sl);       % 01
output(sl+1:sl*2) = sine(1:sl)-cosine(1:sl);    % 10 
output(sl*2+1:sl*3) = sine(1:sl)+cosine(1:sl);  % 00
output(sl*3+1:sl*4) = -sine(1:sl)+cosine(1:sl); % 01

plot(output);