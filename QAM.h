#include "Modulator.h"
#include "DataSet.hpp"
#include "Exceptions.hpp"
#include "Sinusoid.hpp"
#include "Function_Wrappers.hpp"
#include <string>
#include <queue>
#include <fstream>
#include <cmath>
#include <iostream>
#include <boost/thread.hpp>

using std::string; 
using std::queue; 

#ifndef QAM_H
#define QAM_H

         /**
        * \struct constellation_position
        * \brief Structure corresponding to a constellation point. 
          * The struct holds both  the corresponding binary sequence and the coordinates(on the x,y plan) of the constellation point.
          * \author  Noel Moyo, Serge Mbamba, Yves-Francois Rivard
          * \version 2.0
          * \date 05/12/13
        */
        struct constellation_point
        {
            string bits;
            double x_amplitude;
            double y_amplitude;
            // Overloading the = operator
           struct constellation_point& operator=(const struct constellation_point rhs)
            {
                bits=rhs.bits;
                x_amplitude=rhs.x_amplitude;
                y_amplitude=rhs.y_amplitude;
                return *this;
            }
           // Overloading the == operator
           bool operator==(struct constellation_point rhs)
           {
              
                return(  bits==rhs.bits &&  x_amplitude==rhs.x_amplitude &&  y_amplitude==rhs.y_amplitude);
           }
          
           // Overloading the stream extraction operator
          friend ostream& operator << (ostream& out, struct constellation_point& rhs)
           {         
              
               out<<"Bits: "<< rhs.bits << " at "<< '('<<rhs.x_amplitude<<','<<rhs.y_amplitude<<')'<<endl;
               return out;
           }
         
          // Overloading the () operator
          struct constellation_point& operator()(string str, double x, double y){
           
                bits=str;
                x_amplitude=x;
                y_amplitude=y;
                return *this;
           }
        };

/**
* \class QAM 
* \brief QAM is the QAM modulator that will be used to send information
* 
* QAM is a modulator that has the ability of a user setting a constellation size, and then 
* the modulator will operate chars from there on. 
*
* \author Hudson, Mike Geddes, Matthew van der Velden
* \modified Noel Moyo, Serge Mbamba, Yves-Francois Rivard
* \version 2.0
* \date 06/12/2013
*/
class QAM : public Modulator
{
public:
	QAM(int M_Ary_Size, float frequency, unsigned int periods, float sample_rate); 

	void push_back(string information);
	DataSet<float> pop(); 
        void basisSort();
        int getSize() const;
        float getFrequency() const;
        unsigned int getPeriod() const;
        float getSampleRate() const;
        vector <constellation_point> getStructs() const;
        vector <DataSet<float> > getConstellation() const;

	//remember we are inheriting the getbits function 
private:
	int QAM_Size;   			// what size is this beast? 
	char information ;			// the thing we will want to convert into signals
	float frequency; 
	unsigned int periods; 
	float sample_rate;
	float pi;
	//DataSet<float> Padding; 
	queue<DataSet<float> > DataQueue; 	  // the queue of data that will buffer information
	vector<DataSet<float> > constellation; // this is the calculated value for the constellation

	// the waves that will be used to compute 
	Sinusoid<float> sine; 
	Sinusoid<float> cosine; 
        
        int x_bit;
        int y_bit;
        
	// below are the functions that will initialize the QAM object. 
        void MQAM();
        void bitsplitter();
        vector <int> amplitudeGenerator(const int bits);
        vector <constellation_point> constellation_structs;
};

#endif