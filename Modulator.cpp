#include "Modulator.h"

/**
* Modulator has a general char to int converter that will be used for 
* mapping bits. 
* @param char byte this is the char input that will be mapped. 
* @param int M_size this lets us know how bit the bits must be. 
*/

std::vector<unsigned> Modulator::getbits(unsigned char byte, int M_size)
{
	std::vector<unsigned> v;
	unsigned char tmp = 0;
	unsigned char mask = 0xff>>(8-M_size);
	unsigned int max = 8;
        
        if (M_size > 8)
        {
                throw InvalidNumOfBits(); 
        }
	
        max = ceil (8/(double)M_size);
	
	for (int k = 0; k<max; k++){
		
		tmp = byte & mask;
		byte >>= M_size;
             
		v.push_back((int)tmp);
	}

	return v;
}

/**
* \fn checks if constellation size is a power of two.
 * @param int size is the size of the constellation that is to be checked.
 * @return true is power of two otherwise false.
*/

bool Modulator::PowerOfTwo(int size ){
   
    if (int(size)!=size || size<1) throw WrongSizeValue();
    float result=size;
    while (result>1)
    {
        result/=2;
    }
    return (result==1);
}

/**
 * \fn Generates a cyclical gray binary sequence for use in FSK
 * @param int bits this is the number of bits for the gray code.
 * @return vector of strings containing the sequence.
*/

vector<string> Modulator::grayGenerator(int bits)
{
    vector<string> grayVector;
    vector<string> tempGrayVector;
    grayVector.push_back("0");
    grayVector.push_back("1");
    
    if (bits == 0)
    {
        throw wrongBitGraySelection(); 
    }
    
    for (int i = 0; i < bits - 1; i++)
    {
        tempGrayVector.clear();
        tempGrayVector = grayVector;
        grayVector.clear();
        
        for (std::vector<string>::iterator it = tempGrayVector.begin(); it < tempGrayVector.end(); it++)
        {
            grayVector.push_back(*it + "0");
        }
        
        for (std::vector<string>::iterator it = tempGrayVector.end() - 1; it >= tempGrayVector.begin(); it--)
        {
            grayVector.push_back(*it + "1");
        }
    }
    
    return grayVector;
}