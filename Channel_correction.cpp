#include "Channel_correction.h"


/**
 * 
 * @param start_freq float value, which gives the minimum value of the frequency range
 * @param end_freq   float value that marks the maximum value of the frequencey range
 * @param delta_freq float value for increamental step change of frequency
 * @param sampling_rate smaplin frequency
 */
Channel_correction::Channel_correction (float start_freq,float end_freq,float delta_freq, double sampling_rate): _current_freq(start_freq), _end_freq(end_freq), _delta_freq (delta_freq), _sampling_rate (sampling_rate)
{
}

/**
 * \fn freqSweep
 * @return DataSet <float> next frequency to output
 */

DataSet <float>& Channel_correction::freqSweep()
{
    if (_current_freq <= _end_freq)
    {
        Sinusoid<float> sinusoid (1.0, _current_freq, 0);
        DataSet<float> signal (sinusoid,_sampling_rate,0,0.1);
        signal.populate();
        set = signal;
        _current_freq = _current_freq + _delta_freq;
        return set;
    }
    else
    {
        throw wrongFrequencySweepRange();
    }
}
/** \fn is_valid_freq
 * \brief check if the current frequency value is within the range
 * @return boolean value
 */
bool Channel_correction::is_valid_freq()
{
    if (_current_freq <= _end_freq)
        return true;
    else
        return false;
}