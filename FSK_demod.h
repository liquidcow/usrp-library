#include "Demodulator.h"
#include "FSK.h"
#include "DataSet.hpp"
#include "Exceptions.hpp"
#include "Sinusoid.hpp"
#include "Function_Wrappers.hpp"
#include "Correlator.hpp"
#include <string>
#include <queue>
#include <cmath>
#include <iostream>
#include <boost/thread.hpp>

using std::string; 
using std::queue; 

#ifndef FSK_DEMOD_H
#define FSK_DEMOD_H


/**
* \class FSK_demod 
* \brief FSK_demod is the FSK demodulator that will be used to receive information
* 
* FSK_demod is a demodulator that has the ability of a converting a modulated message back to it's original form 
*
* \author Serge Mbamba, Noel Moyo, Yves-Francois Rivard
* \version 2.0
* \date 06/12/2013
*/
class FSK_demod : public Demodulator
{
public:
    FSK_demod ( FSK &, coherence );
    string pop();
    void push_back (DataSet<float>);
    float freqSweep (float freq, DataSet <float> input, float range);
    string demodulator (DataSet <float>);

private:
    void MFSK_demod();
    
    float pi;
    int FSK_Size;  
    int seperation;
    unsigned int periods; 
    float sample_rate;
    coherence _coherence;
    float frequency; //centre frequency
    vector <float> frequencies;
    vector <string>  grayVector;
    queue< string > stringQueue; 	  // the queue of data that will buffer information
    vector <DataSet<float> > Iconstellation; // this is the calculated value for the constellation
    vector <DataSet<float> > Qconstellation;
};

#endif