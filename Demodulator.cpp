#include "Demodulator.h"
/**
 * \brief getChar function to extract the demodulated characters of the original message
 * @param inf unsigned vector argument
 * @param size int argument
 * @return single characters of the demodulated message
 */
unsigned char Demodulator::getChar(vector<unsigned> inf, int size)
{
	unsigned char temp = 0x00; 
	unsigned char superTemp = inf[inf.size()-1];
	if (inf[0]<0)
	{
		return temp; 
	}
	temp = temp + superTemp ; 

	for (int j = inf.size()-1; j>=0;j--){
		temp = temp<<size; 
		unsigned char superTemp = inf[j];
		if(inf[0]<0)
		{
			temp = 0x00;
			return temp;
		}
		temp = temp + superTemp ; 
		
	}
	return temp;
}