#include "Function.hpp"
#include <cmath>

#ifndef SINUSOID_HPP
#define SINUSOID_HPP


/**
* \class Sinusoid
* \brief Sinusoid is a class that is akin to sine and cosine waves
*
* Sinusoid allows for a user defined sinusoidal wave that can have 
* its amplitude, frequency and phase set to allow for the creation 
* of unique sinusoidal waves. Such as to create a cosine wave with 
* an amplitude of 1, and a frequency of 1000Hz, write :
* Sinusoid<float> cosine(1,1000, pi/2)
* 
* \author Ashton Hudson
* \version 1.0
* \date 20/11/2012
*
*/

template <class T>
class Sinusoid: public Function<T>
{
public:

	Sinusoid(float amplitude=0,float frequency=0, float phaseAngle=0);
	virtual T operator()(T input);
private:
	float amp; 
	float freq; 
	float phase; 
	float pi; 
};

/**
* Sinusoid constructor that allows for us to set amplitude, frequency and phase
* @param amp This is the amplitude of the wave
* @param freq This is the frequency of the wave in hertz
* @param phase this is the phase of the wave in radians, not degrees! 
*/
template<class T>
Sinusoid<T>::Sinusoid(float amp, float freq, float phase) : amp(amp), freq(freq), phase(phase)
{
	pi = 4*atan(1.0);
}


/**
* The round brackets have been overloaded for when the user wants to 
* evaluate their sinusoidal wave at time T. Just pass the time into the 
* wave that the user wants. 
* @param input this is a user defined type, and the input is time. 
*/
template <class T>
T Sinusoid<T>::operator()(T input)
{ 
	float in = static_cast<float> (input); 
	//in = amp*sin(2*pi*in+phase); 
	T x = static_cast<T> (amp*sin(2*pi*freq*in+phase)); 
	return x; 
}

#endif
