#include <fstream>
#include <cmath>
#include "DataSet.hpp"
#include "Sinusoid.hpp"
#include <boost/thread.hpp>

using namespace std;
using namespace boost;

#ifndef GRAPH_HPP
#define GRAPH_HPP

/**
* \struct graphThreadInphase
* \brief structure for launching a thread of graphing a DataSet's Inphase information
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \version 1.0
* \date 10/12/2012 
* @todo make local variables private
*/
template <class T>
struct graphThreadInphase
{
    graphThreadInphase(string f, DataSet<T> d): filename(f), data(d) {}
    void operator()()
    {
	    filename+="_Inphase.m";
		ofstream graph;
		graph.open(filename.c_str());

		graph<<"clear;"<<endl;
		graph<<"figure(1)"<<endl;
		
		graph<<"x=[";
		for(int i = 0; i < data.size()-1;i++){
			graph<<data[i].real()<<endl;
		}
		graph<<"];"<<endl;
		graph<<"plot(x);"<<endl;
		graph<<"title (\"Inphase plot\");"<<endl;
	  	graph<<"xlabel (\"Samples\");"<<endl;
	  	graph<<"ylabel (\"Amplitude\");"<<endl;
		graph.close();
    }
    string filename;
    DataSet<T> data;
};

/**
* \struct graphThreadConstellation
* \brief structure for launching a thread of graphing a DataSet's IQ constellation
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \version 1.0
* \date 10/12/2012 
* @todo make local variables private
*/
template <class T>
struct graphThreadConstellation
{
    graphThreadConstellation(string f, DataSet<T> d, double freq, double s, int spp): filename(f), data(d), frequency(freq), sampleRate(s), periodsPerSymbol(spp) {}
    void operator()()
    {
    	//define pi
		float pi = 4*atan(1);
		//define the Real and Imaginary Axis
    	Sinusoid<float> sine(1, frequency,0); 
		Sinusoid<float> cosine(1, frequency,pi/2);
		Const<float> zero(0);

		//determine the symbols size
		float sampleSliceSize = (sampleRate/frequency)*periodsPerSymbol;

		//generate the reference sinusoids
		DataSet<float> sineSample(sine, sampleRate, 0.0, periodsPerSymbol/frequency);
		DataSet<float> cosineSample(cosine, sampleRate, 0.0,periodsPerSymbol/frequency);

		//populate the sinusoids
		sineSample.populate();
		cosineSample.populate();

		//vectors to store the XY data
		vector<float> xdata, ydata;
		float max = 0;


		//correlate the data to the Real/Imaginary Axis
		for(long i = 0; i < (data.size()/sampleSliceSize);i++ ){
			//get the symbol slices
			DataSet<float> symbol = data.slice(i*sampleSliceSize,(i+1)*sampleSliceSize);

			//correlate the X and Y
			xdata.push_back(symbol.correlate(cosineSample));
			ydata.push_back(symbol.correlate(sineSample));

			//determine the max values
			if(xdata[xdata.size()-1]){
				max = xdata[xdata.size()-1];
			}
			if(ydata[ydata.size()-1]){
				max = ydata[ydata.size()-1];
			}
		}
		//set up the grid size
		max+=1;
		max = round(max);
		max = 10;

		//write the file
	    filename+="_Constellation.m";
		ofstream graph;
		graph.open(filename.c_str());
		graph<<"clear;"<<endl;
		graph<<"figure(3)"<<endl;
		graph<<"x=[";
		for(int i = 0; i < (data.size()/sampleSliceSize);i++){
			graph<<xdata[i]<<endl;
		}
		graph<<"];";
		graph<<"y=[";
		for(int i = 0; i < (data.size()/sampleSliceSize);i++){
			graph<<ydata[i]<<endl;
		}
		graph<<"];"<<endl;
		graph<<"plot(x,y,\"x\");"<<endl;
		graph<<"title (\"Constellation plot\");"<<endl;
	  	graph<<"xlabel (\"Real\");"<<endl;
	  	graph<<"ylabel (\"Imaginary\");"<<endl;
	  	graph<<"axis ([-"<<max<<", "<<max<<", -"<<max<<", "<<max<<"], \"square\");"<<endl;
	  	graph<<"grid;"<<endl;
		graph.close();
		
    }
    string filename;
    DataSet<T> data;
    double frequency, sampleRate;
    int periodsPerSymbol;
};

/**
* \struct graphThreadQuadrature
* \brief structure for launching a thread of graphing a DataSet's Quadrature information
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \version 1.0
* \date 10/12/2012 
* @todo make local variables private
*/
template <class T>
struct graphThreadQuadrature
{
    graphThreadQuadrature(string f, DataSet<T> d): filename(f), data(d){}
    void operator()()
    {
	    filename+="_Quadrature.m";
	    ofstream graph;
	  	graph.open(filename.c_str());
	  	graph<<"clear;"<<endl;
		graph<<"figure(2)"<<endl;
	  	graph<<"x=[";
	  	for(int i = 0; i < data.size()-1;i++){
	    	graph<<data[i].imag()<<endl;
	  	}
	  	graph<<"];"<<endl;
	  	graph<<"plot(x);"<<endl;
	  	graph<<"title (\"Quadrature plot\");"<<endl;
	  	graph<<"xlabel (\"Samples\");"<<endl;
	  	graph<<"ylabel (\"Amplitude\");"<<endl;	
	  	graph.close();
    }
    string filename;
    DataSet<T> data;
};

/**
* \struct graphThreadMagnitude
* \brief structure for launching a thread of graphing a DataSet's Magnitude information
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \version 1.0
* \date 10/12/2012
* @todo make local variables private 
*/
template <class T>
struct graphThreadMagnitude
{
    graphThreadMagnitude(string f, DataSet<T> d): filename(f), data(d){}
    void operator()()
    {
	    filename+="_Magnitude.m";
	    ofstream graph;
	  	graph.open(filename.c_str());
	  	graph<<"clear;"<<endl;
		graph<<"figure(3)"<<endl;
	  	graph<<"x=[";
	  	for(int i = 0; i < data.size()-1;i++){
	  		T value = sqrt(data[i].real()*data[i].real()+data[i].imag()*data[i].imag());
	    	graph<<value<<endl;
	  	}
	  	graph<<"];"<<endl;
	  	graph<<"plot(x);"<<endl;
	  	graph<<"title (\"Magnitude plot\");"<<endl;
	  	graph<<"xlabel (\"Samples\");"<<endl;
	  	graph<<"ylabel (\"Amplitude\");"<<endl;	
	  	graph.close();
    }
    string filename;
    DataSet<T> data;

};

/**
* \brief Multithreaded class for producing Matlab/Octave scripts to graph a DataSet's information
* \author Ashton Hudson, Mike Geddes, Matthew van der Velden
* \version 1.0
* \date 10/12/2012
*/
template <class T>
class Graph{

public:
	//Constructor
	Graph(double s):sampleRate(s){}
	//various plotting functions
	void plot(string, DataSet<T>);
	void plot(string, DataSet<T>, double, int);
	void plotInphase(string, DataSet<T>);
  	void plotQuadrature(string, DataSet<T>);
  	void plotMagnitude(string, DataSet<T>);
  	void plotConstellation(string, DataSet<T>, double, int);
	
private:
	double sampleRate;
};

/**
* Function to plot just the inphase and quadrature information of a DataSet. No constellation is drawn.
* @param filename text name for the file
* @param data DataSet to be graphed
*/ 
template <class T>
void Graph<T>::plot(string filename, DataSet<T> data){
	//begin the slow writing of the plotable file in each thread
	graphThreadInphase<float> a(filename, data);
	thread t1(a);

	graphThreadQuadrature<float> b(filename, data);
	thread t2(b);

	graphThreadMagnitude<float> c(filename, data);
	thread t3(c);

	//join the threads
	t1.join();
	t2.join();
	t3.join();
}

/**
* Function to plot the all three sets of graphs of a DataSet
* @param filename text name for the file
* @param data DataSet to be graphed
* @param frequency the frequency that the constellation must be plotted at
* @param periodsPerSymbol the number of periods each symbol in the scheme is comprized of.
*/ 
template <class T>
void Graph<T>::plot(string filename, DataSet<T> data, double frequency, int periodsPerSymbol){
	//begin the slow writing of the plotable file in each thread
	graphThreadInphase<float> a(filename, data);
	thread t1(a);

	graphThreadQuadrature<float> b(filename, data);
	thread t2(b);

	graphThreadMagnitude<float> c(filename, data);
	thread t3(c);

	graphThreadConstellation<float> d(filename, data, frequency, sampleRate, periodsPerSymbol);
	thread t4(d);

	//join the threads
	t1.join();
	t2.join();
	t3.join();
	t4.join();
}

/**
* Function to plot the IQ constellation of a DataSet
* @param filename text name for the file
* @param data DataSet to be graphed
* @param frequency the frequency that the constellation must be plotted at
* @param periodsPerSymbol the number of periods each symbol in the scheme is comprized of.
*/ 
template <class T>
void Graph<T>::plotConstellation(string filename, DataSet<T> data, double frequency, int periodsPerSymbol){
	graphThreadConstellation<float> a(filename, data, frequency, sampleRate, periodsPerSymbol);
	thread t(a);
	t.join();
}

/**
* Function to plot the in phase information of a DataSet
* @param filename text name for the file
* @param data DataSet to be graphed
*/ 
template <class T>
void Graph<T>::plotInphase(string filename, DataSet<T> data){
	graphThreadInphase<float> a(filename, data);
	thread t(a);
	t.join();
}

/**
* Function to plot the quadrature information of a DataSet
* @param filename text name for the file
* @param data DataSet to be graphed
*/ 
template <class T>
void Graph<T>::plotQuadrature(string filename, DataSet<T> data){
	graphThreadQuadrature<float> a(filename, data);
	thread t(a);
	t.join();
}

/**
* Function to plot the magnitude information of a DataSet
* @param filename text name for the file
* @param data DataSet to be graphed
*/ 
template <class T>
void Graph<T>::plotMagnitude(string filename, DataSet<T> data){
	graphThreadMagnitude<float> a(filename, data);
	thread t(a);
	t.join();
}

#endif
